import React from 'react';
import { View, Text, StyleSheet, SafeAreaView, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { whiteColor, fontSize16, primaryColor } from '../../redux/actions/constant';

export default class SignupHeader extends React.Component{
  handleBack = () => {
    if (this.props.handleBackBtn) {
      return this.props.handleBackBtn()
    }
    this.props.navigation.goBack();
  }

  logout = () => {
    let { navigation } = this.props;
    AsyncStorage.removeItem('token')
    AsyncStorage.removeItem('user_id')
    AsyncStorage.removeItem('user')
    navigation.navigate('SignIn')
  }

  renerBackButton(){
    if (this.props.previousScreen) {
      let arrowImage = this.props.whiteBg ? require('../../assets/images/arrow-left-red.png') : require('../../assets/images/icon-back.png')
      return <TouchableOpacity style={{position: 'absolute', left: 15, height: 20, width: 165, top: 22}} onPress={this.handleBack}>
                <Image source={arrowImage} style={{width: 25, height: '100%'}} />
                <Text style={styles.previousScreen}>{this.props.previousScreen}</Text>
              </TouchableOpacity>
    }
    if (!this.props.onlyLogo && !this.props.profileHeader && !this.props.messageHeader || this.props.chatHeader) {
      let arrowImage = this.props.whiteBg ? require('../../assets/images/arrow-left-red.png') : require('../../assets/images/icon-back.png')
      return <TouchableOpacity style={{position: 'absolute', left: 15, height: 20, width: 25, top: 22}} onPress={this.handleBack}>
                <Image source={arrowImage} style={{width: '100%', height: '100%'}} />
              </TouchableOpacity>
    } else if(!this.props.onlyLogo && (this.props.profileHeader || this.props.messageHeader)){
      return <TouchableOpacity style={{position: 'absolute', left: 15, height: 22, width: 35, top: 20}} onPress={() => this.props.navigation.navigate('ProfileEdit')}>
                <Text style={[styles.editButtonTxt, this.props.whiteBg ? {color: primaryColor} : {}]}>Edit</Text>
              </TouchableOpacity>
    }
  }

  renderLogo(){
    if (this.props.onlyLogo && !this.props.profileHeader) {
      return <Image source={require('../../assets/images/logo_white.png')} style={{width: 100, height: 40}} />
    } 
  }

  renderTitle(){
    let { label } = this.props;
    if (this.props.messageHeader || this.props.chatHeader) {
      return <Text style={{fontSize: fontSize16, fontWeight: '500', color: '#000', top: 3}}>{label}</Text>
    }
    if (!this.props.onlyLogo && !this.props.profileHeader) {
      return <Text style={{fontSize: fontSize16, fontWeight: '500', color: whiteColor}}>{label}</Text>
    }
  }

  renderPowerButton(){
    
    if (this.props.chatHeader) {
      return <TouchableOpacity style={{position: 'absolute', right: 15, height: 20, width: 20, top: 22}} onPress={this.handleBack}>
                <Image source={require('../../assets/images/infoIcon.png')} style={{width: '100%', height: '100%'}} />
              </TouchableOpacity>
    }


    if (this.props.profileHeader) {
      return <TouchableOpacity style={{position: 'absolute', right: 15, height: 20, width: 20, top: 22}} onPress={this.logout}>
                <Image source={require('../../assets/images/power.png')} style={{width: '100%', height: '100%'}} />
              </TouchableOpacity>
    }
  }

  render(){
    
    return(
      <SafeAreaView style={{backgroundColor: this.props.whiteBg ? '#fff' : primaryColor}}>
        <View style={[{flexDirection: 'row', height: 60, backgroundColor: this.props.whiteBg ? '#fff' : primaryColor, justifyContent: 'center', alignItems: 'center'}, this.props.whiteBg && {borderBottomWidth: 0.3, borderColor: '#e6e6e6'}]}>
          {this.renerBackButton()}
          {this.renderLogo()}
          {this.renderTitle()}
          {this.renderPowerButton()}
        </View>
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
    editButtonTxt:{
      color: '#fff',
      fontSize: 20,
      fontWeight: '600'
    },
    previousScreen: {
      color: '#fff',
      position: 'absolute',
      left: 35,
      fontSize: 20,
      top: -1
    }
})