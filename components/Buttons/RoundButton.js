import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { whiteColor, fontSize19, primaryColor, fontSize15 } from '../../redux/actions/constant';

export default class RoundButton extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation, label, buttonStyle, textStyle, onPress, mainView } = this.props;
        return(
            <View style={[styles.alignRowGoals, mainView]}>
                <TouchableOpacity style={[styles.buttonView,buttonStyle]} onPress={onPress}>
                    <Text style={[styles.signInText,textStyle]}>{label}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    alignRowGoals: {
        width: '100%',
        elevation: 0,
        shadowColor: "#000000",
        shadowOpacity: 0.4,
        shadowRadius: 5,
        shadowOffset: { height: 2, width: 1 },
        borderRadius: 5,
        marginBottom: 20,
    },
    buttonView: {
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: whiteColor,
        overflow: 'hidden'
    },
    signInText: {
        color: primaryColor,
        fontSize: fontSize15,
    }

})