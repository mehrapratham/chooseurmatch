import React from 'react'
import { StyleSheet, ActivityIndicator, View, Dimensions } from 'react-native';
import { connect } from 'react-redux'
import { primaryColor } from '../../redux/actions/constant';
var { height, width } = Dimensions.get('window');
class showLoader extends React.Component {
    render() {
        return (
            <View style={styles.conMain}>
                <View style={styles.innerContainer}>
                </View>
                <View 
                style={styles.loaderBack}>
                    <ActivityIndicator size="large" color={primaryColor} />
                </View>
            </View>
        )
    }
}
export default connect(state => state)(showLoader)

const styles = StyleSheet.create({
    loaderBack:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 10,
        height: height,
    },
    conMain:{
        position: 'absolute', 
        height: height ,
        width: '100%' 
    },
    innerContainer:{ 
        justifyContent: 'center', 
        opacity: 0.5, 
        backgroundColor: 'black', 
        height: height, 
        width: '100%'
    }
});
