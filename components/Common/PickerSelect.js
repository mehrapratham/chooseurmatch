import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import { connect } from 'react-redux';
import RoundButton from '../Buttons/RoundButton';
import ScrollPicker from './ScrollPicker';
import { primaryColor, whiteColor, greyColor } from '../../redux/actions/constant';
import { dataSource } from '../../json/json';

class PickerSelect extends React.Component {
    constructor() {
        super()
        
    }
    render() {
        return (
            <View>
              
                <Dialog
                    visible={this.props.modalVisible}
                    dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                    })}
                    onTouchOutside={() => {
                        this.props.toggleModal(false)
                    }}
                    dialogStyle={{backgroundColor: '#fff',width: '100%',borderRadius: 0,padding: 0}}
                    containerStyle={{justifyContent: 'flex-end',padding: 20,paddingBottom: 0,width: '100%',marginBottom: -25}}
                >
                    {/* <TouchableOpacity onPress={() => {this.props.toggleModal(false)}} style={{position:'absolute', top:5,right:10, zIndex:99999}}>
                        <Text style={{color:'orange', fontSize:20}}>X</Text>
                    </TouchableOpacity> */}
                    <DialogContent>
                        <View style={{height: 500}}>
                        <View style={{flexDirection: 'row',height: 500,backgroundColor: 'red',marginLeft: -20,marginRight: -20}}>
                            <View style={{width: '50%',borderRightWidth: 4,borderColor: '#dadada'}}>
                                <View style={{height: 40,position: 'absolute',top: 0,zIndex:100, backgroundColor: '#e6e6e6', width: '100%',justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{fontWeight: 'bold'}}>FEET</Text>
                                </View>
                                <ScrollPicker
                                    ref={(sp) => {this.sp = sp}}
                                    dataSource={dataSource}
                                    selectedIndex={this.props.selectedIndex - 1}
                                    itemHeight={40}
                                    wrapperHeight={500}
                                    wrapperColor="#fff"
                                    highlightColor="#fff"
                                    renderItem={(data, index, isSelected) => {
                                        return(
                                            <View>
                                                <Text style={isSelected ? {color: '#fff',fontWeight: '600'} : {color: '#d2d0d0'}}>{data}</Text>
                                            </View>
                                        )
                                    }}
                                    onValueChange={(data, selectedIndex) => {
                                        // this.props.onChangeDataSource(data)

                                    }}
                                />
                            </View>
                            <View style={{width: '50%'}}>
                                <View style={{height: 40,position: 'absolute',top: 0,zIndex:100, backgroundColor: '#e6e6e6', width: '100%',justifyContent: 'center', alignItems: 'center'}}>
                                    <Text style={{fontWeight: 'bold'}}>Inch</Text>
                                </View>
                                <ScrollPicker
                                    ref={(sp) => {this.sp = sp}}
                                    dataSource={dataSource}
                                    selectedIndex={this.props.selected - 1}
                                    itemHeight={40}
                                    wrapperHeight={500}
                                    wrapperColor={primaryColor}
                                    highlightColor="#fff"
                                    renderItem={(data, index, isSelected) => {
                                        return(
                                            <View>
                                                <Text style={isSelected ? {color: '#fff',fontWeight: '600'} : {color: '#d2d0d0'}}>{data}</Text>
                                            </View>
                                        )
                                    }}
                                    onValueChange={(data, selectedIndex) => {
                                        // this.props.onChageRightSource(data)
                                    }}
                                />
                            </View>
                        </View>
                        <View style={{position: 'absolute',bottom: 0,width: '100%'}}>
                            <RoundButton label="Done" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={() => this.props.toggleModal(false)}/>
                        </View>
                            
                        </View>
                    </DialogContent>
                </Dialog>
            </View>

        )
    }
}
export default connect(state => state)(PickerSelect)

const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    }

})


