import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, Keyboard, SafeAreaView, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { Container, Content, Drawer, Header, Icon } from 'native-base';
import RoundButton from '../Buttons/RoundButton';
import MainFooter from '../Footer'
import { menuItems } from '../../redux/actions/constant';
import Dialog, { SlideAnimation, DialogContent } from 'react-native-popup-dialog';

var { height, width } = Dimensions.get('window');

export default class DrawerMenu extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      menuItems: [
          {
            label: 'Home',
            iconUrl: require('../../assets/images/avatar.png'),
            width: 18
          },
          {
            label: 'Inbox',
            iconUrl: require('../../assets/images/received.png'),
            width: 20
          },
          {
            label: 'Search',
            iconUrl: require('../../assets/images/searchIcon.png'),
            width: 20
          },
          {
            label: 'My Match',
            iconUrl: require('../../assets/images/parents.png'),
            width: 20
          },
          {
            label: 'Contact Us',
            iconUrl: require('../../assets/images/phone-book.png'),
            width: 18
          },
          {
            label: 'Settings',
            iconUrl: require('../../assets/images/settings.png'),
            width: 18
          }
        ],
        userDetail: {},
        modalVisible: false
    }
  }

    async componentDidMount(){
      let user = await AsyncStorage.getItem('user')
      this.setState({ userDetail: JSON.parse(user) })
    }

    closeDrawer = () => {
      this.setState({ modalVisible: false })
    };
    openDrawer = () => {
      this.setState({ modalVisible: true })
    };

    renderUser(){
      let { userDetail } = this.state;
      return <View>
        <View style={styles.matchCard}>
            <View style={styles.userImgCon}>
              <Image source={userDetail.profile_image ? {uri: userDetail.profile_image} :require('../../assets/images/card_img.png')} style={{width: '100%', height: 100}} />
              <Image source={require('../../assets/images/frameRed.png')} style={styles.frame} />
            </View>
            <View style={[styles.userInfoCon]}>
              <Text style={styles.userName}>{userDetail.nickname}</Text>
              <Text style={styles.userInfoText}>{userDetail.age} year, {userDetail.height}”</Text>
            </View>
        </View>
      </View>
    }

    onItemPress = (val) => {
      let { navigation } = this.props;
      if(val === 'Home') {
        navigation.navigate('Dashboard')
      }
      if(val === 'Inbox') {
        navigation.navigate('Messages')
      }
      if(val === 'Search') {
        navigation.navigate('YourMatch')
      }
      
      if(val === 'My Match') {
        navigation.navigate('NewMatch')
      }
      if(val === 'Contact Us') {
        navigation.navigate('ContactUs')
      }
      if(val === 'Settings') {
        navigation.navigate('Settings')
      }
      this.setState({ modalVisible: false})
    }

    renderMenuItems(){
      return <View style={{zIndex:20, position: 'absolute', top: 100, left: 20, width: '100%'}}>
        {this.renderUser()}
        {menuItems.map((item,key)=>{
          let imageStyle = {width: item.width, height: '100%'}
          return  <View key={key}>
                    <TouchableOpacity  style={{marginBottom:10, width: 200, height: 40, justifyContent: 'center'}} onPress={() => this.onItemPress(item.label)}>
                      <View style={{width: 30, height: 20, top: 4}}>
                        <Image source={item.iconUrl} style={imageStyle} />
                      </View>
                      <Text style={{color: '#fff', fontSize: 18, position: 'absolute', left: 45}}>{item.label}</Text>
                    </TouchableOpacity>
                  </View>
        })}
      </View>
    }

    renderMenuContent(){
      let { navigation } = this.props;
      return  <Container>
                {this.props.header}
                {this.props.child}
                <MainFooter openDrawer={this.openDrawer} navigation={navigation} />
              </Container>
    }
    render(){

        return(
            <View style={{flex: 1}} >
                {/* <Drawer
                  ref={(ref) => { this.drawer = ref; }}
                  content={<View style={{width: 430, height: '100%', top:0, zIndex: 20, flex: 1, left: -150}}>
                            <Image source={require('../../assets/images/side-menu-bg.png')} style={{width: '100%', height: '100%'}} />
                            {this.renderMenuItems()}
                          </View>}
                  onClose={() => this.closeDrawer()}
                  tapToClose={true}
                  openDrawerOffset={0.1}
                  >
                  {this.renderMenuContent()}
                </Drawer> */}
                <Dialog
                    visible={this.state.modalVisible}
                    dialogAnimation={new SlideAnimation({
                    slideFrom: 'left',
                    })}
                    onTouchOutside={() => {
                      this.closeDrawer()
                    }}
                    dialogStyle={{backgroundColor: 'transparent',width: '100%',borderRadius: 0,padding: 0}}
                    containerStyle={{justifyContent: 'flex-end',paddingBottom: 0,width: '100%',marginBottom: -25}}
                >
                    {/* <TouchableOpacity onPress={() => {this.props.toggleModal(false)}} style={{position:'absolute', top:5,right:10, zIndex:99999}}>
                        <Text style={{color:'orange', fontSize:20}}>X</Text>
                    </TouchableOpacity> */}
                    <DialogContent>
                        {/* <View style={{height: '100%', width: 300}}> */}
                          <View style={{flexDirection: 'row'}}>
                            <View style={{width:300, height: '100%', left: -20}}>
                              <Image source={require('../../assets/images/side-menu-bg.png')} style={{width: '100%', height: '100%'}} />
                              {this.renderMenuItems()}
                            </View>
                            <TouchableOpacity style={{width: width-300, left: -20}} activeOpacity={0.5} onPress={this.closeDrawer}>

                            </TouchableOpacity>
                          </View>
                        {/* </View> */}
                    </DialogContent>
                </Dialog>
                {this.renderMenuContent()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
  matchCard: {
      width: '100%',
      marginRight: 15,
      marginBottom: 40
    },
    frame: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: 100
    },
    userName: {
      fontSize: 20,
      color: '#fff',
      marginTop: 30
    },
    userInfoCon: {
      position: 'absolute',
      left: 110
    },
    userImgCon: {
      width: 100
    },
    userInfoText: {
      color: '#fff',
      fontSize: 16
    }
})