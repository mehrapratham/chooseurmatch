import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content, Footer, FooterTab } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import Tag from 'react-native-vector-icons/MaterialIcons'
var { height, width } = Dimensions.get('window');

export default class MainFooter extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        return(
          <Footer styl={styles.footerContainer}>
            <FooterTab style={styles.footerTabStyle}>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={() => navigation.navigate('Messages')}>
                  <Image source={require('../../assets/images/message-text-outline.png')} style={{width: 20, height: 20}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={() => navigation.navigate('Recomended')}>
                  <Image source={require('../../assets/images/account-multiple-outline.png')} style={{width: 30, height: 18}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity style={styles.roundBtn} onPress={() => this.props.openDrawer()} >
                  <Image source={require('../../assets/images/plus.png')} style={{width: 18, height: 18}} />
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem}>
                <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
                  {/* <Image source={require('../../assets/images/format-list-bulleted.png')} style={{width: 22, height: 20}} /> */}
                  <Tag name="notifications-none" style={{fontSize: 28, color: '#b2b2b2', marginTop: 2}}/>
                </TouchableOpacity>
              </View>
              <View style={styles.footerItem} >
                <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                  <Image source={require('../../assets/images/account-outline.png')} style={{width: 20, height: 20}} />
                </TouchableOpacity>
              </View>
              </FooterTab>
          </Footer>
        )
    }
}
const styles = StyleSheet.create({

    footerContainer: {
    	flexDirection: 'row',
    	backgroundColor: 'red'
    },
    footerItem: {
    	flex: 1,
    	alignItems: 'center',
    	justifyContent: 'center'
    },
    roundBtn: {
      width: 65,
      height: 65,
      borderRadius: 40,
      backgroundColor: primaryColor,
      alignItems: 'center',
      justifyContent: 'center',
      top: -15,

      elevation: 7,
      shadowColor: "#000000",
      shadowOpacity: 0.4,
      shadowRadius: 45,
      shadowOffset: { height: 2, width: 1 },
    },
    btnText: {
      color: '#fff',
      fontSize: 40,
      top: -3
    },
    footerTabStyle: {
      backgroundColor: '#fff'
    }
})