import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import { fontFamily, greyColor, fontSize12, blackColor } from '../../redux/actions/constant';

export default class TextField extends React.Component{
    render(){
        let { label, secureTextEntry, maxLength, onChange, value, type, onFocus } = this.props;
        return(
            <View>
                <View style={{flexDirection: 'row'}}>
                    <Text style={styles.simpleText}>{label}</Text>
                    <View style={styles.absStarView}><Text style={styles.starText}>*</Text></View>
                </View>
                <TextInput placeholder="" style={styles.textInput} secureTextEntry={secureTextEntry} autoCapitalize = 'none' maxLength={maxLength} onChangeText={onChange} value={value} keyboardType={type} onFocus={onFocus}/>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    absStarView: {
        marginLeft: 3
    },
    simpleText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    starText: {
        fontSize: 8
    },
    textInput: {
        borderBottomWidth: 0.5,
        height: 40,
        borderColor: greyColor,
        color: blackColor,
    }
})