import React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { whiteColor, fontSize19 } from '../../redux/actions/constant';

export default class RoundTextField extends React.Component{
    render(){
        let { placeholder, secureTextEntry, type, onChange, value, maxLength } = this.props;
        return(
            <TextInput 
                placeholder={placeholder}
                style={styles.textInput} 
                placeholderTextColor={whiteColor}
                secureTextEntry={secureTextEntry}
                keyboardType= {type}
                onChangeText={onChange}
                value={value}
                autoCapitalize = 'none'
                maxLength={maxLength}
            />
        )
    }
}
const styles = StyleSheet.create({
    textInput: {
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: whiteColor,
        paddingLeft: 20,
        paddingRight: 20,
        textAlign: 'center',
        color: whiteColor,
        fontSize: fontSize19
    }

})