import types from "./ActionTypes";

export const saveUserAuth = data => {
  return {
    type: types.SAVE_USER_AUTH,
    payload: data
  };
};

