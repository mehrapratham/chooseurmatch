// export const API_URL = 'https://thinkmatters.softuvo.xyz/api';

// export const API_CHAT_URL = 'http://localhost:3003';

export const API_URL = 'https://chooseurmatch.com/wp-content/plugins/chooseurmatchapi/chooseurmatchapi.php';

// fontSizes
export const fontSize15 = 15;
export const fontSize19 = 19;
export const fontSize12 = 12;
export const fontSize13 = 13;
export const fontSize16 = 16;
export const fontSize14 = 14;
export const fontSize10 = 10;
export const fontSize22 = 22;
export const fontSize21 = 21;
export const fontSize9 = 9;
export const fontSize18 = 18;
export const fontSize11 = 11;
export const fontSize24 = 24;
export const fontSize32 = 28;
export const fontSize8 = 8;


// colors
export const primaryColor = '#e11f3b';
export const whiteColor = '#fff';
export const blackColor = '#000';
export const greyColor = '#515250';


// font-family

export const fontFamily = 'verdana';


export const menuItems = [
    {
      label: 'Home',
      iconUrl: require('../../assets/images/avatar.png'),
      width: 18
    },
    {
      label: 'Inbox',
      iconUrl: require('../../assets/images/received.png'),
      width: 20
    },
    {
      label: 'Search',
      iconUrl: require('../../assets/images/searchIcon.png'),
      width: 20
    },
    {
      label: 'My Match',
      iconUrl: require('../../assets/images/parents.png'),
      width: 20
    },
    {
      label: 'Contact Us',
      iconUrl: require('../../assets/images/phone-book.png'),
      width: 18
    },
    {
      label: 'Settings',
      iconUrl: require('../../assets/images/settings.png'),
      width: 18
    }
  ]

  export const dataArray = [
    { title: "TAGLINE", icon: require('../../assets/images/tagline.png') },
    { title: "ABOUT", icon: require('../../assets/images/about.png') },
    { title: "BASIC INFORMATION", icon: require('../../assets/images/basic_information.png') },
    { title: "PHYSICAL APPEARANCE", icon: require('../../assets/images/physical_apperance.png') },
    { title: "LOCATION", icon: require('../../assets/images/location.png') },
    { title: "ADDITIONAL INFORMATION", icon: require('../../assets/images/additional_information.png') },
    { title: "CONTACT INFORMATION", icon: require('../../assets/images/contact_information.png') },
    { title: "SECURITY", icon: require('../../assets/images/security.png') }
  ];

  export const dataArrayMyPrefrence = [
    { title: "PHYSICAL APPEARANCE", icon: require('../../assets/images/physical_apperance.png') },
    { title: "LOCATION", icon: require('../../assets/images/location.png') },
    { title: "ADDITIONAL INFORMATION", icon: require('../../assets/images/additional_information.png') }
  ];
