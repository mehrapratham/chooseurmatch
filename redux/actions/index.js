import axios from 'axios'
import { API_URL } from './constant';
import { SAVE_REGISTER_USER, OPEN_TOAST, SAVE_SEARCH_LIST } from "./ActionTypes";
import {AsyncStorage} from 'react-native'
var qs = require('qs');
export const apiCall = (data) => {
    return async dispatch => {
        return new Promise(
            (resolve, reject) => 
            axios.post(`${API_URL}`, qs.stringify(data),{
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then(res => {
                return resolve(res)
            })
            .catch((error) => {
                return reject({...error})
            })
        )
    }
}

export const apiCalls = (data) => {
    // let formData = new FormData()
    // formData.append('profile_image', {
    //     name: data.fileName,
    //     uri: data.uri,
    //     type: data.type, // or photo.type
    // });
    return async dispatch => {
        let user_id = await AsyncStorage.getItem('user_id')
        let apiData = { action: 'saveProfileImage', profile_image: data, id: user_id}
        return new Promise(
            (resolve, reject) => 
            axios.post(`${API_URL}`, qs.stringify(apiData),{
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then(res => {
                return resolve(res)
            })
            .catch((error) => {
                return reject({...error})
            })
        )
    }
}

export const openToast = data => {
    return {
        type: OPEN_TOAST,
        payload: {
            toast_msg: data
        }
    }
}

export const saveRegisterUser = data => {
    return {
      type: SAVE_REGISTER_USER,
      payload: data
    };
  };

  export const saveSearchList = data => {
    return {
      type: SAVE_SEARCH_LIST,
      payload: data
    };
  };


