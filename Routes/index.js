  import { createStackNavigator, createAppContainer } from "react-navigation";
import Dashboard from "../Views/Dashboard";
import StartScreen from "../Views/Auth/StartScreen";
import SignIn from "../Views/Auth/SignIn";
import CreateProfileFor from "../Views/Auth/CreateProfileFor";
import PersonalDetailIst from "../Views/Auth/PersonalDetailIst";
import PersonalDetailSecond from "../Views/Auth/PersonalDetailSecond";
import PersonalDetailThird from "../Views/Auth/PersonalDetailThird";
import LoginDetails from "../Views/Auth/LoginDetails";
import Verify from "../Views/Auth/Verify";
import Partner from '../Views/Partner'
import UploadScreen from '../Views/Dashboard/UploadScreen'
import Profile from '../Views/Profile'
import Messages from '../Views/Chat/Messages'
import BodyDetails from "../Views/Auth/BodyDetails";
import ChatBox from "../Views/Chat/ChatBox";
import Recomended from "../Views/Profile/Recomended";
import ResetPassword from '../Views/Auth/ResetPassword'
import ConformOtp from '../Views/Auth/ConformOtp'
import SendOtp from '../Views/Auth/SendOtp'
import ProfileViews from '../Views/Profile/ProfileViews'
import ProfileEdit from '../Views/Profile/ProfileEdit'
import Contact from '../Views/Contact'
import YourMatch from "../Views/YourMatch";
import CheckRoute from "../Views/CheckRoute";
import LikedUser from "../Views/Lists/LikedUser";
import FavouriteUser from "../Views/Lists/FavouriteUser";
import TaggedUser from "../Views/Lists/TaggedUser";
import NewMatch from "../Views/Lists/NewMatch";
import MemberLooking from "../Views/Lists/MemberLooking";
import ContactUs from '../Views/Contact/ContactUs'
import DetailPage from '../Views/Profile/DetailPage'
import Notification from '../Views/Notification'
import SearchedUser from "../Views/Lists/SearchedUser";
import Settings from "../Views/Settings";


const AppNavigator = createStackNavigator(
    {
      Dashboard: Dashboard,
      StartScreen: StartScreen,
      SignIn: SignIn,
      CreateProfileFor: CreateProfileFor,
      PersonalDetailIst: PersonalDetailIst,
      PersonalDetailSecond: PersonalDetailSecond,
      PersonalDetailThird: PersonalDetailThird,
      LoginDetails: LoginDetails,
      Verify: Verify,
      Partner: Partner,
      UploadScreen: UploadScreen,
      Profile: Profile,
      Messages: Messages,
      BodyDetails: BodyDetails,
      ChatBox: ChatBox,
      Recomended: Recomended,
      ResetPassword: ResetPassword,
      ConformOtp: ConformOtp,
      SendOtp: SendOtp,
      ProfileViews: ProfileViews,
      ProfileEdit: ProfileEdit,
      Contact: Contact,
      YourMatch: YourMatch,
      CheckRoute: CheckRoute,
      LikedUser: LikedUser,
      FavouriteUser: FavouriteUser,
      TaggedUser: TaggedUser,
      NewMatch: NewMatch,
      MemberLooking: MemberLooking,
      ContactUs: ContactUs,
      DetailPage: DetailPage,
      Notification: Notification,
      SearchedUser: SearchedUser,
      Settings: Settings
    },
    {
      initialRouteName: "CheckRoute",
      defaultNavigationOptions: {
        gesturesEnabled: false
      },
      transitionConfig: () => ({
        transitionSpec: {
          duration: 0,  // Set the animation duration time as 0 !!
        },
      }),
    }
);


const Router = createAppContainer(AppNavigator);

export default Router