export const createProfileList = [
    {
        image: require('../assets/images/user.png'),
        label: 'Self'
    },
    {
        image: require('../assets/images/collaboration.png'),
        label: 'Relative'
    },
    {
        image: require('../assets/images/boy-smiling.png'),
        label: 'Son'
    },
    {
        image: require('../assets/images/smiling-girl.png'),
        label: 'Daughter'
    },
    {
        image: require('../assets/images/friendship.png'),
        label: 'Brother'
    },
    {
        image: require('../assets/images/friendship2.png'),
        label: 'Sister'
    },
    {
        image: require('../assets/images/high-five.png'),
        label: 'Friend'
    }
]

export const dataSource = [1,2,3,4,5,6,7,8,9,10,11,12] 

export const maritalData = [
    {
        label: 'Married',
        value: '1'
    },
    {
        label: 'Single',
        value: '2'
    },
    {
        label: 'Divorced',
        value: '3'
    },
    {
        label: 'Widowed',
        value: '4'
    }
]

export const personalDetailIstValidation = {
    gender: {
        required: {
            errorMsg: 'Gender is required'
        },
    },
    dob: {
        required: {
            errorMsg: 'Dob is required'
        },
    },
    country: {
        required: {
            errorMsg: 'Country is required'
        },
    },
    state: {
        required: {
            errorMsg: 'State is required'
        },
    },
    city: {
        required: {
            errorMsg: 'City is required'
        },
    }
}

export const personalDetailSecondValidation = {
    occupation: {
        required: {
            errorMsg: 'Occupation is required'
        },
    },
    education: {
        required: {
            errorMsg: 'Education is required'
        },
    },
    income: {
        required: {
            errorMsg: 'Income is required'
        },
    }
}

export const signInValidation = {
    email: {
        required: {
            errorMsg: 'Email is required'
        },
        email: {
            errorMsg: 'Email is not valid'
        }
    },
    password: {
        required: {
            errorMsg: 'Password is required'
        },
    }
}

export const personalDetailThirdValidation = {
    marital_status: {
        required: {
            errorMsg: 'Marital_Status is required'
        },
    },
    mother_tongue: {
        required: {
            errorMsg: 'Language is required'
        },
    },
    religion: {
        required: {
            errorMsg: 'Community is required'
        },
    },
    live_in: {
        required: {
            errorMsg: 'Live_in is required'
        },
    }
}

export const loginValidation = {
    first_name: {
        required: {
            errorMsg: 'First Name is required'
        },
    },
    last_name: {
        required: {
            errorMsg: 'Last Name is required'
        },
    },
    email: {
        required: {
            errorMsg: 'Email is required'
        },
        email: {
            errorMsg: 'Email is not valid'
        }
    },
    user_id: {
        required: {
            errorMsg: 'UserId is required'
        },
    },
    mobile: {
        required: {
            errorMsg: 'Mobile is required'
        },
    },
    password: {
        required: {
            errorMsg: 'Password is required'
        },
    },
    security_answer: {
        required: {
            errorMsg: 'Security Answer is required'
        },
    },
    security_question: {
        required: {
            errorMsg: 'Security Question is required'
        },
    }
}

