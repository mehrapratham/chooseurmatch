import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, ScrollView } from 'react-native';
import { Container, Content, Footer, Card, CardItem, Right, Icon, Left, Segment, Button, Fab } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11 } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import DrawerMenu from '../../components/Common/DrawerMenu'
var { height, width } = Dimensions.get('window');

export default class Contact extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        let child = <Content contentContainerStyle={{flex: 1}}>
                    <View style={styles.topRedView}>
                        <SignupHeader navigation={navigation} previousScreen="Contacts" messageHeader />
                        <View style={styles.segmentOuter}>
                           <TextInput placeholder="Search Friends" style={styles.searchBox} /> 
                        </View>
                    </View>
                    <View style={styles.contentView}>
                    <ScrollView style={{height: height - 260, paddingTop:20}}>
                        <View style={styles.listContainer}>
                            <View style={styles.listHearer}>
                                <Text style={styles.headerText}>Online(</Text><Text style={styles.headerTextRed}>5</Text><Text style={styles.headerText}>)</Text>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listHearer}>
                                <Text style={styles.headerText}>Online(</Text><Text style={styles.headerTextRed}>5</Text><Text style={styles.headerText}>)</Text>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.listItem}>
                                <View>
                                    <View style={styles.listUserCon}>
                                        <Image source={require('../../assets/images/userImg.png')} style={{width: 40, height: 40}} />
                                    </View>
                                    <View style={styles.onlineStatus}></View>
                                </View>
                                <Text style={styles.listUserText}>Oswald Cobblepot</Text>
                                <View style={styles.buttons}>
                                    <TouchableOpacity style={styles.infoButton}>
                                        <Image source={require('../../assets/images/informationGray.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.messageIcon}>
                                        <Image source={require('../../assets/images/message.png')} style={{width: 25, height: 25}} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        
                    </ScrollView>
                    </View>
                </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
const styles = StyleSheet.create({
    topRedView: {
        paddingBottom: 50,
        backgroundColor: primaryColor
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15
    },
    
    contentView: {
        padding: 15,
        marginTop: -60,
        position: 'absolute',
        top: 220,
        width: '100%'
    },
    searchBox: {
        height: 40,
        textAlign: 'center',
        width: '100%'
    },
    listHearer: {
        backgroundColor: '#f8f8f8',
        padding: 10,
        paddingLeft: 15,
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderColor: '#e0e0e0'
    },
    listContainer: {
        borderRadius: 5,
        overflow: 'hidden'
    },
    headerText: {
        color: greyColor,
        fontSize: 18
    },
    headerTextRed: {
        color: primaryColor,
        fontSize: 18
    },
    listItem: {
        backgroundColor: '#fff',
        padding: 15,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomWidth: 0.5,
        borderRightWidth: 0.5,
        borderLeftWidth: 0.5,
        borderColor: '#e0e0e0',
        flexDirection: 'row'
    },
    listUserCon: {
        width: 40,
        height: 40,
        borderRadius: 40,
        overflow: 'hidden',
        position: 'relative'
    },
    listUserText: {
        color: '#363636',
        left: 10,
        top: 12
    },
    onlineStatus: {
        position: 'absolute',
        width: 15,
        height: 15,
        borderRadius: 15,
        backgroundColor: 'red',
        borderWidth: 2,
        borderColor: '#fff',
        right: -5,
        bottom: -2
    },
    infoButton: {
        marginRight: 20
    },
    messageIcon: {
        marginRight: 10
    },
    buttons: {
        flexDirection: 'row',
        position: 'absolute',
        right: 15,
        top: 25
    }
    
})