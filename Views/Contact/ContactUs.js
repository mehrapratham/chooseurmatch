import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, Keyboard, AsyncStorage } from 'react-native';
import { Container, Content, Accordion, Icon, Button } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11, dataArray, dataArrayMyPrefrence } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import { connect } from 'react-redux'
import { apiCalls, apiCall, openToast } from '../../redux/actions';
import RNPickerSelect from 'react-native-picker-select';
import ShowLoader from '../../components/ShowLoader';
import DrawerMenu from '../../components/Common/DrawerMenu';
var { height, width } = Dimensions.get('window');

class ContactUs extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        loader: false,
        activeIndex: 1,
        contact: {
            name: '',
            contact_subject: '',
            contact_email: '',
            message: ''
        },
        userDetail: {}
    }
    async componentDidMount(){
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        this.setState({ userDetail: user })
    }

    onChange = (key, event) => {
        let {contact} = this.state;
        contact[key] = event.nativeEvent.text;
        this.setState({contact})
    }

    onSubmit(data){
        this.setState({loader: true})
        let {contact} = this.state;
        contact.action = 'contactUs'
        this.props.dispatch(apiCall(contact)).then(res => {
            if (res.data.status == 'error') {
                this.props.dispatch(openToast(res.data.message))
                this.setState({loader: false})
            } else {
                contact = {
                    name: '',
                    contact_subject: '',
                    contact_email: '',
                    message: ''
                }
                this.setState({contact, loader: false})
                this.props.dispatch(openToast(res.data[0].message))
            }
            
        });
    }

    setActiveIndex = (index) => {
        this.setState({ activeIndex: index })
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { activeIndex, contact, userDetail } = this.state;
        let child = <Content>
                        <View style={{flex: 1}}>
                            <View style={styles.topRedView}>
                                <SignupHeader navigation={navigation} label="Contact Us" />
                                <View style={styles.imageCenterView}>
                                    <View style={styles.userImgCon}>
                                    <Image source={userDetail.profile_image ? {uri: userDetail.profile_image} : require('../../assets/images/card_img.png')} style={{width: '100%', height: 80}} />
                                        <Image source={require('../../assets/images/framePrimary.png')} style={styles.frame} />
                                    </View>
                                </View>
                                <View style={styles.segmentOuter}>
                                    <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                        <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>Get in touch</Text>
                                    </TouchableOpacity>
                                    
                                </View>
                            </View>
                            {activeIndex === 1 && <View style={{ padding: 15, marginTop: 20}}>
                                <View style={{backgroundColor: '#ededed', padding: 10}}>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%', marginBottom: 20}}>
                                        <View style={{position: 'absolute', left: 0, top: 12, width: 80}}>
                                            <Text style={{fontSize: fontSize13}}>Name:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <TextInput value={contact.name} onChange={this.onChange.bind(this, 'name')} style={{backgroundColor: whiteColor, minHeight: 40, borderRadius: 5, paddingLeft: 10, paddingRight: 10, fontSize: fontSize13}} />
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%', marginBottom: 20}}>
                                        <View style={{position: 'absolute', left: 0, top: 12, width: 80}}>
                                            <Text style={{fontSize: fontSize13}}>Email:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <TextInput value={contact.contact_email} onChange={this.onChange.bind(this, 'contact_email')} style={{backgroundColor: whiteColor, minHeight: 40, borderRadius: 5, paddingLeft: 10, paddingRight: 10, fontSize: fontSize13}} />
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%', marginBottom: 20}}>
                                        <View style={{position: 'absolute', left: 0, top: 12, width: 80}}>
                                            <Text style={{fontSize: fontSize13}}>Subject:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <TextInput value={contact.contact_subject} onChange={this.onChange.bind(this, 'contact_subject')} style={{backgroundColor: whiteColor, minHeight: 40, borderRadius: 5, paddingLeft: 10, paddingRight: 10, fontSize: fontSize13}} />
                                        </View>
                                    </View>
                                    <View style={{height: 110, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 12, width: 80}}>
                                            <Text style={{fontSize: fontSize13}}>Message:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <TextInput value={contact.message} onChange={this.onChange.bind(this, 'message')} multiline={true} style={{backgroundColor: whiteColor, minHeight: 100, borderRadius: 5, paddingLeft: 10, paddingRight: 10, fontSize: fontSize13}} />
                                        </View>
                                    </View>
                                    <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 15, marginBottom: 10}} onPress={this.onSubmit.bind(this)}>
                                        <Text style={{color: whiteColor, fontSize: fontSize12, fontWeight: '500'}}>Send</Text>
                                    </Button>
                                </View>
                            </View>}
                        </View>
                        {this.handelLoader()}
                    </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(ContactUs)
const styles = StyleSheet.create({
    topRedView: {
        backgroundColor: primaryColor
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15,
        marginBottom: -20,
        borderWidth: 1,
        borderColor: '#e6e6e6'
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    contentView: {
        padding: 15,
        width: '100%',
        marginTop: 40,
        paddingTop: 0
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize8,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      position: 'relative'
    },
    halfImageCon: {
      width: 90,
      height: 100
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    marginBottom20: {
      marginBottom: 10
    },
    nameCardText: {
      color: primaryColor,
      fontSize: fontSize16,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    pIdText: {
      color: '#525252',
      fontSize: fontSize8,
      fontFamily: fontFamily
    },
    rightBottom: {
        position: 'absolute', 
        bottom:10, 
        margin: 0, 
        right: 10,
        height: 35,
        width: 35
    },
    rightBottomIcon: {
       width: 18,
       height: 18 
    },
    rightTop: {
        position: 'absolute',
        top: 4,
        right: 5,
        width: 35,
        height: 35
    },
    moreIcon: {
        fontSize: 40
    },
    userImgCon: {
      width: 80,
      alignItems: 'center',

    },
    frame: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: 80
    },
    imageCenterView: {
        width: '100%',
        alignItems: 'center'
    },
    innerContentView: {
        backgroundColor: whiteColor,
        borderWidth: 1,
        borderColor: '#e6e6e6',
        borderRadius: 5
    },
    innerViewAcc: {
        margin: 10,
        marginBottom: 0
    }
})