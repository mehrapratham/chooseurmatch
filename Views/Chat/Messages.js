import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Button, Icon, List, ListItem, Segment } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize9, fontSize18, fontSize11, fontSize24, fontSize32 } from '../../redux/actions/constant';
import MainFooter from '../../components/Footer'
import SignupHeader from '../../components/Headers/SignupHeader';
import DrawerMenu from '../../components/Common/DrawerMenu'
import { connect } from 'react-redux'
import { apiCall } from '../../redux/actions';
import ShowLoader from '../../components/ShowLoader';
import moment from 'moment'
import ListView from 'deprecated-react-native-listview'

class Messages extends React.Component{
    static navigationOptions = {
        header: null
		}
		constructor(props) {
			super(props);
			this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
			this.state = {
				listViewData: [],
			};
		}

		async componentDidMount(){
			let from_user_id = await AsyncStorage.getItem('user_id') || ''
			this.setState({loader: true})
			this.props.dispatch(apiCall({action: 'MessageUsersList', user_id: from_user_id})).then(res => {
				this.setState({loader: false})
				this.setState({ listViewData: res.data.data })
			}).catch(err => {
				this.setState({loader: false})
			})
		}



		async deleteRow(secId, rowId, rowMap, to_user_id) {
			let from_user_id = await AsyncStorage.getItem('user_id') || ''
			rowMap[`${secId}${rowId}`].props.closeRow();
			const newData = [...this.state.listViewData];
			newData.splice(rowId, 1);
			this.setState({ listViewData: newData, loader: true });
			this.props.dispatch(apiCall({action: 'deleteConversation', from_user_id, to_user_id })).then(res => {
				this.setState({loader: false})
			}).catch(err => {
				this.setState({loader: false})
			})
		}

		handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
		
    render(){
				let { navigation } = this.props;
				const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
				let child = <Content>
					<SignupHeader navigation={navigation} label="Messages" messageHeader previousScreen="true"  whiteBg />
							
							{this.state.listViewData.length ? <List
								rightOpenValue={-75}
								disableRightSwipe
								dataSource={this.ds.cloneWithRows(this.state.listViewData)}
								renderRow={data =>
									<TouchableOpacity style={{padding: 10, backgroundColor: '@3e3e3e'}} activeOpacity={0.7} onPress={() => navigation.navigate('ChatBox',{to_user_id: data.to_user_id})}>
										<View style={{height: 60,paddingLeft: 90,justifyContent: 'center',paddingRight: 90}}>
											<View style={styles.avatar}>
												<Image source={data.to_user_profile ? {uri: data.to_user_profile} : require('../../assets/images/card_img.png')} style={styles.imageStyle}/>
											</View>
											<View>
												<Text style={styles.nameText}>{data.to_user_name}</Text>
												<Text style={styles.desText} numberOfLines={1}>{data.message} </Text>
											</View>
											<View style={styles.timeView}>
												<Text style={styles.timeText}>{moment(data.create_date).format('hh:mm A')}</Text>
											</View>
										</View>	
									</TouchableOpacity>
								}
								
								renderRightHiddenRow={(data, secId, rowId, rowMap) =>
									<View style={{flexDirection: 'row', height: 80}}>
										
										<Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap, data.to_user_id)} style={{height: 80}}>
											<View style={{flexDirection: 'column'}}>
												<Icon active name="trash" style={{color: '#fff', fontSize: fontSize24, fontWeight: '500'}} />
												<Text style={{color: '#fff', fontSize: fontSize12, fontWeight: '500', textAlign: 'center'}}>Delete</Text>
											</View>
										</Button>
									</View>
									
									
								}
							/>: <View style={{marginTop: 20}}><Text style={{color: primaryColor, textAlign: 'center'}}>No Record Found!</Text></View>}
							{this.handelLoader()}
              </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(Messages)
const styles = StyleSheet.create({
	avatar: {
		position: 'absolute',
		height: 60,
		width: 60,
		left: 10,
		top: 0,
		borderRadius: 30,
		overflow: 'hidden'
	},
	imageStyle: {
		width: '100%',
		height: '100%'
	},
	nameText: {
		color: '#363636',
		fontSize: fontSize16,
		fontFamily: fontFamily,
		fontWeight: '500',
		marginBottom: 5
	},
	desText: {
		color: '#b9b9b9',
		fontSize: fontSize12,
		fontFamily: fontFamily
	},
	timeView: {
		position: 'absolute',
		right: 15,
		top: 10
	},
	timeText: {
		color: '#b9b9b9',
		fontSize: fontSize14,
		fontFamily: fontFamily
	}
})