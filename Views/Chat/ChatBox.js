import React from 'react'
import { Text, View, TextInput, ScrollView, Dimensions, KeyboardAvoidingView, Linking, StyleSheet, Image, Platform, TouchableOpacity, AsyncStorage } from 'react-native';
import { connect } from 'react-redux'
import { Container, Content } from 'native-base';
import { primaryColor, fontSize16, fontFamily, fontSize12, fontSize14 } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import { apiCalls, apiCall, openToast } from '../../redux/actions';
import ShowLoader from '../../components/ShowLoader';

var { height, width } = Dimensions.get('window');

class Chat extends React.Component {

    constructor() {
        super()
        this.state = {
            toggle: false,
            chatBox: '',
            ChatArr: [],
            from_user_id: ''
        }
    }
    static navigationOptions = {
        header: null
    }

    async componentDidMount(){
        this.setState({loader: true})
        let { to_user_id, to_user_name } = this.props.navigation && this.props.navigation.state && this.props.navigation.state.params || ''
        let from_user_id = await AsyncStorage.getItem('user_id') || ''
        this.props.dispatch(apiCall({ action: 'AllMessageList', from_user_id, to_user_id })).then(res => {
            this.setState({loader: false})
            if(!res.data.data){
                this.setState({ ChatArr: [], from_user_id })
            }else{
                this.setState({ ChatArr: res.data.data, from_user_id })
            }
        }).catch(err => {
            this.setState({loader: false})
        })
    }


    handelChnage(key, event) {
        let { chatBox } = this.state
        chatBox = event
        this.setState({ chatBox, errors: {} })
    }

    async sendMessage(){
        let {chatBox, ChatArr} = this.state;
        if (this.state.chatBox) {
            let { to_user_id } = this.props.navigation.state.params || ''
            let from_user_id = await AsyncStorage.getItem('user_id')
            let messageObject = {message: chatBox, from_user_id}
            ChatArr.push(messageObject)
            chatBox = '';
            this.setState({chatBox, ChatArr})
            this.props.dispatch(apiCall({action: "saveMessage", from: from_user_id, to: to_user_id, message: messageObject.message})).then(res => {
            });
        }
        
    }

    handleChatMessages(val, index) {
        let { from_user_id } = this.state;
        if (val.from_user_id == from_user_id) {
            return (
                
                <View key={index} style={styles.sentMsgOutMain}>
                    <View style={styles.sentMsgOut}>
                        <Text style={styles.sentMsg}>
                            {val.message}
                        </Text>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.messageOutImage}>
                    <View style={styles.profileImageOut}>
                        <Image style={styles.userImage} source={val.to_user_profile ? {uri: val.to_user_profile} : require('../../assets/images/card_img.png')} />
                    </View>
                    <View style={styles.recieveMsgOutMain}>
                        <View style={styles.recieveMsgOut}>
                            <Text style={styles.recieveMsg}>
                                {val.message}
                            </Text>
                        </View>
                    </View>
                </View>

            )
        }
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }

    render() {
        let { chatBox } = this.state
        let { navigation } = this.props;
        return (
            <Container>
                <SignupHeader navigation={navigation} chatHeader label={' '} whiteBg />
                <KeyboardAvoidingView
                    contentContainerStyle={{ height: '100%' }}
                    behavior={Platform.OS == 'ios' ? 'position' : ''}
                    style={{ flex: 1 }}>
                        <ScrollView
                            ref={ref => this.scrollView = ref}
                            onContentSizeChange={(contentWidth, contentHeight) => {
                                this.scrollView.scrollToEnd({ animated: false });
                            }}>
                            <View style={{ paddingLeft: 5, paddingRight: 5 }}>
                                {
                                    this.state && this.state.ChatArr.map((val, index) => {
                                        return (
                                            this.handleChatMessages(val, index)
                                        )
                                    })
                                }
                            </View>
                        </ScrollView>
                    <View style={styles.textFieldOut}>
                        <TextInput
                            value={chatBox}
                            placeholder=" Your Message..."
                            multiline={true}
                            style={styles.textField}
                            onChangeText={this.handelChnage.bind(this, 'chatBox')} />

                        <View style={{
                            position: 'absolute',
                            zIndex: 999999,
                            right: 0,
                            height:50, 
                            width:50
                        }}>
                        <TouchableOpacity style={{ zIndex:999, height: 120}} onPress={this.sendMessage.bind(this)}>
                            <Image style={styles.imageMain} source={require('../../assets/images/send.png')} />
                        </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
                {this.handelLoader()}
                {/* </View> */}
            </Container>
        )
    }
}

export default connect(state => state)(Chat)

const styles = StyleSheet.create({
    widthAdjust: {
        position: 'absolute',
        right: 20,
        top: -5,
        width: 15,
        height: 18
    },
    shadowTopMargin: {
        marginTop: 5,
    },
    sentMsgOutMain: {
        minWidth: 70,
        borderRadius: 10,
        margin: 10,
        marginLeft: 0,
        backgroundColor: '#e6e6e6',
        alignSelf: 'flex-end',
        shadowColor: '#000',
        shadowOffset: { width: 7, height: 7 },
        shadowOpacity: 0.2,
        shadowRadius: 8,
        elevation: 7,
        borderBottomRightRadius: 0
    },
    sentMsgOut: {
        margin: 3,
        overflow: 'hidden'
    },
    sentMsg: {
        padding: 5,
        paddingLeft: 11,
        paddingRight: 11,
        borderRadius: 50,
        maxWidth: width - 130,
        color: '#000',
        fontSize: fontSize14,
        fontFamily: fontFamily
    },
    recieveMsgOutMain: {
        minWidth: 70,
        borderRadius: 10,
        margin: 10,
        marginLeft: 0,
        backgroundColor: primaryColor,
        alignSelf: 'flex-start',
        shadowColor: '#000',
        shadowOffset: { width: 7, height: 7 },
        shadowOpacity: 0.2,
        shadowRadius: 8,
        elevation: 7,
        borderBottomLeftRadius: 0
    },
    recieveMsgOut: {
        padding: 5,
        alignSelf: 'flex-start',
        borderRadius: 15,
        overflow: 'hidden',
        margin: 3
    },
    recieveMsg: {
        paddingTop: 5,
        paddingBottom: 0,
        paddingLeft: 11,
        paddingRight: 11,
        maxWidth: width - 140,
        color: '#fff',
        fontSize: fontSize14,
        fontFamily: fontFamily
    },
    timeMainSend: {
        color: "#fff",
        fontSize: 9,
        alignSelf: "flex-end",
        marginRight: 10,
        marginBottom: 5
    },
    timeMainRecieve: {
        color: "#7f7e7e",
        fontSize: 9,
        alignSelf: "flex-end",
        marginRight: 10,
        marginBottom: 5
    },
    textField: {
        padding: 10,
        width: '100%',
        backgroundColor: "#f2f2f2",
        paddingLeft: 15,
        paddingRight: 10,
        borderWidth: 0,
        paddingTop: 10,
        minHeight: 60,
        justifyContent: 'center'
    },
    textFieldOut: {
        maxHeight: 120,
        position: 'relative',
        padding: 8,
        justifyContent: 'center',
        paddingRight: 50,
        backgroundColor: primaryColor,
        borderWidth: 0.5,
        borderColor: '#bfbfbf',
        minHeight: 60,
        padding: 0
    },
    imageMain: {
        height: 25,
        width: 24,
        alignSelf: 'center',
        marginTop: 12
    },
    iconOut: {
        position: 'absolute',
        height: 30,
        width: 30,
        left: 10
    },
    messageOutImage: {
        position: 'relative',
        paddingLeft: 60,
        justifyContent: "flex-end"
    },
    profileImageOut: {
        position: "absolute",
        height: 35,
        width: 35,
        left: 8,
        borderWidth:0.5,
        borderColor:'#ccc',
        borderRadius:20,
        overflow:'hidden',
        bottom: 10
    },
    userImage: {
        height: '100%',
        width: '100%'
    }
})