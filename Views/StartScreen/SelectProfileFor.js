import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, SafeAreaView, Image } from 'react-native';
import CombinedButton from '../../components/Buttons/CombinedButton';
import { whiteColor } from '../../redux/actions/constant';
import {Container, Header, Left, Body, Right, Icon, Button, Title} from 'native-base'
var { height, width } = Dimensions.get('window');

export default class SelectProfileFor extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        return(
          <Container>
            <SafeAreaView style={{backgroundColor: 'blue'}}>
              <Header style={styles.header} contentContainerStyle={{height: 40}}>
                <Button transparent style={styles.backButton}>
                  <Image source={require('../../assets/images/icon-back.png')} name='arrow-back' style={styles.backButtonIcon} />
                </Button>
                <Text style={styles.headerText}>Create Profile For</Text>
              </Header>
            </SafeAreaView>
          </Container>
        )
    }
}
const styles = StyleSheet.create({
    header: {
        backgroundColor: 'red',
        padding: 0,
    },
    headerText: {
      color: '#fff',
      fontSize: 16,
      fontWeight: '600',
    },
    backButton: {
      position: 'absolute',
      left: 20,
      bottom: 12
    },
    backButtonIcon: {
      width: 28,
      height: 28
    }
    

})