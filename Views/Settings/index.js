import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, Keyboard, AsyncStorage } from 'react-native';
import { Container, Content, Accordion, Icon, Button } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11, dataArray, dataArrayMyPrefrence } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import { connect } from 'react-redux'
import { apiCalls, apiCall, openToast } from '../../redux/actions';
import RNPickerSelect from 'react-native-picker-select';
import ShowLoader from '../../components/ShowLoader';
import DrawerMenu from '../../components/Common/DrawerMenu';
import { Switch } from 'react-native-switch';
var { height, width } = Dimensions.get('window');

class Settings extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        loader: false,
        activeIndex: 1,
        userDetail: {},
        email_setting: false,
        phone_setting: false,
        message_setting: false
    }
    async componentDidMount(){
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        this.setState({ userDetail: user })
    }

    async onSubmit(){
        this.setState({loader: true})
        let { email_setting, phone_setting, message_setting } = this.state;
        let user_id = await AsyncStorage.getItem('user_id')
        let email = (email_setting) ? '1': '';
        let phone = (phone_setting) ? '1': '';
        let message = (message_setting) ? '1': '';
        this.props.dispatch(apiCall({action: 'privacySettingUser', user_id, email_setting: email, phone_setting: phone, message_setting: message })).then(res => {
            console.log(res,"res")
            this.setState({loader: false})
            this.props.dispatch(openToast(res.data.message))
        })
    }

    setActiveIndex = (index) => {
        this.setState({ activeIndex: index })
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { activeIndex, userDetail } = this.state;
        let child = <Content>
                        <View style={{flex: 1}}>
                            <View style={styles.topRedView}>
                                <SignupHeader navigation={navigation} label="Settings" />
                                <View style={styles.imageCenterView}>
                                    <View style={styles.userImgCon}>
                                        <Image source={userDetail.profile_image ? {uri: userDetail.profile_image} : require('../../assets/images/card_img.png')} style={{width: '100%', height: 80}} />
                                        <Image source={require('../../assets/images/framePrimary.png')} style={styles.frame} />
                                    </View>
                                </View>
                                <View style={styles.segmentOuter}>
                                    <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                        <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>Privacy Settings</Text>
                                    </TouchableOpacity>
                                    
                                </View>
                            </View>
                            {activeIndex === 1 && <View style={{ padding: 15, marginTop: 20}}>
                                <View style={{backgroundColor: '#ededed', padding: 10}}>
                                    <View style={{padding: 10, paddingTop: 5, paddingLeft:0}}><Text style={{fontSize: 15, color: primaryColor, fontWeight: '600'}}>MY PRIVACY</Text></View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%', marginBottom: 10}}>
                                        <View>
                                            <Text style={{fontSize: fontSize13}}>Show email to paid user:</Text> 
                                        </View>
                                        <View style={{ position: 'absolute', right: 10, top: -2}}>
                                            <Switch 
                                                backgroundActive={primaryColor}
                                                circleSize={23}
                                                circleBorderWidth={2}
                                                value={this.state.email_setting}
                                                onValueChange={() => this.setState({ email_setting: !this.state.email_setting })}
                                            />
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%', marginBottom: 10}}>
                                        <View>
                                            <Text style={{fontSize: fontSize13}}>Show Phone to paid user:</Text> 
                                        </View>
                                        <View style={{ position: 'absolute', right: 10, top: -2}}>
                                            <Switch 
                                                backgroundActive={primaryColor}
                                                circleSize={23}
                                                circleBorderWidth={2}
                                                onValueChange={() => this.setState({ phone_setting: !this.state.phone_setting })}
                                                value={this.state.phone_setting}
                                            />
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%', marginBottom: 10}}>
                                        <View>
                                            <Text style={{fontSize: fontSize13}}>Show Message to paid user:</Text> 
                                        </View>
                                        <View style={{ position: 'absolute', right: 10, top: -2}}>
                                            <Switch 
                                                backgroundActive={primaryColor}
                                                circleSize={23}
                                                circleBorderWidth={2}
                                                onValueChange={() => this.setState({ message_setting: !this.state.message_setting })}
                                                value={this.state.message_setting}
                                            />
                                        </View>
                                    </View>
                                    <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 0, marginBottom: 10}} onPress={this.onSubmit.bind(this)}>
                                        <Text style={{color: whiteColor, fontSize: fontSize12, fontWeight: '500'}}>Save</Text>
                                    </Button>
                                </View>
                            </View>}
                        </View>
                        {this.handelLoader()}
                    </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(Settings)
const styles = StyleSheet.create({
    topRedView: {
        backgroundColor: primaryColor
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15,
        marginBottom: -20,
        borderWidth: 1,
        borderColor: '#e6e6e6'
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    contentView: {
        padding: 15,
        width: '100%',
        marginTop: 40,
        paddingTop: 0
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize8,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      position: 'relative'
    },
    halfImageCon: {
      width: 90,
      height: 100
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    marginBottom20: {
      marginBottom: 10
    },
    nameCardText: {
      color: primaryColor,
      fontSize: fontSize16,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    pIdText: {
      color: '#525252',
      fontSize: fontSize8,
      fontFamily: fontFamily
    },
    rightBottom: {
        position: 'absolute', 
        bottom:10, 
        margin: 0, 
        right: 10,
        height: 35,
        width: 35
    },
    rightBottomIcon: {
       width: 18,
       height: 18 
    },
    rightTop: {
        position: 'absolute',
        top: 4,
        right: 5,
        width: 35,
        height: 35
    },
    moreIcon: {
        fontSize: 40
    },
    userImgCon: {
      width: 80,
      alignItems: 'center',

    },
    frame: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: 80
    },
    imageCenterView: {
        width: '100%',
        alignItems: 'center'
    },
    innerContentView: {
        backgroundColor: whiteColor,
        borderWidth: 1,
        borderColor: '#e6e6e6',
        borderRadius: 5
    },
    innerViewAcc: {
        margin: 10,
        marginBottom: 0
    }
})