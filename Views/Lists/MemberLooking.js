import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Footer, Card, CardItem, Right, Icon, Left, Segment, Button, Fab } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11 } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import DrawerMenu from '../../components/Common/DrawerMenu'
import { apiCall } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
var { height, width } = Dimensions.get('window');

class MemberLooking extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        activeIndex: 1,
        active: false,
        list: [],
        loader: false
    }
    async componentDidMount(){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({action: 'memberLooking', user_id})).then(res => {
            this.setState({ loader: false })
            if(res.data.status === 'success'){
                this.setState({ list: res.data.users })
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }
    // setActiveIndex = (index) => {
    //     this.setState({ activeIndex: index })
    // }
    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let {list} = this.state;
        let child = <Content>
                    <View style={{flex: 1}}>
                        <View style={styles.topRedView} >
                            <SignupHeader navigation={navigation} label="Member Looking For Me" />
                        </View>
                        <View style={styles.contentView}>
                            <View style={{ paddingTop:0}}>
                                {list.length ? list.map((item,key) => {
                                    return (
                                        <TouchableOpacity style={styles.cartContainer} key={key} onPress={() => navigation.navigate('DetailPage',{to_user_id: item.tokensID})} activeOpacity={0.7}>
                                            <View style={styles.halfImageCon}>
                                                <Image source={item.profile_image ? {uri: item.profile_image}:require('../../assets/images/card_img.png')} style={{width: '100%', height: '100%'}} />
                                            </View>
                                            <View style={styles.rightConCard}>
                                                <View style={styles.marginBottom20}>
                                                <Text style={styles.nameCardText}>{item.nickname}</Text>
                                                <Text style={styles.pIdText}>PROFILE ID: {item.profile_id}</Text>
                                                </View>
                                                <View>
                                                <Text style={[styles.pIdText,{fontSize: fontSize9}]}>{item.age +' years'}, {item.height}", {item.religion}</Text>
                                                <Text style={[styles.pIdText,{fontSize: fontSize11, fontWeight: '500'}]}>{item.education}</Text>
                                                <Text style={[styles.pIdText,{fontSize: fontSize11, fontWeight: '500'}]}>{item.occupation}</Text>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                }): <View/>}
                            </View>
                        </View>
                    </View>
                    {this.handelLoader()}
                </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(MemberLooking)
const styles = StyleSheet.create({
    topRedView: {
        // paddingBottom: 50,
        backgroundColor: primaryColor,
        
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    contentView: {
        padding: 15,
        // marginTop: -60,
        width: '100%'
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize8,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      position: 'relative',
      height: 110
    },
    halfImageCon: {
      width: 90,
      height: 110
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    marginBottom20: {
      marginBottom: 10
    },
    nameCardText: {
      color: primaryColor,
      fontSize: fontSize16,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    pIdText: {
      color: '#525252',
      fontSize: fontSize8,
      fontFamily: fontFamily
    },
    rightBottom: {
        position: 'absolute', 
        bottom:10, 
        margin: 0, 
        right: 10,
        height: 35,
        width: 35
    },
    rightBottomIcon: {
       width: 18,
       height: 18 
    },
    rightTop: {
        position: 'absolute',
        top: 4,
        right: 5,
        width: 35,
        height: 35
    },
    moreIcon: {
        fontSize: 40
    }
})