import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, Keyboard, AsyncStorage } from 'react-native';
import { Container, Content, Accordion, Icon, Button } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11, dataArray, dataArrayMyPrefrence } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import { connect } from 'react-redux'
import { apiCalls, apiCall, openToast, saveSearchList } from '../../redux/actions';
import RNPickerSelect from 'react-native-picker-select';
import ShowLoader from '../../components/ShowLoader';
import DrawerMenu from '../../components/Common/DrawerMenu';
var { height, width } = Dimensions.get('window');

class ProfileEdit extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        loader: false,
        isDateTimePickerVisible: false,
        activeIndex: 1,
        active: false,
        heightData: [],
        dummyHeightData:[],
        height: {
            label: '',
            value: ''
        },
        endHeightData: [],
        dummyEndHeightData: [],
        endHeight: {
            label: '',
            value: ''
        },
        maritalData:[],
        dummyMaritalData:[],
        marital_status: {
            label: '',
            value: ''
        },
        languageData: [],
        dummyLanguageData: [],
        language: {
            label: '',
            value: ''
        },
        educationData: [],
        dummyEducationData: [],
        education: {
            label: '',
            value: ''
        },
        countryData: [],
        dummyCountryData: [],
        country: {
            label: '',
            value: ''
        },
        stateData: [],
        dummyStateData: [],
        state: {
            label: '',
            value: ''
        },
        cityData: [],
        dummyCityData: [],
        city: {
            label: '',
            value: ''
        },
        religionData: [],
        dummyReligionData: [],
        religion: {
            label: '',
            value: ''
        },
        genderData:[
            {
                label: 'Male',
                value: '1'
            },
            {
                label: 'Female',
                value: '2'
            }
        ],
        dummyGenderData: [
            {
                label: '',
                value: ''
            },
            {
                label: 'Male',
                value: '1'
            },
            {
                label: 'Female',
                value: '2'
            }
        ],
        gender: {
            value: '',
            label: ''
        },



        ds_gender: {
            value: '',
            label: ''
        },
        ds_height: {
            label: '',
            value: ''
        },
        ds_endHeightData: [],
        ds_dummyEndHeightData: [],
        ds_endHeight: {
            label: '',
            value: ''
        },
        ds_country: {
            label: '',
            value: ''
        },
        ds_stateData: [],
        ds_dummyStateData: [],
        ds_state: {
            label: '',
            value: ''
        },
        ds_cityData: [],
        ds_dummyCityData: [],
        ds_city: {
            label: '',
            value: ''
        },
        ds_marital_status: {
            label: '',
            value: ''
        },
        ds_language: {
            label: '',
            value: ''
        },
        occupation: {
            value: '',
            label: ''
        },
        income: {
            value: '',
            label: ''
        },
        ds_religion: {
            label: '',
            value: ''
        },
        occupationData: [],
        dummyOccupationData: [],
        incomeData: [],
        dummyIncomeData: []
    }

    UNSAFE_componentWillMount(){
        this.apiGetEditProfileData()
    }

    async apiGetEditProfileData(){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({action: "getEditProfileData", user_id})).then(res => {
            let newObj = {label: '', value: ''}
            let { data } = res.data
            this.setState({ loader: false})
            if(data.height){
                let saveData = data.height.map(item => {
                    return {
                        label: item.height, value: item.id
                    }
                })
                this.setState({ heightData: saveData, dummyHeightData: [newObj, ...saveData] })
            }
            if(data.marital_status){
                let saveData = data.marital_status.map(item => {
                    return {
                        label: item.status, value: item.id
                    }
                })
                this.setState({ maritalData: saveData, dummyMaritalData: [newObj, ...saveData] })
            }
            if(data.language){
                let saveData = data.language.map(item => {
                    return {
                        label: item.language, value: item.id
                    }
                })
                this.setState({ languageData: saveData, dummyLanguageData: [newObj, ...saveData] })
            }
            if(data.education){
                let saveData = data.education.map(item => {
                    return {
                        label: item.education, value: item.id
                    }
                })
                this.setState({ educationData: saveData, dummyEducationData: [newObj, ...saveData] })
            }
            if(data.religion){
                let saveData = data.religion.map(item => {
                    return {
                        label: item.religion, value: item.id
                    }
                })
                this.setState({ religionData: saveData, dummyReligionData: [newObj, ...saveData] })
            }
            if(data.income){
                let saveData = data.income.map(item => {
                    return {
                        label: item.income, value: item.id
                    }
                })
                this.setState({ incomeData: saveData, dummyIncomeData: [newObj, ...saveData] })
            }
            if(data.occupation){
                let saveData = data.occupation.map(item => {
                    return {
                        label: item.occupation, value: item.id
                    }
                })
                this.setState({ occupationData: saveData, dummyOccupationData: [newObj, ...saveData] })
            }
            if(data.countries){
                let saveData = data.countries.map(item => {
                    return {
                        label: item.name, value: item.id
                    }
                })
                this.setState({ countryData: saveData, dummyCountryData: [newObj, ...saveData] })
            }
            
        }).catch(err => {
            this.setState({ loader: false })
        })
    }
    

    onChange = (event,index, keyCode, data) => {
        let findObj = data.find((itm,key) => key === index)
        if(keyCode === 'height'){
            if(index){
                let newObj = {label: '', value: ''}
                this.setState({ height: findObj, loader: true })
                this.props.dispatch(apiCall({action: 'getEndHeight', id: findObj.value})).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.height, value: item.id
                            }
                        })
                        this.setState({ endHeightData: saveData, dummyEndHeightData: [newObj, ...saveData] })
                    }
                })
            }else{
                this.setState({ height: {label: '',value: ''} })
            }
        }
        if(keyCode === 'endHeight'){
            if(index){
                this.setState({ endHeight: findObj })
            }else{
                this.setState({ endHeight: {label: '',value: ''} })
            }
        }
        if(keyCode === 'marital_status'){
            if(index){
                this.setState({ marital_status: findObj })
            }else{
                this.setState({ marital_status: {label: '',value: ''} })
            }
        }
        if(keyCode === 'language'){
            if(index){
                this.setState({ language: findObj })
            }else{
                this.setState({ language: {label: '',value: ''} })
            }
        }
        if(keyCode === 'religion'){
            if(index){
                this.setState({ religion: findObj })
            }else{
                this.setState({ religion: {label: '',value: ''} })
            }
        }
        if(keyCode === 'gender'){
            if(index){
                this.setState({ gender: findObj })
            }else{
                this.setState({ gender: {label: '',value: ''} })
            }
        }
        if(keyCode === 'country'){
            if(index){
                this.setState({ country: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getStateByCountry', country_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ stateData: saveData, dummyStateData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ country: {label: '',value: ''} })
            }
        }
        if(keyCode === 'state'){
            if(index){
                this.setState({ state: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getCityByState', state_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ cityData: saveData, dummyCityData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ state: {label: '',value: ''} })
            }
        }
        if(keyCode === 'city'){
            if(index){
                this.setState({ city: findObj })
            }else{
                this.setState({ city: {label: '',value: ''} })
            }
        }
        
    }

    onChangeAdvanceSearch = (event,index, keyCode, data) => {
        let findObj = data.find((itm,key) => key === index)
        if(keyCode === 'height'){
            if(index){
                let newObj = {label: '', value: ''}
                this.setState({ ds_height: findObj, loader: true })
                this.props.dispatch(apiCall({action: 'getEndHeight', id: findObj.value})).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.height, value: item.id
                            }
                        })
                        this.setState({ ds_endHeightData: saveData, ds_dummyEndHeightData: [newObj, ...saveData] })
                    }
                })
            }else{
                this.setState({ ds_height: {label: '',value: ''} })
            }
        }
        if(keyCode === 'endHeight'){
            if(index){
                this.setState({ ds_endHeight: findObj })
            }else{
                this.setState({ ds_endHeight: {label: '',value: ''} })
            }
        }
        if(keyCode === 'marital_status'){
            if(index){
                this.setState({ ds_marital_status: findObj })
            }else{
                this.setState({ ds_marital_status: {label: '',value: ''} })
            }
        }
        if(keyCode === 'religion'){
            if(index){
                this.setState({ ds_religion: findObj })
            }else{
                this.setState({ ds_religion: {label: '',value: ''} })
            }
        }
        if(keyCode === 'income'){
            if(index){
                this.setState({ income: findObj })
            }else{
                this.setState({ income: {label: '',value: ''} })
            }
        }
        if(keyCode === 'occupation'){
            if(index){
                this.setState({ occupation: findObj })
            }else{
                this.setState({ occupation: {label: '',value: ''} })
            }
        }
        if(keyCode === 'language'){
            if(index){
                this.setState({ ds_language: findObj })
            }else{
                this.setState({ ds_language: {label: '',value: ''} })
            }
        }
        if(keyCode === 'education'){
            if(index){
                this.setState({ education: findObj })
            }else{
                this.setState({ education: {label: '',value: ''} })
            }
        }
        if(keyCode === 'gender'){
            if(index){
                this.setState({ ds_gender: findObj })
            }else{
                this.setState({ ds_gender: {label: '',value: ''} })
            }
        }
        if(keyCode === 'country'){
            if(index){
                this.setState({ ds_country: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getStateByCountry', country_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ ds_stateData: saveData, ds_dummyStateData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ ds_country: {label: '',value: ''} })
            }
        }
        if(keyCode === 'state'){
            if(index){
                this.setState({ ds_state: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getCityByState', state_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ ds_cityData: saveData, ds_dummyCityData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ ds_state: {label: '',value: ''} })
            }
        }
        if(keyCode === 'city'){
            if(index){
                this.setState({ ds_city: findObj })
            }else{
                this.setState({ ds_city: {label: '',value: ''} })
            }
        }
        
    }

    async onSubmit(data){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({...data, action: 'editUserProfile', user_id})).then(res => {
            this.setState({ loader: false })
            if(res.data){
                this.props.dispatch(openToast('Profile Updated successfully!'))
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    async onSubmitMyPrefrence(data){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({...data, action: 'savePartnerUserProfile', user_id})).then(res => {
            this.setState({ loader: false })
            if(res.data){
                this.props.dispatch(openToast('Profile Updated successfully!'))
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    setActiveIndex = (index) => {
        this.setState({ activeIndex: index })
    }

    onNormalSearch = async () => {
        let user_id = await AsyncStorage.getItem('user_id')
        let { gender, height, endHeight, marital_status, country, state, city, religion, language } = this.state;
        let data = { action: 'normalSearch', user_id, gender: gender.value, height_start: height.value, height_end: endHeight.value, maritalStatus: marital_status.value, country: country.value, state: state.value, city: city.value, religion: religion.value, language: language.value }
        this.setState({ loader: true })
        this.props.dispatch(apiCall(data)).then(res => {
            console.log(res,"res")
            this.setState({ loader: false })
            this.props.dispatch(saveSearchList(res.data.data))
            this.props.navigation.navigate('SearchedUser')
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    onAdvancedSearch = async () => {
        let user_id = await AsyncStorage.getItem('user_id')
        let { ds_gender, ds_height, ds_endHeight, ds_marital_status, ds_country, ds_state, ds_city, ds_religion, ds_language, education, occupation, income } = this.state;
        let data = { action: 'advanceSearch', user_id, gender: ds_gender.value, height_start: ds_height.value, height_end: ds_endHeight.value, maritalStatus: ds_marital_status.value, country: ds_country.value, state: ds_state.value, city: ds_city.value, religion: ds_religion.value, language: ds_language.value, education, occupation: occupation.value, income: income.value }
        this.setState({ loader: true })
        this.props.dispatch(apiCall(data)).then(res => {
            console.log(res,"res")
            this.setState({ loader: false })
            this.props.navigation.navigate('SearchedUser')
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { activeIndex, genderData, dummyGenderData, gender, dummyHeightData, heightData, height, marital_status, maritalData, dummyMaritalData, dummyCountryData, countryData, country, city, cityData, dummyCityData, state, stateData, dummyStateData, religion, religionData, dummyReligionData, language, languageData, dummyLanguageData, endHeight, endHeightData, dummyEndHeightData, ds_gender, ds_height, ds_dummyEndHeightData, ds_endHeightData, ds_endHeight, ds_marital_status, ds_country, ds_dummyStateData, ds_stateData, ds_state, ds_city, ds_cityData, ds_dummyCityData, ds_religion, ds_language, occupation, income, education, educationData, dummyEducationData, occupationData, dummyOccupationData, incomeData, dummyIncomeData } = this.state;
        let child = <Content>
                        <View style={{flex: 1}}>
                            <View style={styles.topRedView}>
                                <SignupHeader navigation={navigation} label="Search"  />
                                <View style={styles.imageCenterView}>
                                    <View style={styles.userImgCon}>
                                    <Image source={require('../../assets/images/card_img.png')} style={{width: '100%', height: 80}} />
                                    <Image source={require('../../assets/images/framePrimary.png')} style={styles.frame} />
                                    </View>
                                </View>
                                <View style={styles.segmentOuter}>
                                    <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                        <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>REGULAR SEARCH</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={activeIndex == 2 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(2)}>
                                        <Text style={activeIndex == 2 ? styles.activeText : styles.unActiveText}>ADVANCED SEARCH</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {activeIndex === 1 && <View style={{ padding: 15, marginTop: 20}}>
                                <View style={{backgroundColor: '#ededed', padding: 10}}>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>Gender:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'gender', dummyGenderData)}
                                                items={genderData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{gender.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>Start Height:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'height', dummyHeightData)}
                                                items={heightData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{height.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>End Height:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'endHeight', dummyEndHeightData)}
                                                items={endHeightData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{endHeight.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>Marital Status:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'marital_status', dummyMaritalData)}
                                                items={maritalData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{marital_status.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>Country:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'country', dummyCountryData)}
                                                items={countryData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{country.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>State:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'state', dummyStateData)}
                                                items={stateData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{state.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>District / City:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'city', dummyCityData)}
                                                items={cityData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{city.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}}>Religion:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'religion', dummyReligionData)}
                                                items={religionData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{religion.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                        <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                            <Text style={{fontSize: fontSize11}} numberOfLines={1}>Mother Tongue:</Text> 
                                        </View>
                                        <View style={{ width: '100%', paddingLeft: 85}}>
                                            <RNPickerSelect
                                                onValueChange={(e,i) => this.onChange(e, i, 'language', dummyLanguageData)}
                                                items={languageData}
                                                style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                            >
                                                <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                    <Text style={{fontSize: fontSize11}}>{language.label}</Text>
                                                    <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                </View>
                                                
                                            </RNPickerSelect>
                                        </View>
                                    </View>
                                    <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', marginTop: 15, marginBottom: 10}} onPress={this.onNormalSearch}>
                                        <Text style={{color: whiteColor, fontSize: fontSize12, fontWeight: '500'}}>SEARCH</Text>
                                    </Button>
                                </View>
                            </View>}
                            {activeIndex === 2 && 
                            <View>
                                <View style={{ padding: 15, marginTop: 20}}>
                                    <View style={{backgroundColor: '#ededed', padding: 10}}>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>Gender:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'gender', dummyGenderData)}
                                                    items={genderData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_gender.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>Start Height:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'height', dummyHeightData)}
                                                    items={heightData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_height.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>End Height:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'endHeight', ds_dummyEndHeightData)}
                                                    items={ds_endHeightData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_endHeight.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>Marital Status:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'marital_status', dummyMaritalData)}
                                                    items={maritalData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_marital_status.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>Religion:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'religion', dummyReligionData)}
                                                    items={religionData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_religion.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}} numberOfLines={1}>Mother Tongue:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'language', dummyLanguageData)}
                                                    items={languageData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_language.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ padding: 15, paddingTop: 0}}>
                                    <View style={{backgroundColor: '#ededed', padding: 10}}>
                                        <View style={{marginBottom: 10}}>
                                            <View style={{position: 'absolute', top: 0, left: -5}}>
                                                <Image source={require('../../assets/images/tagline.png')} style={{height: 25, width: 25}}/>
                                            </View>
                                            <Text style={[{ fontWeight: "400", fontSize: fontSize14, marginLeft: 30, color: primaryColor, paddingTop: 2 }]}>
                                                EDUCATION AND CARRER
                                            </Text>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>Education:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'education', dummyEducationData)}
                                                    items={educationData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{education.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>Occupation:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'occupation', dummyOccupationData)}
                                                    items={occupationData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{occupation.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}} numberOfLines={1}>Annual Income:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'income', dummyIncomeData)}
                                                    items={incomeData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{income.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        
                                    </View>
                                </View>
                                <View style={{ padding: 15, paddingTop: 0}}>
                                    <View style={{backgroundColor: '#ededed', padding: 10}}>
                                        <View style={{marginBottom: 10}}>
                                            <View style={{position: 'absolute', top: 0, left: -5}}>
                                                <Image source={require('../../assets/images/tagline.png')} style={{height: 25, width: 25}}/>
                                            </View>
                                            <Text style={[{ fontWeight: "400", fontSize: fontSize14, marginLeft: 30, color: primaryColor, paddingTop: 2 }]}>
                                                LOCATION DETAILS
                                            </Text>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>Country:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'country', dummyCountryData)}
                                                    items={countryData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_country.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>State:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'state', ds_dummyStateData)}
                                                    items={ds_stateData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_state.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                        <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                                            <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                                                <Text style={{fontSize: fontSize11}}>District / City:</Text> 
                                            </View>
                                            <View style={{ width: '100%', paddingLeft: 85}}>
                                                <RNPickerSelect
                                                    onValueChange={(e,i) => this.onChangeAdvanceSearch(e, i, 'city', ds_dummyCityData)}
                                                    items={ds_cityData}
                                                    style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                                                >
                                                    <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                                        <Text style={{fontSize: fontSize11}}>{ds_city.label}</Text>
                                                        <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                                                    </View>
                                                    
                                                </RNPickerSelect>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', marginTop: 10, marginBottom: 40}} onPress={this.onAdvancedSearch}>
                                    <Text style={{color: whiteColor, fontSize: fontSize12, fontWeight: '500'}}>SEARCH</Text>
                                </Button>
                            </View>
                            }
                        </View>
                        {this.handelLoader()}
                    </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(ProfileEdit)
const styles = StyleSheet.create({
    topRedView: {
        backgroundColor: primaryColor
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15,
        marginBottom: -20,
        borderWidth: 1,
        borderColor: '#e6e6e6'
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    contentView: {
        padding: 15,
        width: '100%',
        marginTop: 40,
        paddingTop: 0
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize8,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      position: 'relative'
    },
    halfImageCon: {
      width: 90,
      height: 100
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    marginBottom20: {
      marginBottom: 10
    },
    nameCardText: {
      color: primaryColor,
      fontSize: fontSize16,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    pIdText: {
      color: '#525252',
      fontSize: fontSize8,
      fontFamily: fontFamily
    },
    rightBottom: {
        position: 'absolute', 
        bottom:10, 
        margin: 0, 
        right: 10,
        height: 35,
        width: 35
    },
    rightBottomIcon: {
       width: 18,
       height: 18 
    },
    rightTop: {
        position: 'absolute',
        top: 4,
        right: 5,
        width: 35,
        height: 35
    },
    moreIcon: {
        fontSize: 40
    },
    userImgCon: {
      width: 80,
      alignItems: 'center',

    },
    frame: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: 80
    },
    imageCenterView: {
        width: '100%',
        alignItems: 'center'
    },
    innerContentView: {
        backgroundColor: whiteColor,
        borderWidth: 1,
        borderColor: '#e6e6e6',
        borderRadius: 5
    },
    innerViewAcc: {
        margin: 10,
        marginBottom: 0
    }
})