

import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, Keyboard, AsyncStorage } from 'react-native';
import { Container, Content, Accordion, Icon, Button } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11, dataArray, dataArrayMyPrefrence, fontSize24 } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import { connect } from 'react-redux'
import { apiCalls, apiCall, openToast } from '../../redux/actions';
import RNPickerSelect from 'react-native-picker-select';
import ShowLoader from '../../components/ShowLoader';
import DrawerMenu from '../../components/Common/DrawerMenu';
import Dialog, { DialogContent, SlideAnimation } from 'react-native-popup-dialog';
import RNUpiPayment from 'react-native-upi-payment';
var { height, width } = Dimensions.get('window');

class DetailPage extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        loader: false,
        activeIndex: 1,
		userDetail: {},
		modalVisible: false,
		price: 5,
		paymentFor: 2
    }

    async componentDidMount(){
		let from_user_id = await AsyncStorage.getItem('user_id')
        let { to_user_id } = this.props.navigation && this.props.navigation.state && this.props.navigation.state.params
        this.setState({ loader: true })
        this.props.dispatch(apiCall({action: 'getViewProfileData', user_id: to_user_id, from_user_id: from_user_id })).then(res => {
            console.log(res,"res")
            this.setState({ loader: false })
            if(res.data.data){
                this.setState({ userDetail: res.data.data.user })
            }
        }).catch(err =>{
            this.setState({ loader: false })
        })
    }

    onChange = (event,index, keyCode, data) => {
    }

    onSubmit(data){
        
    }

    setActiveIndex = (index) => {
        this.setState({ activeIndex: index })
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }

    onSubmit(){
		this.setState({ modalVisible: false })
		setTimeout(() => {
			RNUpiPayment.initializePayment({
				vpa: 'navichawla92-1@okicici', // or can be john@ybl or mobileNo@upi
				payeeName: 'Navjot Singh',
				amount: '2',
				transactionRef: "aasf-332-aoei-fns"
			},this.successCallback,this.failureCallback);
		}, 10);
	}
	
	failureCallback = (data) => {
        console.log(data,"data")
        // if(data['Status']=="SUCCESS"){
        //     this.setState({Status:"SUCCESS",txnId:data['txnId']});
        // }else{
        //     this.setState({Status:"FAILURE"})
        // }
    }
    successCallback = async (data) => {
		let from_user_id = await AsyncStorage.getItem('user_id')
		let { to_user_id } = this.props.navigation && this.props.navigation.state && this.props.navigation.state.params
		console.log(data,"data err")
		this.setState({ loader: true })
		this.props.dispatch(apiCall({action: 'appPayment', to_user_id: to_user_id, from_user_id: from_user_id, payment_for: this.state.paymentFor, transaction_id: data.txnId, payment_status: 'SUCCESS' })).then(res => {
            console.log(res,"res")
            this.setState({ loader: false })
            if(res.data.data){
                this.setState({ userDetail: res.data.data.user })
            }
        }).catch(err =>{
            this.setState({ loader: false })
        })
        //nothing happened here using Google Pay
	}
	
    render(){
		let { navigation } = this.props;
		
		let { activeIndex, userDetail } = this.state;
        let { to_user_id } = this.props.navigation && this.props.navigation.state && this.props.navigation.state.params
        let child = <Content>
                        <View style={{flex: 1}}>
                            <View style={styles.topRedView}>
                                <SignupHeader navigation={navigation} previousScreen=" " label="Detail Page" />
                                <View style={{ paddingTop:0}}>
	                                <View style={styles.cardOuter}>
	                                    <ImageBackground source={userDetail && userDetail.profile_image ? {uri: userDetail.profile_image} :  require('../../assets/images/card_img.png')} style={{width: '100%', height: 250 }} fadeDuration={10}>
	                                        <View style={{backgroundColor: 'rgba(0,0,0,0.5)', height: '100%'}}>

	                                            <View style={styles.inerCardView}>
	                                                <View style={styles.marginBottom20}>
	                                                    <Text style={styles.nameText}>{userDetail.nickname}</Text>
	                                                    <Text style={styles.profileIdText}>PROFILE ID: {userDetail.profile_id || 123}</Text>
	                                                </View>
	                                                <View>
	                                                    <Text style={styles.yearText}>{userDetail.height}</Text>
	                                                    {/* <Text style={styles.eduText}>{userDetail.education}</Text> */}
	                                                    <Text style={styles.eduText}>{userDetail.occupation}</Text>
	                                                </View>
	                                            </View>
                                                {userDetail.message_visible ? <TouchableOpacity style={{height: 50, width: 50, backgroundColor: '#e5e1e1', borderRadius: 25, position: 'absolute', right: 20, bottom: 20, justifyContent: 'center', alignItems: 'center', zIndex: 10}} activeOpacity={0.7} onPress={() => navigation.navigate('ChatBox',{to_user_id })}>
                                                    <Image source={require('../../assets/images/message-text-outline.png')} style={{width: 20, height: 20}} />
                                                </TouchableOpacity> : null}
	                                            
	                                        </View>
	                                    </ImageBackground>
	                                </View>
                                </View>                                
                            </View>
                            <View style={styles.bottomOuter}>
                            	{/* <View style={styles.segmentOuter}>
                                  <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                      <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>Faivourite</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                      <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>Like</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                      <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>Tagged</Text>
                                  </TouchableOpacity>
                              </View> */}
                                <View>
                                	<Text style={styles.basicHeading}>Basic Information</Text>
                                	<Text style={styles.infoText}>{userDetail.brief_description}</Text>
                                </View>
                                <View style={styles.middleLine}></View>
                                <View>
		                            	<Text style={styles.bacisBlack}>Basic Detail</Text>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Name</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.nickname}</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Gender</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.gender}</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Age</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.dob}</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Height</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.height}</Text>
		                            		</View>
		                            	</View>
		                            	{/* <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Weight</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>71</Text>
		                            		</View>
		                            	</View> */}
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Comlexion</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.complexion}</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Habits</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.habits}</Text>
		                            		</View>
		                            	</View>
                                        <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Caste</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text numberOfLines={1}>{userDetail.caste}</Text>
		                            		</View>
		                            	</View>
                                        <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Religion</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.religion}</Text>
		                            		</View>
		                            	</View>
                                        <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Education</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text numberOfLines={1}>{userDetail.education}</Text>
		                            		</View>
		                            	</View>
                                        <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Gender</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.gender}</Text>
		                            		</View>
		                            	</View>
                                        <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Income</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.income}</Text>
		                            		</View>
		                            	</View>
                                        <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Language</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text numberOfLines={1}>{userDetail.mother_tongue}</Text>
		                            		</View>
		                            	</View>
		                            	{/* <View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Smoking Habits</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>none</Text>
		                            		</View>
		                            	</View> */}
		                            </View>

		                            <View style={styles.middleLine}></View>
                                <View>
		                            	<Text style={styles.bacisBlack}>Contact Detail</Text>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Contact Number</Text>
												<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text style={{marginRight: 10}}>{userDetail.phone_visible ? userDetail.mobile : '**********'}</Text>
												{!userDetail.phone_visible && <TouchableOpacity onPress={()=> this.setState({ modalVisible: true, price: 10, paymentFor: 3 })}><Icon active name="lock" style={{color: 'grey', fontSize: fontSize24, fontWeight: '500', marginTop: -3}} /></TouchableOpacity>}
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Chat Status</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text style={{marginRight: 10}}>{userDetail.message_visible ? 'Online' : 'Offline'}</Text>
												{!userDetail.message_visible && <TouchableOpacity onPress={() => this.setState({ modalVisible: true, price: 5, paymentFor: 1 })}><Icon active name="lock" style={{color: 'grey', fontSize: fontSize24, fontWeight: '500', marginTop: -3}} /></TouchableOpacity>}
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Send Mail</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text style={{marginRight: 10}}>{userDetail.email_visible ? userDetail.email : '**********'}</Text>
												{!userDetail.email_visible &&<TouchableOpacity onPress={() => this.setState({ modalVisible: true, price: 5, paymentFor: 2 })}><Icon active name="lock" style={{color: 'grey', fontSize: fontSize24, fontWeight: '500', marginTop: -3}} /></TouchableOpacity>}
		                            		</View>
		                            	</View>
		                            </View>


		                            <View style={styles.middleLine}></View>
                                {/* <View>
		                            	<Text style={styles.bacisBlack}>Professional Detail</Text>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>test</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>test est test</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>test</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>test test test</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>test</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>test test test test</Text>
		                            		</View>
		                            	</View>
		                            </View> */}

		                            {/* <View style={styles.middleLine}></View> */}
                                <View>
		                            	<Text style={styles.bacisBlack}>Location</Text>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>Country</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.country}</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>State</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.state}</Text>
		                            		</View>
		                            	</View>
		                            	<View style={styles.infoContainer}>
		                            		<View style={styles.infoLeft}>
		                            			<Text>City</Text>
		                            			<Text style={styles.textRight}>:</Text>
		                            		</View>
		                            		<View style={styles.infoRight}>
		                            			<Text>{userDetail.city}</Text>
		                            		</View>
		                            	</View>
		                            </View>



                            </View>
                            
                        </View>
                        {this.handelLoader()}
						<Dialog
							visible={this.state.modalVisible}
							dialogAnimation={new SlideAnimation({
							slideFrom: 'bottom',
							})}
							onTouchOutside={() => {
								this.setState({ modalVisible: false })
							}}
							dialogStyle={{backgroundColor: '#fff',width: '100%',borderRadius: 0,padding: 0}}
							containerStyle={{justifyContent: 'center',padding: 20,paddingBottom: 0,width: '100%',marginBottom: -25}}
						>
							<DialogContent>
								<View style={{display: 'flex', flexDirection: 'row', marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
									<View style={{marginBottom: 10, marginRight: 20, marginTop: 10}}>
										<Text>Price: Rs {this.state.price}</Text>
									</View>
									<Button style={{width: 100, backgroundColor: primaryColor, height: 35, justifyContent: 'center', alignItems: 'center'}} onPress={this.onSubmit.bind(this)}>
										<Text style={{color: whiteColor, fontSize: fontSize12, fontWeight: '500'}}>Pay Now</Text>
									</Button>
								</View>
							</DialogContent>
						</Dialog>
                    </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(DetailPage)
const styles = StyleSheet.create({
    topRedView: {
        backgroundColor: primaryColor,
        padding: 15,
        height: 320,
        marginBottom: 0,
        paddingTop: 0
    },
    segmentOuter: {
        padding: 7,
        paddingLeft: 3,
        paddingRight:3,
        flexDirection: 'row',
        backgroundColor: '#e6e6e6',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#e6e6e6',
        width: '100%',
        marginBottom: 15
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '30.5%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight:5,
        marginLeft: 5
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '33.33%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      position: 'relative'
    },
    halfImageCon: {
      width: 90,
      height: 100
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    userImgCon: {
      width: 80,
      alignItems: 'center',

    },
    frame: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: 80
    },
    imageCenterView: {
        width: '100%',
        alignItems: 'center'
    },
    basicHeading:{
    	fontSize: 20,
    	color: primaryColor,
    	textTransform: 'uppercase',
    	marginBottom: 5
    },
    bottomOuter:{
    	paddingLeft: 15,
    	paddingRight: 15,
    	paddingTop: 10,
    	paddingBottom: 40
    },
    infoText: {
    	color: greyColor
    },
    middleLine:{
    	height: 1,
    	backgroundColor: '#c1c1c1',
    	marginTop: 20,
    	marginBottom: 20
    },
    bacisBlack:{
    	fontSize: 18,
    	marginBottom: 10
    },
    infoContainer:{
    	flexDirection: 'row',
    	marginBottom:10
    },
    infoLeft:{
    	width: '40%',
    	flexDirection: 'row'
    },
    infoRight:{
		width: '60%',
		flexDirection: 'row'
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize12,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    }
})