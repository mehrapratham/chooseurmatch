import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Footer, Card, CardItem, Right, Icon, Left, Segment, Button, Fab } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13 } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import DrawerMenu from '../../components/Common/DrawerMenu'
import { apiCall } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
import Tag from 'react-native-vector-icons/AntDesign'
import Star from 'react-native-vector-icons/EvilIcons'
var { height, width } = Dimensions.get('window');

class Recomended extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        activeIndex: 1,
        allProfiles: [],
        active: false,
        uniqueId: ''
    }
    async componentDidMount(){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({action: "allProfiles", user_id})).then(res => {
            this.setState({ loader: false, allProfiles: res.data.data })
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    async likeProfile(likeStatus, key){
        let user_id = await AsyncStorage.getItem('user_id')
        let {allProfiles} = this.state;
        if (likeStatus == 'yes') {
            allProfiles[key].usermeta.liked = 'no'    
        } else {
            allProfiles[key].usermeta.liked = 'yes'
        }
        this.setState({allProfiles})
        this.props.dispatch(apiCall({action: 'likeUnlikePost', user_id, user_login: allProfiles[key].usermeta.profile_id})).then(res => {
        }).catch(err => {
        })
    }

    async favProfile(favStatus, key){
        let {allProfiles} = this.state;
        let user_id = await AsyncStorage.getItem('user_id')
        if (favStatus == 'yes') {
            allProfiles[key].usermeta.favourite = 'no'    
        } else {
            allProfiles[key].usermeta.favourite = 'yes'
        }
        this.setState({allProfiles})
        this.props.dispatch(apiCall({action: 'favoriteUnfavoritePost', user_id, user_login: allProfiles[key].usermeta.profile_id})).then(res => {
        }).catch(err => {
        })
    }

    async tagProfile(tagStatus, key){
        let {allProfiles} = this.state;
        let user_id = await AsyncStorage.getItem('user_id')
        if (tagStatus == 'yes') {
            allProfiles[key].usermeta.tagged = 'no'    
        } else {
            allProfiles[key].usermeta.tagged = 'yes'
        }
        this.setState({allProfiles})
        this.props.dispatch(apiCall({action: 'taggedUntaggedPost', user_id, user_login: allProfiles[key].usermeta.profile_id})).then(res => {
        }).catch(err => {
        })
    }

    render(){
        let { navigation } = this.props;
        let { allProfiles } = this.state;
        let child = <Content>
                    <View style={styles.topRedView}>
                        <SignupHeader label="All Profiles" navigation={navigation}/>
                        {/* <View style={styles.segmentOuter}>
                            <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>Recomended</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={activeIndex == 2 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(2)}>
                                <Text style={activeIndex == 2 ? styles.activeText : styles.unActiveText}>Prefered</Text>
                            </TouchableOpacity>
                        </View> */}
                    </View>
                    <View style={styles.contentView}>
                    {allProfiles.length ? allProfiles.map((item,key) => {
                        console.log(item,"item")
                        return (
                            <TouchableOpacity style={{ paddingTop:20, zIndex: 9}} key={key} activeOpacity={0.7} onPress={() => navigation.navigate('DetailPage',{to_user_id: item.usermeta.tokensID})}>
                                <View style={styles.cardOuter}>
                                    <ImageBackground source={item.usermeta.profile_image ?  {uri: item.usermeta.profile_image} : require('../../assets/images/card_img.png')} style={{width: '100%', height: 250 }} fadeDuration={10}>
                                        <View style={{backgroundColor: 'rgba(0,0,0,0.5)', height: '100%'}}>

                                            <TouchableOpacity onPress={this.likeProfile.bind(this, item.usermeta.liked, key)} style={[styles.favCon, (item.usermeta.liked == 'yes') && {backgroundColor: primaryColor}]} activeOpacity={0.7}>
                                                <Tag name="hearto" style={[{fontSize: 24, left: 0, top: 1, color: primaryColor}, (item.usermeta.liked == 'yes') && {color: '#fff'}]} />
                                            </TouchableOpacity>

                                            <View style={styles.inerCardView}>
                                                <View style={styles.marginBottom20}>
                                                    <Text style={styles.nameText}>{item.usermeta.nickname}</Text>
                                                    <Text style={styles.profileIdText}>PROFILE ID: {item.usermeta.profile_id}</Text>
                                                </View>
                                                <View>
                                                    <Text style={styles.yearText}>{item.usermeta.age+' years'}, {item.usermeta.height} {item.usermeta.religion}</Text>
                                                    <Text style={styles.eduText}>{item.usermeta.education}</Text>
                                                    <Text style={styles.eduText}>{item.usermeta.occupation}</Text>
                                                </View>
                                            </View>
                                            {/* <Fab
                                                active={this.state.active}
                                                direction="up"
                                                containerStyle={{ bottom: 0}}
                                                style={styles.favCon}
                                                position="bottomRight"
                                                onPress={() => this.setState({ active: !this.state.active, key })}>
                                                <Tag name={this.state.active ? "pluscircle" : "pluscircle"} style={{color: '#000', marginTop: 2}}/>
                                                <Button style={[{ backgroundColor: '#d6d5d6' }, (item.usermeta.favourite == 'yes') && {backgroundColor: primaryColor}]} onPress={this.favProfile.bind(this, item.usermeta.favourite, key)}>
                                                    <Star name="star" style={[{fontSize: 28, left: 0, top: 1, color: primaryColor}, (item.usermeta.favourite == 'yes') && {color: '#fff'}]} />
                                                </Button>
                                                <Button style={[{ backgroundColor: '#d6d5d6' }, (item.usermeta.tagged == 'yes') && {backgroundColor: primaryColor}]} onPress={this.tagProfile.bind(this, item.usermeta.tagged, key)}>
                                                    <Tag name="tag" style={[{fontSize: 20, left: 1, top: 2, color: primaryColor}, (item.usermeta.tagged == 'yes') && {color: '#fff'}]} />
                                                </Button>
                                            </Fab> */}
                                            {this.state.uniqueId === item.usermeta.tokensID ? <TouchableOpacity style={[{position: 'absolute', right: 20, bottom: 20, width: 50, height: 50, borderRadius: 25, backgroundColor: '#b2b2b2', zIndex: 10, justifyContent: 'center', alignItems: 'center'}]} onPress={() => this.setState({ uniqueId: '' })}>
                                                <Tag name={"closecircle"} style={{color: '#000', fontSize: 26, marginTop: 2}}/>
                                            </TouchableOpacity>:
                                            <TouchableOpacity style={[{position: 'absolute', right: 20, bottom: 20, width: 50, height: 50, borderRadius: 25, backgroundColor: '#b2b2b2', zIndex: 10, justifyContent: 'center', alignItems: 'center'}]} onPress={() => this.setState({ uniqueId: item.usermeta.tokensID })}>
                                                <Tag name={"pluscircle"} style={{color: '#000', fontSize: 26, marginTop: 2}}/>
                                            </TouchableOpacity>}
                                            {this.state.uniqueId === item.usermeta.tokensID &&<TouchableOpacity style={[{position: 'absolute', right: 20, bottom: 80, width: 50, height: 50, borderRadius: 25, backgroundColor: '#b2b2b2', justifyContent: 'center', alignItems: 'center', zIndex: 10}, (item.usermeta.favourite == 'yes') && {backgroundColor: primaryColor}]} onPress={this.favProfile.bind(this, item.usermeta.favourite, key)}>
                                                {/* <Button style={[{ backgroundColor: '#d6d5d6' }, (item.usermeta.favourite == 'yes') && {backgroundColor: primaryColor}]} onPress={this.favProfile.bind(this, item.usermeta.favourite, key)}> */}
                                                    <Star name="star" style={[{fontSize: 28, left: 0, top: 1, color: primaryColor}, (item.usermeta.favourite == 'yes') && {color: '#fff'}]} />
                                                {/* </Button> */}
                                            </TouchableOpacity>}
                                            {this.state.uniqueId === item.usermeta.tokensID &&<TouchableOpacity style={[{position: 'absolute', right: 20, bottom: 140, width: 50, height: 50, borderRadius: 25, backgroundColor: '#b2b2b2', justifyContent: 'center', alignItems: 'center', zIndex: 10}, (item.usermeta.tagged == 'yes') && {backgroundColor: primaryColor}]} onPress={this.tagProfile.bind(this, item.usermeta.tagged, key)}>
                                                {/* <Button style={[{ backgroundColor: '#d6d5d6' }, (item.usermeta.favourite == 'yes') && {backgroundColor: primaryColor}]} onPress={this.favProfile.bind(this, item.usermeta.favourite, key)}> */}
                                                    <Star name="tag" style={[{fontSize: 28, left: 0, top: 1, color: primaryColor}, (item.usermeta.tagged == 'yes') && {color: '#fff'}]} />
                                                {/* </Button> */}
                                            </TouchableOpacity>}
                                        </View>
                                    </ImageBackground>
                                </View>
                            </TouchableOpacity>
                        )
                    }): <View />}
                    
                    </View>
                    {this.handelLoader()}
                </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}

export default connect(state => state)(Recomended)
const styles = StyleSheet.create({
    topRedView: {
        
        backgroundColor: primaryColor
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    contentView: {
        padding: 15,
        paddingTop: 0,
        // marginTop: -60,
        width: '100%'
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize12,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    }
})