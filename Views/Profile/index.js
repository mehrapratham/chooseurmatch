import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, AsyncStorage } from 'react-native';
import { Container, Content, Footer, Card, CardItem, Right, Icon, Left } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22 } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import { Switch } from 'react-native-switch';
import DrawerMenu from '../../components/Common/DrawerMenu'
var { height, width } = Dimensions.get('window');

export default class Profile extends React.Component{
    static navigationOptions = {
        header: null
    }
    constructor(props){
      super(props);
      this.state = {
        user: {}
      }
    }
    async componentDidMount(){
      let user = await AsyncStorage.getItem('user')
      this.setState({user: JSON.parse(user)})
    }
    render(){
        let { navigation } = this.props;
        let {user} = this.state;
        let child = <Content>
                      <SignupHeader navigation={navigation} profileHeader />
                      <View style={styles.profileContainer}>
                        <View>
                          <View style={styles.profileImgCon}>
                            <Image source={user.profile_image ? {uri: user.profile_image} : require('../../assets/images/userImg.png')} style={{width: 90, height: 90}} />
                          </View>
                        </View>
                        <View style={styles.userInfo}>
                          <Text style={styles.nameText}>{user.first_name + ' ' +user.last_name}</Text>
                          <Text style={styles.descriptionText}>{user.brief_description}</Text>
                        </View>
                      </View>

                      <View>
                          <Card transparent>
                            <TouchableOpacity style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={styles.labelText}>Email</Text>
                                </Left>

                                <Text style={styles.infoText}>{user.email}</Text>

                                {/*<Right style={styles.alignEnd}>
                                  <Icon name="arrow-forward" style={ styles.itemArrow} />
                                </Right>*/}
                               </CardItem>
                           </TouchableOpacity>
                           <TouchableOpacity style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={styles.labelText}>Religion</Text>
                                </Left>

                                <Text style={styles.infoText}>{user.religion}</Text>

                                {/*<Right style={styles.alignEnd}>
                                  <Icon name="arrow-forward" style={ styles.itemArrow} />
                                </Right>*/}
                               </CardItem>
                           </TouchableOpacity>
                           <TouchableOpacity style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={styles.labelText}>Mobile</Text>
                                </Left>

                                <Text style={styles.infoText}>{user.mobile}</Text>

                                {/*<Right style={styles.alignEnd}>
                                  <Icon name="arrow-forward" style={ styles.itemArrow}  />
                                </Right>*/}
                               </CardItem>
                           </TouchableOpacity>
                           {/*<View style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={styles.labelText}>Notifications</Text>
                                </Left>

                                <Right style={styles.alignEnd}>
                                  <Switch 
                                    backgroundActive={primaryColor}
                                    circleSize={23}
                                    circleBorderWidth={2}
                                  />
                                </Right>
                               </CardItem>
                           </View>
                           <TouchableOpacity style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={styles.labelText}>Settings</Text>
                                </Left>
                                <Right style={styles.alignEnd}>
                                  <Icon name="arrow-forward" style={ styles.itemArrow}  />
                                </Right>
                               </CardItem>
                           </TouchableOpacity>
                           <TouchableOpacity style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={styles.labelText}>Feedback</Text>
                                </Left>
                                <Right style={styles.alignEnd}>
                                  <Icon name="arrow-forward" style={ styles.itemArrow}  />
                                </Right>
                               </CardItem>
                           </TouchableOpacity>
                           <TouchableOpacity style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={styles.labelText}>Get help</Text>
                                </Left>
                                <Right style={styles.alignEnd}>
                                  <Icon name="arrow-forward" style={ styles.itemArrow}  />
                                </Right>
                               </CardItem>
                           </TouchableOpacity>*/}
                           {/* <TouchableOpacity style={styles.itemStyle}>
                              <CardItem style={{margin: 0, padding: 0, border: 'none'}}>
                                <Left>
                                  <Text style={[styles.labelText, styles.redLink]}>Delete Account</Text>
                                </Left>
                               </CardItem>
                           </TouchableOpacity> */}
                          </Card>
                      </View>
                  </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
const styles = StyleSheet.create({
    profileContainer: {
      backgroundColor: primaryColor,
      padding: 20,
      paddingTop: 5,
      paddingBottom: 40,
      alignItems: 'center'
    },
    profileImgCon: {
      width: 90,
      height: 90,
      borderRadius: 45,
      overflow: 'hidden',
      marginBottom: 20
    },
    userInfo: {
      alignItems: 'center'
    },
    nameText: {
      fontSize: fontSize22,
      color: whiteColor,
      marginBottom: 20,
      fontWeight: '600',
      fontFamily: fontFamily
    },
    descriptionText: {
      fontSize: 14,
      color: whiteColor,
      fontFamily: fontFamily,
      textAlign: 'center'
    },
    labelText: {
      fontSize: 18,
      color: '#363636'
    },
    alignEnd: {
      alignItems: 'flex-end',
      alignSelf: 'flex-end'
    },
    infoText: {
      position:'absolute',
      right: 32,
      top: 10,
      fontSize: 18,
      color: '#b9b9b9'
    },
    itemStyle: {
      borderBottomWidth: 1, 
      borderColor: '#ccc',
      paddingTop: 6,
      paddingBottom: 6
    },
    redLink: {
      color: primaryColor
    },
    itemArrow:{
      fontSize:30, 
      top: -24, 
      position: 'absolute'
    }
})