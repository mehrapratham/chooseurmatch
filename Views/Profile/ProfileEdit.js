import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, Keyboard, AsyncStorage } from 'react-native';
import { Container, Content, Accordion, Icon, Button } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11, dataArray, dataArrayMyPrefrence } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux'
import { apiCalls, apiCall, openToast } from '../../redux/actions';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment'
import ShowLoader from '../../components/ShowLoader';
import DrawerMenu from '../../components/Common/DrawerMenu'
var { height, width } = Dimensions.get('window');
const options = {
    mediaType: 'photo',
    includeBase64: true
};

class ProfileEdit extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        loader: false,
        isDateTimePickerVisible: false,
        activeIndex: 1,
        active: false,
        avatarSource: '',
        editProfileData: [],
        complexionData: [],
        dummyComplexionData: [],
        complexion: {
            label: '',
            value: ''
        },
        heightData: [],
        dummyHeightData:[],
        height: {
            label: '',
            value: ''
        },
        maritalData:[],
        dummyMaritalData:[],
        marital_status: {
            label: '',
            value: ''
        },
        languageData: [],
        dummyLanguageData: [],
        language: {
            label: '',
            value: ''
        },
        casteData: [],
        dummyCasteData: [],
        caste: {
            label: '',
            value: ''
        },
        educationData: [],
        dummyEducationData: [],
        education: {
            label: '',
            value: ''
        },
        securityQueData: [],
        dummySecurityQueData: [],
        securityQue: {
            label: '',
            value: ''
        },
        countryData: [],
        dummyCountryData: [],
        country: {
            label: '',
            value: ''
        },
        stateData: [],
        dummyStateData: [],
        state: {
            label: '',
            value: ''
        },
        cityData: [],
        dummyCityData: [],
        city: {
            label: '',
            value: ''
        },
        religionData: [],
        dummyReligionData: [],
        religion: {
            label: '',
            value: ''
        },
        genderData:[
            {
                label: 'Male',
                value: '1'
            },
            {
                label: 'Female',
                value: '2'
            }
        ],
        dummyGenderData: [
            {
                label: '',
                value: ''
            },
            {
                label: 'Male',
                value: '1'
            },
            {
                label: 'Female',
                value: '2'
            }
        ],
        gender: {
            value: '',
            label: ''
        },
        tagline: '',
        about: '',
        first_name: '',
        last_name: '',
        dob: '',
        habits: '',
        live_in: '',
        occupation: {
            label: '',
            value: ''
        },
        income: {
            label: '',
            value: ''
        },
        email: '',
        mobile: '',
        security_answer: '',


        ds_complexion: {
            label: '',
            value: ''
        },
        ds_height: {
            label: '',
            value: ''
        },
        ds_habits: '',
        ds_country: {
            label: '',
            value: ''
        },
        ds_stateData: [],
        ds_dummyStateData: [],
        ds_state: {
            label: '',
            value: ''
        },
        ds_cityData: [],
        ds_dummyCityData: [],
        ds_city: {
            label: '',
            value: ''
        },
        ds_marital_status: {
            label: '',
            value: ''
        },
        ds_language: {
            label: '',
            value: ''
        },
        ds_caste: {
            label: '',
            value: ''
        },
        ds_education: {
            label: '',
            value: ''
        },
        ds_occupation: {
            label: '',
            value: ''
        },
        occupationData: [],
        dummyOccupationData: [],
        ds_income: {
            label: '',
            value: ''
        },
        dummyIncomeData: [],
        incomeData: [],
        ds_religion: {
            label: '',
            value: ''
        },
        ds_gender: {
            label: '',
            value: ''
        },
        userDetail: {}
    }

    UNSAFE_componentWillMount(){
        this.apiGetEditProfileData()
        this.setUserDetail()
    }
    async setUserDetail(){
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        this.setState({ userDetail: user })
    }
    showDateTimePicker = () => {
        Keyboard.dismiss();
        this.setState({ isDateTimePickerVisible: true });
    };
    
    hideDateTimePicker = () => {
        Keyboard.dismiss();
        this.setState({ isDateTimePickerVisible: false });
    };
    
    handleDatePicked = date => {
        this.setState({ dob: moment(date).format('DD-MM-YYYY') });
        this.hideDateTimePicker();
    };

    async apiGetEditProfileData(){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({action: "getEditProfileData", user_id})).then(res => {
            let newObj = {label: '', value: ''}
            let { data } = res.data
            if(data){
                this.setState({ editProfileData: res.data })
            }
            if(data.complexion){
                let saveData = data.complexion.map(item => {
                    return {
                        label: item.type, value: item.id
                    }
                })
                this.setState({ complexionData: saveData, dummyComplexionData: [newObj, ...saveData] })
            }
            if(data.height){
                let saveData = data.height.map(item => {
                    return {
                        label: item.height, value: item.id
                    }
                })
                this.setState({ heightData: saveData, dummyHeightData: [newObj, ...saveData] })
            }
            if(data.marital_status){
                let saveData = data.marital_status.map(item => {
                    return {
                        label: item.status, value: item.id
                    }
                })
                this.setState({ maritalData: saveData, dummyMaritalData: [newObj, ...saveData] })
            }
            if(data.language){
                let saveData = data.language.map(item => {
                    return {
                        label: item.language, value: item.id
                    }
                })
                this.setState({ languageData: saveData, dummyLanguageData: [newObj, ...saveData] })
            }
            if(data.caste){
                let saveData = data.caste.map(item => {
                    return {
                        label: item.community, value: item.id
                    }
                })
                this.setState({ casteData: saveData, dummyCasteData: [newObj, ...saveData] })
            }
            if(data.education){
                let saveData = data.education.map(item => {
                    return {
                        label: item.education, value: item.id
                    }
                })
                this.setState({ educationData: saveData, dummyEducationData: [newObj, ...saveData] })
            }
            if(data.occupation){
                let saveData = data.occupation.map(item => {
                    return {
                        label: item.occupation, value: item.id
                    }
                })
                this.setState({ occupationData: saveData, dummyOccupationData: [newObj, ...saveData] })
            }
            if(data.income){
                let saveData = data.income.map(item => {
                    return {
                        label: item.income, value: item.id
                    }
                })
                this.setState({ incomeData: saveData, dummyIncomeData: [newObj, ...saveData] })
            }
            if(data.securityQuestions){
                let saveData = data.securityQuestions.map(item => {
                    return {
                        label: item.question, value: item.id
                    }
                })
                this.setState({ securityQueData: saveData, dummySecurityQueData: [newObj, ...saveData] })
            }
            if(data.religion){
                let saveData = data.religion.map(item => {
                    return {
                        label: item.religion, value: item.id
                    }
                })
                this.setState({ religionData: saveData, dummyReligionData: [newObj, ...saveData] })
            }
            if(data.countries){
                let saveData = data.countries.map(item => {
                    return {
                        label: item.name, value: item.id
                    }
                })
                this.setState({ countryData: saveData, dummyCountryData: [newObj, ...saveData] })
            }
            if(data.user.country){
                this.setState({ loader: true })
                this.props.dispatch(apiCall({ action: 'getStateByCountry', country_id: data.user.country })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ stateData: saveData, dummyStateData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }
            if(data.user.state){
                this.setState({ loader: true })
                this.props.dispatch(apiCall({ action: 'getCityByState', state_id: data.user.state })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ cityData: saveData, dummyCityData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }
            setTimeout(() => {
                let { description, brief_description, first_name, last_name, dob, habits, live_in, email, mobile, security_answer } = data.user
                this.setState({ about: brief_description, tagline: description, first_name, last_name, dob, habits, live_in, email, mobile, security_answer, loader: false })
                this.getDropdownList(data.user)
            }, 1000);
            
        }).catch(err => {
            this.setState({ loader: false})
        })
    }

    getDropdownList(user){
        let { complexionData, heightData, maritalData,languageData, educationData, casteData, securityQueData, countryData, stateData, cityData, genderData, religionData, occupationData, incomeData } = this.state;
        if(user.complexion){
            let findObj = complexionData.find(itm => itm.value === user.complexion)
            if(findObj){
                this.setState({ complexion: findObj })
            }
        }
        if(user.height){
            let findObj = heightData.find(itm => itm.value === user.height)
            if(findObj){
                this.setState({ height: findObj })
            }
        }
        if(user.marital_status){
            let findObj = maritalData.find(itm => itm.value === user.marital_status)
            if(findObj){
                this.setState({ marital_status: findObj })
            }
        }
        if(user.mother_tongue){
            let findObj = languageData.find(itm => itm.value === user.mother_tongue)
            if(findObj){
                this.setState({ language: findObj })
            }
        }
        if(user.caste){
            let findObj = casteData.find(itm => itm.value === user.caste)
            if(findObj){
                this.setState({ caste: findObj })
            }
        }
        if(user.education){
            let findObj = educationData.find(itm => itm.value === user.education)
            if(findObj){
                this.setState({ education: findObj })
            }
        }
        if(user.occupation){
            let findObj = occupationData.find(itm => itm.value === user.occupation)
            if(findObj){
                this.setState({ occupation: findObj })
            }
        }
        if(user.income){
            let findObj = incomeData.find(itm => itm.value === user.income)
            if(findObj){
                this.setState({ income: findObj })
            }
        }
        if(user.security_question){
            let findObj = securityQueData.find(itm => itm.value === user.security_question)
            if(findObj){
                this.setState({ securityQue: findObj })
            }
        }
        if(user.gender){
            let findObj = genderData.find(itm => itm.value === user.gender)
            if(findObj){
                this.setState({ gender: findObj })
            }
        }
        if(user.country){
            let findObj = countryData.find(itm => itm.value === user.country)
            if(findObj){
                this.setState({ country: findObj })
            }
        }
        if(user.state){
            let findObj = stateData.find(itm => itm.value === user.state)
            if(findObj){
                this.setState({ state: findObj })
            }
            
        }
        if(user.city){
            let findObj = cityData.find(itm => itm.value === user.city)
            if(findObj){
                this.setState({ city: findObj })
            }
        }
        if(user.religion){
            let findObj = religionData.find(itm => itm.value === user.religion)
            if(findObj){
                this.setState({ religion: findObj })
            }
        }
        
    }

    _renderHeader(item, expanded, index) {
        return (
          <View style={[expanded ? {marginBottom: 0, borderBottomLeftRadius: 0, borderBottomRightRadius: 0}: {marginBottom: 10, borderRadius: 5},{ flexDirection: "row", padding: 10, justifyContent: "space-between", alignItems: "center", borderRadius: 5,backgroundColor: '#ededed', paddingBottom: 15 }]}>
            <View style={{position: 'absolute', top: 10, left: 10}}>
                <Image source={item.icon} style={{height: 25, width: 25}}/>
            </View>
            <Text style={[{ fontWeight: "500", fontSize: fontSize14, marginLeft: 30, color: primaryColor, paddingTop: 2 },!expanded && {paddingTop: 3}]}>
                {" "}{item.title}
            </Text>
            {expanded
              ? <Image source={require('../../assets/images/caret-up.png')} style={{height: 7, width: 13, marginTop: 5}}/>
              : <Image source={require('../../assets/images/caret-down.png')} style={{height: 7, width: 13, marginTop: 5}}/>}
          </View>
        );
    }

    _renderHeaderMyPrefrence(item, expanded) {
        return (
          <View style={[expanded ? {marginBottom: 0, borderBottomLeftRadius: 0, borderBottomRightRadius: 0}: {marginBottom: 10, borderRadius: 5},{ flexDirection: "row", padding: 10, justifyContent: "space-between", alignItems: "center", borderRadius: 5,backgroundColor: '#ededed', paddingBottom: 15 }]}>
            <View style={{position: 'absolute', top: 10, left: 10}}>
                <Image source={item.icon} style={{height: 25, width: 25}}/>
            </View>
            <Text style={[{ fontWeight: "500", fontSize: fontSize14, marginLeft: 30, color: primaryColor, paddingTop: 2 },!expanded && {paddingTop: 3}]}>
                {" "}{item.title}
            </Text>
            {expanded
              ? <Image source={require('../../assets/images/caret-up.png')} style={{height: 7, width: 13, marginTop: 5}}/>
              : <Image source={require('../../assets/images/caret-down.png')} style={{height: 7, width: 13, marginTop: 5}}/>}
          </View>
        );
    }
    
    _renderContent (item) {
        if(item.title == 'TAGLINE') {
            return this.renderTaglineContent()
        }
        if(item.title == 'ABOUT') {
            return this.renderAboutContent()
        }
        if(item.title == 'BASIC INFORMATION') {
            return this.renderBasicInfoContent()
        }
        if(item.title == 'PHYSICAL APPEARANCE') {
            return this.renderPhysicalContent()
        }
        if(item.title == 'CONTACT INFORMATION') {
            return this.renderContactInfoContent()
        }
        if(item.title == 'LOCATION') {
            return this.renderLocationContent()
        }
        if(item.title == 'ADDITIONAL INFORMATION') {
            return this.renderAdditionalInfoContent()
        }
        if(item.title == 'SECURITY') {
            return this.renderSecurityContent()
        }
    }

    _renderContentMyPrefrence (item) {
        if(item.title == 'PHYSICAL APPEARANCE') {
            return this.renderDsPhysicalContent()
        }
        if(item.title == 'ADDITIONAL INFORMATION') {
            return this.renderDsAdditionalInfoContent()
        }
        if(item.title == 'LOCATION') {
            return this.renderDsLocationContent()
        }
    }

    

    onChange = (event,index, keyCode, data) => {
        let findObj = data.find((itm,key) => key === index)
        if(keyCode === 'complexion'){
            if(index){
                this.setState({ complexion: findObj })
            }else{
                this.setState({ complexion: {label: '',value: ''} })
            }
        }
        if(keyCode === 'height'){
            if(index){
                this.setState({ height: findObj })
            }else{
                this.setState({ height: {label: '',value: ''} })
            }
        }
        if(keyCode === 'marital_status'){
            if(index){
                this.setState({ marital_status: findObj })
            }else{
                this.setState({ marital_status: {label: '',value: ''} })
            }
        }
        if(keyCode === 'language'){
            if(index){
                this.setState({ language: findObj })
            }else{
                this.setState({ language: {label: '',value: ''} })
            }
        }
        if(keyCode === 'caste'){
            if(index){
                this.setState({ caste: findObj })
            }else{
                this.setState({ caste: {label: '',value: ''} })
            }
        }
        if(keyCode === 'education'){
            if(index){
                this.setState({ education: findObj })
            }else{
                this.setState({ education: {label: '',value: ''} })
            }
        }
        if(keyCode === 'occupation'){
            if(index){
                this.setState({ occupation: findObj })
            }else{
                this.setState({ occupation: {label: '',value: ''} })
            }
        }
        if(keyCode === 'income'){
            if(index){
                this.setState({ income: findObj })
            }else{
                this.setState({ income: {label: '',value: ''} })
            }
        }
        if(keyCode === 'religion'){
            if(index){
                this.setState({ religion: findObj })
            }else{
                this.setState({ religion: {label: '',value: ''} })
            }
        }
        if(keyCode === 'securityQue'){
            if(index){
                this.setState({ securityQue: findObj })
            }else{
                this.setState({ securityQue: {label: '',value: ''} })
            }
        }
        if(keyCode === 'gender'){
            if(index){
                this.setState({ gender: findObj })
            }else{
                this.setState({ gender: {label: '',value: ''} })
            }
        }
        if(keyCode === 'country'){
            if(index){
                this.setState({ country: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getStateByCountry', country_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ stateData: saveData, dummyStateData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ country: {label: '',value: ''} })
            }
        }
        if(keyCode === 'state'){
            if(index){
                this.setState({ state: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getCityByState', state_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ cityData: saveData, dummyCityData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ state: {label: '',value: ''} })
            }
        }
        if(keyCode === 'city'){
            if(index){
                this.setState({ city: findObj })
            }else{
                this.setState({ city: {label: '',value: ''} })
            }
        }
        
    }

    onChangeMyPrefrence = (event,index, keyCode, data) => {
        let findObj = data.find((itm,key) => key === index)
        if(keyCode === 'complexion'){
            if(index){
                this.setState({ ds_complexion: findObj })
            }else{
                this.setState({ ds_complexion: {label: '',value: ''} })
            }
        }
        if(keyCode === 'height'){
            if(index){
                this.setState({ ds_height: findObj })
            }else{
                this.setState({ ds_height: {label: '',value: ''} })
            }
        }
        if(keyCode === 'marital_status'){
            if(index){
                this.setState({ ds_marital_status: findObj })
            }else{
                this.setState({ ds_marital_status: {label: '',value: ''} })
            }
        }
        if(keyCode === 'religion'){
            if(index){
                this.setState({ ds_religion: findObj })
            }else{
                this.setState({ ds_religion: {label: '',value: ''} })
            }
        }
        if(keyCode === 'language'){
            if(index){
                this.setState({ ds_language: findObj })
            }else{
                this.setState({ ds_language: {label: '',value: ''} })
            }
        }
        if(keyCode === 'caste'){
            if(index){
                this.setState({ ds_caste: findObj })
            }else{
                this.setState({ ds_caste: {label: '',value: ''} })
            }
        }
        if(keyCode === 'education'){
            if(index){
                this.setState({ ds_education: findObj })
            }else{
                this.setState({ ds_education: {label: '',value: ''} })
            }
        }
        if(keyCode === 'occupation'){
            if(index){
                this.setState({ ds_occupation: findObj })
            }else{
                this.setState({ ds_occupation: {label: '',value: ''} })
            }
        }
        if(keyCode === 'income'){
            if(index){
                this.setState({ ds_income: findObj })
            }else{
                this.setState({ ds_income: {label: '',value: ''} })
            }
        }
        if(keyCode === 'gender'){
            if(index){
                this.setState({ ds_gender: findObj })
            }else{
                this.setState({ ds_gender: {label: '',value: ''} })
            }
        }
        if(keyCode === 'country'){
            if(index){
                this.setState({ ds_country: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getStateByCountry', country_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ ds_stateData: saveData, ds_dummyStateData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ ds_country: {label: '',value: ''} })
            }
        }
        if(keyCode === 'state'){
            if(index){
                this.setState({ ds_state: findObj, loader: true })
                this.props.dispatch(apiCall({ action: 'getCityByState', state_id: findObj.value })).then(res => {
                    this.setState({ loader: false })
                    if(res.data){
                        let saveData = res.data.map(item => {
                            return {
                                label: item.name, value: item.id
                            }
                        })
                        let newObj = {label: '', value: ''}
                        this.setState({ ds_cityData: saveData, ds_dummyCityData: [newObj, ...saveData] })
                    }
                }).catch(err => {
                    this.setState({ loader: false })
                })
            }else{
                this.setState({ ds_state: {label: '',value: ''} })
            }
        }
        if(keyCode === 'city'){
            if(index){
                this.setState({ ds_city: findObj })
            }else{
                this.setState({ ds_city: {label: '',value: ''} })
            }
        }
        
    }

    async onSubmit(data){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({...data, action: 'editUserProfile', user_id})).then(res => {
            this.setState({ loader: false })
            if(res.data){
                this.props.dispatch(openToast('Profile Updated successfully!'))
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    async onSubmitMyPrefrence(data){
        this.setState({ loader: true })
        let user_id = await AsyncStorage.getItem('user_id')
        this.props.dispatch(apiCall({...data, action: 'savePartnerUserProfile', user_id})).then(res => {
            this.setState({ loader: false })
            if(res.data){
                this.props.dispatch(openToast('Profile Updated successfully!'))
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    renderTaglineContent() {
        let { tagline } = this.state;
        let data = { tagline }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <TextInput multiline={true} style={{backgroundColor: whiteColor, minHeight: 50, borderRadius: 5, paddingLeft: 10, paddingRight: 10, fontSize: fontSize11}} value={tagline} onChangeText={(event) => this.setState({ tagline: event })}/>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 10, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    renderAboutContent() {
        let { about } = this.state;
        let data = { about }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <TextInput multiline={true} style={{backgroundColor: whiteColor, minHeight: 50, borderRadius: 5, paddingLeft: 10, paddingRight: 10, fontSize: fontSize11}} value={about} onChangeText={(event) => this.setState({ about: event })}/>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 10, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    renderBasicInfoContent(){
        let { first_name, last_name, gender, dob, dummyGenderData, genderData } = this.state;
        let data = { first_name, last_name, gender: gender.value, dob }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>First Name:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} value={first_name} onChangeText={(event) => this.setState({ first_name: event })}/>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Last Name:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} value={last_name} onChangeText={(event) => this.setState({ last_name: event })}/>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Gender:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'gender', dummyGenderData)}
                            items={genderData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{gender.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>DOB:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} onFocus={this.showDateTimePicker} value={dob}/>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    renderLocationContent(){
        let { dummyCountryData, countryData, country, stateData, dummyStateData, state, city, cityData, dummyCityData, live_in } = this.state;
        let data = { country: country.value, state: state.value, city: city.value, live_in }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Country:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'country', dummyCountryData)}
                            items={countryData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{country.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>State:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'state', dummyStateData)}
                            items={stateData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{state.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>City:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'city', dummyCityData)}
                            items={cityData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{city.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Live-in:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} value={live_in} onChangeText={(event) => this.setState({ live_in: event })}/>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    renderDsLocationContent(){
        let { dummyCountryData, countryData, ds_country, ds_stateData, ds_dummyStateData, ds_state, ds_city, ds_cityData, ds_dummyCityData } = this.state;
        let data = { ds_country: ds_country.value, ds_state: ds_state.value, ds_city: ds_city.value }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Country:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'country', dummyCountryData)}
                            items={countryData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{ds_country.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>State:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'state', ds_dummyStateData)}
                            items={ds_stateData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{ds_state.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>City:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'city', ds_dummyCityData)}
                            items={ds_cityData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{ds_city.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                {/* <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Live-in:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}} value={live_in} onChangeText={(event) => this.setState({ live_in: event })}/>
                    </View>
                </View> */}
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmitMyPrefrence(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    renderPhysicalContent(){
        let { complexionData, complexion, dummyComplexionData, height, heightData, dummyHeightData, habits } = this.state;
        let data = { complexion: complexion.value, height: height.value, habit: habits }
        return(
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                        <Text style={{fontSize: fontSize11}}>Complexion:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'complexion', dummyComplexionData)}
                            items={complexionData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{complexion.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                        
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                        <Text style={{fontSize: fontSize11}}>Height:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'height', dummyHeightData)}
                            items={heightData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{height.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                        <Text style={{fontSize: fontSize11}}>Habit:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} value={habits} onChangeText={(event) => this.setState({ habits: event })}/>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View> 
        )
    }

    renderDsPhysicalContent(){
        let { complexionData, ds_complexion, dummyComplexionData, ds_height, heightData, dummyHeightData, ds_habits, ds_gender, genderData, dummyGenderData } = this.state;
        let data = { ds_complexion: ds_complexion.value, ds_height: ds_height.value, ds_habit: ds_habits, ds_gender: ds_gender.value }
        return(
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                        <Text style={{fontSize: fontSize11}}>Complexion:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'complexion', dummyComplexionData)}
                            items={complexionData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{ds_complexion.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                        
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                        <Text style={{fontSize: fontSize11}}>Height:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'height', dummyHeightData)}
                            items={heightData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{ds_height.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Gender:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'gender', dummyGenderData)}
                            items={genderData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{ds_gender.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                        <Text style={{fontSize: fontSize11}}>Habit:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} value={ds_habits} onChangeText={(event) => this.setState({ ds_habits: event })}/>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmitMyPrefrence(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View> 
        )
    }

    renderAdditionalInfoContent(){
        let { maritalData, dummyMaritalData, marital_status, languageData, dummyLanguageData, language, casteData, dummyCasteData, caste, education, educationData, dummyEducationData, occupation, income, religion, religionData, dummyReligionData, occupationData, dummyOccupationData, incomeData, dummyIncomeData } = this.state;
        let data = { marital_status: marital_status.value, language: language.value, caste: caste.value, education: education.value, occupation: occupation.value, income: income.value, community: religion.value }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Marital Status:</Text>
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'marital_status', dummyMaritalData)}
                            items={maritalData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{marital_status.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>

                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Language:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'language', dummyLanguageData)}
                            items={languageData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{language.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Caste:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'caste', dummyCasteData)}
                            items={casteData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{caste.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Education:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'education', dummyEducationData)}
                            items={educationData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{education.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Occupation:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'occupation', dummyOccupationData)}
                            items={occupationData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{occupation.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Income:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'income', dummyIncomeData)}
                            items={incomeData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{income.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Religion:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'religion', dummyReligionData)}
                            items={religionData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{religion.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    renderDsAdditionalInfoContent(){
        let { maritalData, dummyMaritalData, ds_marital_status, languageData, dummyLanguageData, ds_language, casteData, dummyCasteData, ds_caste, ds_education, educationData, dummyEducationData, ds_occupation, ds_income, ds_religion, religionData, dummyReligionData, occupationData, dummyOccupationData, incomeData, dummyIncomeData } = this.state;
        let data = { ds_marital_status: ds_marital_status.value, ds_language: ds_language.value, ds_caste: ds_caste.value, ds_education: ds_education.value, ds_occupation: ds_occupation.value, ds_income: ds_income.value, ds_community: ds_religion.value }
        // console.log(casteData,caste,"casteData")
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Marital Status:</Text>
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'marital_status', dummyMaritalData)}
                            items={maritalData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center'}}>
                                <Text style={{fontSize: fontSize11}}>{ds_marital_status.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>

                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Language:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'language', dummyLanguageData)}
                            items={languageData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{ds_language.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Caste:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'caste', dummyCasteData)}
                            items={casteData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{ds_caste.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Education:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'education', dummyEducationData)}
                            items={educationData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{ds_education.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Occupation:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'occupation', dummyOccupationData)}
                            items={occupationData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{ds_occupation.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Income:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'income', dummyIncomeData)}
                            items={incomeData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{ds_income.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Religion:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChangeMyPrefrence(e, i, 'religion', dummyReligionData)}
                            items={religionData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{ds_religion.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmitMyPrefrence(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }
    

    renderContactInfoContent(){
        let { mobile, email } = this.state;
        let data = { email, phone_number: mobile }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Email:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} value={email} onChangeText={(event) => this.setState({ email: event })}/>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Phone No:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} keyboardType="numeric" value={mobile} onChangeText={(event) => this.setState({ mobile: event })}/>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    renderSecurityContent(){
        let { securityQue, dummySecurityQueData, securityQueData, security_answer } = this.state;
        let data = { security_question: securityQue.value, security_answer }
        return (
            <View style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10, backgroundColor: "#ededed", paddingBottom: 0, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Security Qus:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <RNPickerSelect
                            onValueChange={(e,i) => this.onChange(e, i, 'securityQue', dummySecurityQueData)}
                            items={securityQueData}
                            style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11}}
                        >
                            <View style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, justifyContent: 'center', paddingRight: 30}}>
                                <Text style={{fontSize: fontSize11}}>{securityQue.label}</Text>
                                <Image source={require('../../assets/images/caret-down-select.png')} style={{position: 'absolute', right: 10, top: 10, height: 7, width: 13}}/>
                            </View>
                            
                        </RNPickerSelect>
                    </View>
                </View>
                <View style={{height: 30, flexDirection: 'row', width: '100%'}}>
                    <View style={{position: 'absolute', left: 0, top: 5, width: 80}}>
                       <Text style={{fontSize: fontSize11}}>Security Ans:</Text> 
                    </View>
                    <View style={{ width: '100%', paddingLeft: 85}}>
                        <TextInput style={{backgroundColor: whiteColor, borderRadius: 5, paddingLeft: 5, paddingRight: 0, height: 25, fontSize: fontSize11, paddingTop: 0, paddingBottom: 0}} value={security_answer} onChangeText={(event) => this.setState({ security_answer: event })}/>
                    </View>
                </View>
                <Button style={{width: 100, backgroundColor: primaryColor, height: 25, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', marginTop: 5, marginBottom: 10}} onPress={() => this.onSubmit(data)}>
                    <Text style={{color: whiteColor, fontSize: fontSize12}}>Save</Text>
                </Button>
            </View>
        )
    }

    setActiveIndex = async (index) => {
        let user_id = await AsyncStorage.getItem('user_id')
        if(index === 2){
            this.setState({ loader: true })
            this.props.dispatch(apiCall({action: 'getParterPrefilledData', user_id})).then(res => {
                this.setState({ loader: false })
                let { data } = res.data
                if(data.ds_user.ds_country){
                    this.setState({ loader: true })
                    this.props.dispatch(apiCall({ action: 'getStateByCountry', country_id: data.ds_user.ds_country })).then(res => {
                        this.setState({ loader: false })
                        if(res.data){
                            let saveData = res.data.map(item => {
                                return {
                                    label: item.name, value: item.id
                                }
                            })
                            let newObj = {label: '', value: ''}
                            this.setState({ ds_stateData: saveData, ds_dummyStateData: [newObj, ...saveData] })
                        }
                    }).catch(err => {
                        this.setState({ loader: false })
                    })
                }
                if(data.ds_user.ds_state){
                    this.setState({ loader: true })
                    this.props.dispatch(apiCall({ action: 'getCityByState', state_id: data.ds_user.ds_state })).then(res => {
                        this.setState({ loader: false })
                        if(res.data){
                            let saveData = res.data.map(item => {
                                return {
                                    label: item.name, value: item.id
                                }
                            })
                            let newObj = {label: '', value: ''}
                            this.setState({ ds_cityData: saveData, ds_dummyCityData: [newObj, ...saveData] })
                        }
                    }).catch(err => {
                        this.setState({ loader: false })
                    })
                }
                setTimeout(() => {
                    let { ds_habits } = data.ds_user
                    this.setState({ ds_habits, loader: false })
                    this.getDropdownListMyPrefrence(data.ds_user)
                }, 1000);
                
            }).catch(err => {
            })
        }
        this.setState({ activeIndex: index })
    }

    getDropdownListMyPrefrence(user){
        let { complexionData, heightData, maritalData,languageData, educationData, casteData, securityQueData, countryData, stateData, cityData, genderData, religionData, ds_stateData, ds_cityData, occupationData, ds_income } = this.state;
        if(user.ds_complexion){
            let findObj = complexionData.find(itm => itm.value === user.ds_complexion)
            if(findObj){
                this.setState({ ds_complexion: findObj })
            }
        }
        if(user.ds_height){
            let findObj = heightData.find(itm => itm.value === user.ds_height)
            if(findObj){
                this.setState({ ds_height: findObj })
            }
        }
        if(user.ds_marital_status){
            let findObj = maritalData.find(itm => itm.value === user.ds_marital_status)
            if(findObj){
                this.setState({ ds_marital_status: findObj })
            }
        }
        if(user.ds_mother_tongue){
            let findObj = languageData.find(itm => itm.value === user.ds_mother_tongue)
            if(findObj){
                this.setState({ ds_language: findObj })
            }
        }
        if(user.ds_caste){
            let findObj = casteData.find(itm => itm.value === user.ds_caste)
            if(findObj){
                this.setState({ ds_caste: findObj })
            }
        }
        if(user.ds_education){
            let findObj = educationData.find(itm => itm.value === user.ds_education)
            if(findObj){
                this.setState({ ds_education: findObj })
            }
        }
        if(user.ds_occupation){
            let findObj = occupationData.find(itm => itm.value === user.ds_occupation)
            if(findObj){
                this.setState({ ds_occupation: findObj })
            }
        }
        if(user.ds_income){
            let findObj = incomeData.find(itm => itm.value === user.ds_income)
            if(findObj){
                this.setState({ ds_income: findObj })
            }
        }
        if(user.ds_gender){
            let findObj = genderData.find(itm => itm.value === user.ds_gender)
            if(findObj){
                this.setState({ ds_gender: findObj })
            }
        }
        if(user.ds_country){
            let findObj = countryData.find(itm => itm.value === user.ds_country)
            if(findObj){
                this.setState({ ds_country: findObj })
            }
        }
        if(user.ds_state){
            let findObj = ds_stateData.find(itm => itm.value === user.ds_state)
            if(findObj){
                this.setState({ ds_state: findObj })
            }
            
        }
        if(user.ds_city){
            let findObj = ds_cityData.find(itm => itm.value === user.ds_city)
            if(findObj){
                this.setState({ ds_city: findObj })
            }
        }
        if(user.ds_religion){
            let findObj = religionData.find(itm => itm.value === user.ds_religion)
            if(findObj){
                this.setState({ ds_religion: findObj })
            }
        }
        
    }

    onEditImage = () => {
        let {dispatch} = this.props;
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
          }).then(image => {
            console.log(image,"dshfjhkhdgfhs");
          });
          
        // launchImageLibrary(options, (response) => {
        //    console.log(response,"response")
            // if (response.didCancel) {
            // } else if (response.error) {
            // } else if (response.customButton) {
            // } else {
            //   const source = { uri: 'data:image/jpeg;base64,' + response.base64 }
            //   console.log(response.base64,"response.data")
            //   const abc = { uri: 'data:image/jpeg;base64,' + response.base64 }
            //   console.log(abc,"abc")
           
            //   // You can also display the image using data:
            //   // const source = { uri: 'data:image/jpeg;base64,' + response.data };
           
            //   this.setState({
            //     avatarSource: source, loader: true
            //   });
            //   dispatch(apiCalls(source.uri)).then(async res => {
            //       console.log(res,"res")
            //       this.setState({ loader: false})
            //       let user = await AsyncStorage.getItem('user')
            //       user = JSON.parse(user)
            //       if(res.data.data){
            //         this.props.dispatch(openToast('Image Updated Successfully!'))
            //         let data = {...user,profile_image: res.data.data}
            //         console.log(data,"data")
            //         await AsyncStorage.setItem('user', JSON.stringify(data))
            //         this.setState({ userDetail: data })
            //       }
            //   }).catch(err => {
            //     this.setState({ loader: false})
            //     console.log(err,"errr")
            //     this.props.dispatch(openToast('Error Uploading Image!'))
            //   })
            // }
        //   });
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { activeIndex, userDetail } = this.state;
        let child = <Content>
                    <View style={{flex: 1}}>
                        <View style={styles.topRedView}>
                            <SignupHeader navigation={navigation} label="Edit Profile" />
                            <View style={styles.imageCenterView}>
                                <TouchableOpacity style={styles.userImgCon} onPress={this.onEditImage}>
                                  <Image source={userDetail.profile_image ? {uri: userDetail.profile_image} : require('../../assets/images/card_img.png')} style={{width: '100%', height: 100}} />
                                  <Image source={require('../../assets/images/framePrimary.png')} style={styles.frame} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.segmentOuter}>
                                <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                    <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>My Self</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={activeIndex == 2 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(2)}>
                                    <Text style={activeIndex == 2 ? styles.activeText : styles.unActiveText}>My Prefrences</Text>
                                </TouchableOpacity>
                            </View>
                            {/* <Accordion dataArray={dataArray} expanded={true} animation={true}/> */}
                        </View>
                        {activeIndex === 1 &&
                            <View style={styles.contentView}>
                                <View style={styles.innerContentView}>
                                    <View style={styles.innerViewAcc}>
                                        <Accordion
                                            dataArray={dataArray}
                                            animation={true}
                                            expanded={[0]}
                                            renderHeader={this._renderHeader}
                                            renderContent={this._renderContent.bind(this)}
                                            style={{borderWidth: 0}}
                                            headerStyle={{flexDirection: 'row', justifyContent: 'flex-start'}}
                                        />
                                    </View>
                                </View>
                            </View>}
                            {activeIndex === 2 && <View style={styles.contentView}>
                            <View style={styles.innerContentView}>
                                <View style={styles.innerViewAcc}>
                                    <Accordion
                                        dataArray={dataArrayMyPrefrence}
                                        animation={true}
                                        expanded={[0]}
                                        renderHeader={this._renderHeaderMyPrefrence.bind(this)}
                                        renderContent={this._renderContentMyPrefrence.bind(this)}
                                        style={{borderWidth: 0}}
                                        headerStyle={{flexDirection: 'row', justifyContent: 'flex-start'}}
                                    />
                                    {/* <Accordion dataArray={dataArray} expanded={[0]}/> */}
                                    {/* <Button light><Text> Light </Text></Button> */}
                                </View>
                            </View>
                        </View>}
                    </View>
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this.handleDatePicked}
                        onCancel={this.hideDateTimePicker}
                    />
                    {this.handelLoader()}
                </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(ProfileEdit)
const styles = StyleSheet.create({
    topRedView: {
        backgroundColor: primaryColor
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15,
        marginBottom: -20,
        borderWidth: 1,
        borderColor: '#e6e6e6'
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    contentView: {
        padding: 15,
        width: '100%',
        marginTop: 40,
        paddingTop: 0
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize8,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      position: 'relative'
    },
    halfImageCon: {
      width: 90,
      height: 100
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    marginBottom20: {
      marginBottom: 10
    },
    nameCardText: {
      color: primaryColor,
      fontSize: fontSize16,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    pIdText: {
      color: '#525252',
      fontSize: fontSize8,
      fontFamily: fontFamily
    },
    rightBottom: {
        position: 'absolute', 
        bottom:10, 
        margin: 0, 
        right: 10,
        height: 35,
        width: 35
    },
    rightBottomIcon: {
       width: 18,
       height: 18 
    },
    rightTop: {
        position: 'absolute',
        top: 4,
        right: 5,
        width: 35,
        height: 35
    },
    moreIcon: {
        fontSize: 40
    },
    userImgCon: {
      width: 100,
      alignItems: 'center',

    },
    frame: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: 100
    },
    imageCenterView: {
        width: '100%',
        alignItems: 'center'
    },
    innerContentView: {
        backgroundColor: whiteColor,
        borderWidth: 1,
        borderColor: '#e6e6e6',
        borderRadius: 5
    },
    innerViewAcc: {
        margin: 10,
        marginBottom: 0
    }
})