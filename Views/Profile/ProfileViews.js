import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput, ScrollView } from 'react-native';
import { Container, Content, Footer, Card, CardItem, Right, Icon, Left, Segment, Button, Fab } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize22, fontSize21, fontSize18, fontSize8, fontSize13, fontSize9, fontSize11 } from '../../redux/actions/constant';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer';
import DrawerMenu from '../../components/Common/DrawerMenu'
var { height, width } = Dimensions.get('window');

export default class ProfileViews extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        activeIndex: 1,
        active: false,
    }
    setActiveIndex = (index) => {
        this.setState({ activeIndex: index })
    }
    render(){
        let { navigation } = this.props;
        let child = <Content>
                    <View style={{flex: 1}}>
                        <View style={styles.topRedView} >
                            <SignupHeader navigation={navigation} previousScreen="Profile Views" messageHeader />
                            {/* <View style={styles.segmentOuter}>
                                <TouchableOpacity style={activeIndex == 1 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(1)}>
                                    <Text style={activeIndex == 1 ? styles.activeText : styles.unActiveText}>All Profiles</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={activeIndex == 2 ? styles.activeSegment : styles.unActiveSegment} onPress={() => this.setActiveIndex(2)}>
                                    <Text style={activeIndex == 2 ? styles.activeText : styles.unActiveText}>Recent Profiles</Text>
                                </TouchableOpacity>
                            </View> */}
                        </View>
                        <View style={styles.contentView}>
                        <View style={{ paddingTop:0}}>
                            <View style={styles.cartContainer}>
                                <View style={styles.halfImageCon}>
                                    <Image source={require('../../assets/images/card_img.png')} style={{width: '100%', height: '100%'}} />
                                </View>
                                <View style={styles.rightConCard}>
                                    <View style={styles.marginBottom20}>
                                    <Text style={styles.nameCardText}>Name name</Text>
                                    <Text style={styles.pIdText}>PROFILE ID: CSD57DG</Text>
                                    </View>
                                    <View>
                                    <Text style={[styles.pIdText,{fontSize: fontSize9}]}>26 yesr, 5.9", Christian, Protestant</Text>
                                    <Text style={[styles.pIdText,{fontSize: fontSize11, fontWeight: '500'}]}>MCA/PGDCA, Rs. 15 - 20 Lakh,</Text>
                                    <Text style={[styles.pIdText,{fontSize: fontSize11, fontWeight: '500'}]}>Software</Text>
                                    </View>
                                </View>
                                {/* <TouchableOpacity style={[styles.favCon, styles.rightBottom]} activeOpacity={0.7}>
                                    <Image source={require('../../assets/images/heart.png')} style={[styles.heartIcon, styles.rightBottomIcon]} />
                                </TouchableOpacity> */}

                                {/* <TouchableOpacity style={[styles.rightTop]} activeOpacity={0.7}>
                                    <Icon name="more" style={styles.moreIcon} />
                                </TouchableOpacity> */}
                            </View>
                            
                        </View>
                        </View>
                    </View>
                </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
const styles = StyleSheet.create({
    topRedView: {
        // paddingBottom: 50,
        backgroundColor: primaryColor,
        
    },
    segmentOuter: {
        padding: 7,
        flexDirection: 'row',
        backgroundColor: whiteColor,
        borderRadius: 5,
        margin: 15
    },
    activeSegment: {
        backgroundColor: primaryColor,
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    unActiveSegment: {
        borderRadius: 5,
        width: '50%',
        padding: 7,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeText: {
        color: whiteColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    unActiveText: {
        color: primaryColor,
        fontSize: fontSize16,
        fontWeight: '400',
        fontFamily: fontFamily
    },
    cardOuter: {
        height: 250,
        borderRadius: 5,
        overflow: 'hidden',
        marginBottom: 15
    },
    contentView: {
        padding: 15,
        // marginTop: -60,
        width: '100%'
    },
    inerCardView: {
        position: 'absolute',
        bottom: 20,
        left: 20
    },
    marginBottom20: {
        marginBottom: 20
    },
    nameText: {
        fontSize: fontSize21,
        color: whiteColor,
        fontFamily: fontFamily,
        fontWeight: '500'
    },
    profileIdText: {
        fontSize: fontSize8,
        color: whiteColor,
        fontFamily: fontFamily
    },
    yearText: {
        color: whiteColor,
        fontSize: fontSize13,
        fontFamily: fontFamily
    },
    eduText: {
        color: whiteColor,
        fontSize: fontSize14,
        fontWeight: '500',
        fontFamily: fontFamily
    },
    favCon: {
        height: 45,
        width: 45,
        borderRadius: 30,
        backgroundColor: '#d6d5d6',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    heartIcon: {
        width: 22,
        height: 22
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      position: 'relative',
      height: 110
    },
    halfImageCon: {
      width: 90,
      height: 110
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    marginBottom20: {
      marginBottom: 10
    },
    nameCardText: {
      color: primaryColor,
      fontSize: fontSize16,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    pIdText: {
      color: '#525252',
      fontSize: fontSize8,
      fontFamily: fontFamily
    },
    rightBottom: {
        position: 'absolute', 
        bottom:10, 
        margin: 0, 
        right: 10,
        height: 35,
        width: 35
    },
    rightBottomIcon: {
       width: 18,
       height: 18 
    },
    rightTop: {
        position: 'absolute',
        top: 4,
        right: 5,
        width: 35,
        height: 35
    },
    moreIcon: {
        fontSize: 40
    }
})