import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Button, Icon, List, ListItem, Segment } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize9, fontSize18, fontSize11, fontSize24, fontSize32 } from '../../redux/actions/constant';
import MainFooter from '../../components/Footer'
import SignupHeader from '../../components/Headers/SignupHeader';
import DrawerMenu from '../../components/Common/DrawerMenu'
import { apiCall } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
import moment from 'moment';
import ListView from 'deprecated-react-native-listview'
var { height, width } = Dimensions.get('window');

class Notification extends React.Component{
    static navigationOptions = {
        header: null
		}
		constructor(props) {
			super(props);
			this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
			this.state = {
				listViewData: [],
			};
		}

		async componentDidMount(){
			this.setState({loader: true})
			let user_id = await AsyncStorage.getItem('user_id')
			this.props.dispatch(apiCall({action: 'messageNotification', user_id})).then(res => {
				console.log(res,"res")
				this.setState({loader: false})
				this.setState({ listViewData: res.data.data })
				console.log(res,"res")
			}).catch(err => {
				this.setState({loader: false})
			})
		}


		deleteRow(secId, rowId, rowMap) {
			rowMap[`${secId}${rowId}`].props.closeRow();
			const newData = [...this.state.listViewData];
			newData.splice(rowId, 1);
			this.setState({ listViewData: newData });
		}

		handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
		
    render(){
				let { navigation } = this.props;
				const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
				let child = <Content>
							<SignupHeader navigation={navigation} messageHeader previousScreen="true" label="Notification" whiteBg />
							{this.state.listViewData && this.state.listViewData.length ? 
							<List
								disableLeftSwipe
								disableRightSwipe
								dataSource={this.ds.cloneWithRows(this.state.listViewData)}
								renderRow={data =>
									<TouchableOpacity style={[{padding: 10}, ('test' != 'test') && {backgroundColor: '#eee'}]} activeOpacity={0.7} >
										<View style={{height: 60,paddingLeft: 90,justifyContent: 'center',paddingRight: 90}}>
											<View style={styles.avatar}>
												<Image source={data.to_user_image ? {uri: data.to_user_image} : require('../../assets/images/card_img.png')} style={styles.imageStyle}/>
											</View>
											<View>
												<Text style={styles.nameText} numberOfLines={1}>{data.to_user_name}</Text>
												<Text style={styles.desText} numberOfLines={2}>{data.title} </Text>
											</View>
											<View style={styles.timeView}>
												<Text style={styles.timeText}>{moment(data.createddate).format('hh:mm A')}</Text>
											</View>
										</View>	
									</TouchableOpacity>
								}
								
								renderRightHiddenRow={(data, secId, rowId, rowMap) =>
									<View style={{flexDirection: 'row', height: 80}}>
										<Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)} style={{height: 80, backgroundColor: '#e6e6e6'}}>
											<View style={{flexDirection: 'column'}}>
												<Icon active name="more" style={{color: '#363636', fontSize: fontSize32, fontWeight: '500'}}/>
												<Text style={{color: '#363636', fontSize: fontSize12, fontWeight: '500', textAlign: 'center'}}>More</Text>
											</View>
										</Button>
										<Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)} style={{height: 80}}>
										<View style={{flexDirection: 'column'}}>
											<Icon active name="trash" style={{color: '#fff', fontSize: fontSize24, fontWeight: '500'}} />
											<Text style={{color: '#fff', fontSize: fontSize12, fontWeight: '500', textAlign: 'center'}}>Delete</Text>
										</View>
										</Button>
									</View>			
								}
							/>:
							<Text style={{marginTop: 20, color: primaryColor, textAlign: 'center'}}>No Record Found!</Text>}
							{this.handelLoader()}
              </Content>
        return(
            <DrawerMenu navigation={navigation} child={child} />
        )
    }
}
export default connect(state => state)(Notification)
const styles = StyleSheet.create({
	avatar: {
		position: 'absolute',
		height: 60,
		width: 60,
		left: 10,
		top: 0,
		borderRadius: 30,
		overflow: 'hidden'
	},
	imageStyle: {
		width: '100%',
		height: '100%'
	},
	nameText: {
		color: '#363636',
		fontSize: fontSize16,
		fontFamily: fontFamily,
		fontWeight: '500',
		marginBottom: 5
	},
	desText: {
		color: '#b9b9b9',
		fontSize: fontSize12,
		fontFamily: fontFamily
	},
	timeView: {
		position: 'absolute',
		right: 15,
		top: 10
	},
	timeText: {
		color: '#b9b9b9',
		fontSize: fontSize14,
		fontFamily: fontFamily
	}
})