import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
var { height, width } = Dimensions.get('window');

export default class Partner extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Partner Preference" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Matches and Interested :-</Text>
                                </View>
                                <View style={styles.width50Con}>
                                  <View style={[styles.marginBottom30, styles.width50, styles.paddingRight10]}>
                                      <TextField label="Min Age" />
                                  </View>
                                  <View style={[styles.marginBottom30, styles.width50, styles.paddingLeft10]}>
                                      <TextField label="Max Age" />
                                  </View>
                                </View>
                                <View style={styles.width50Con}>
                                  <View style={[styles.marginBottom30, styles.width50, styles.paddingRight10]}>
                                      <TextField label="Min Height" />
                                  </View>
                                  <View style={[styles.marginBottom30, styles.width50, styles.paddingLeft10]}>
                                      <TextField label="Max Height" />
                                  </View>
                                </View>

                                <View style={styles.marginBottom30}>
                                    <TextField label="Physical Status" />
                                </View>

                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Religious Preferences :-</Text>
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Religion" />
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Mother Tongue" />
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Cast" />
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Manglik" />
                                </View>

                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Professional Preferences :-</Text>
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Education" />
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Occupation" />
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Annual Income" />
                                </View>
                                <View style={styles.padding35}>
                                    <RoundButton label="Next" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} />
                                </View>
                            </View>
                        </View>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    },
    width50Con: {
      flexDirection: 'row'
    },
    width50: {
      flex: 1
    },
    paddingRight10: {
      paddingRight: 10
    },
    paddingLeft10: {
      paddingLeft: 10
    }
})