
import React from 'react'
import { Text, View, AsyncStorage, Image, Dimensions, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
// import { GooglePay } from 'react-native-google-pay';
import RNUpiPayment from 'react-native-upi-payment';
var { height, width } = Dimensions.get('window');
const allowedCardNetworks = ['VISA', 'MASTERCARD'];
const allowedCardAuthMethods = ['PAN_ONLY', 'CRYPTOGRAM_3DS'];
const requestData = {
    cardPaymentMethod: {
      tokenizationSpecification: {
        type: 'PAYMENT_GATEWAY',
        // stripe (see Example):
        gateway: 'stripe',
        gatewayMerchantId: '',
        stripe: {
          publishableKey: 'pk_test_51HEBPYCcu7l5rBuu8DiuppprJgrmeooeeqSkhjBTEi270CqcWRPjHKcwS0OwQoSJErm4e9fG2Q58lzzMAEANTuKn00a3ru8JL7',
          version: '2018-11-08',
        }
      },
      allowedCardNetworks,
      allowedCardAuthMethods,
    },
    transaction: {
      totalPrice: '10',
      totalPriceStatus: 'FINAL',
      currencyCode: 'USD',
    },
    merchantName: 'Example Merchant',
};

class CheckRoute extends React.Component {
    static navigationOptions = {
        header: null
    }
    constructor(){
        super();
        this.state={
            Status:"", txnId:"",
        }
    }
    
    async UNSAFE_componentWillMount(){
        let token = await AsyncStorage.getItem('token')
        setTimeout(() => {
            if(token){
                this.props.navigation.navigate('Dashboard')
            }else{
                this.props.navigation.navigate('StartScreen')
            }
        }, 2000);
        // console.log(GooglePay,"GooglePay")
        // GooglePay.setEnvironment(GooglePay.ENVIRONMENT_TEST);
    }
    onPress = () => {
        // GooglePay.isReadyToPay(allowedCardNetworks, allowedCardAuthMethods)
        // .then((ready) => {
        //     if (ready) {
        //     // Request payment token
        //     GooglePay.requestPayment(requestData)
        //         .then((res) => {
        //         // Send a token to your payment gateway
        //         })
        //         .catch((error) => console.log(error.code, error.message));
        //     }
        // })
        // RNUpiPayment.initializePayment({
        //     vpa: 'parveshchauhan27@okicici', // or can be john@ybl or mobileNo@upi
        //     payeeName: 'Parvesh Kumar',
        //     amount: '1',
        //     transactionRef: "aasf-332-aoei-fn"
        // },this.successCallback,this.failureCallback);
    }
    failureCallback = (data) => {
        console.log(data,"data")
        if(data['Status']=="SUCCESS"){
            this.setState({Status:"SUCCESS",txnId:data['txnId']});
        }else{
            this.setState({Status:"FAILURE"})
        }
    }
    successCallback = (data) => {
        console.log(data,"data err")
        //nothing happened here using Google Pay
    }
    render() {
        return (
            <View style={{height: height, width: width}}>
            {/* <TouchableOpacity style={{marginTop: 30, height: 50}} onPress={this.onPress}><Text>Pay Now</Text></TouchableOpacity> */}
                <Image source={require('../../assets/images/splash-screen.png')} style={{width: '100%', height: '100%'}}/>
            </View>
        )
    }
}

export default connect(state => state)(CheckRoute)