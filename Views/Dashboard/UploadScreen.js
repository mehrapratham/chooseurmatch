import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content, Footer } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
import MainFooter from '../../components/Footer'

import DrawerMenu from '../../components/Common/DrawerMenu'
var { height, width } = Dimensions.get('window');

export default class UploadScreen extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        let child = <Content>
                      <View style={styles.container}>
                        <View style={styles.containerView}>
                          <View style={styles.marginBottom30}>
                            <View style={styles.textCenter}>
                                <Text style={styles.headingText}>Welcome to ChooseUrMatch!</Text>
                            </View>
                            <View style={styles.textCenter}>
                                <Text style={styles.headingText}>Lets help you with next steps</Text>
                            </View>
                          </View>

                          <View>
                            <Text style={styles.nameText}>Pankaj Sharma</Text>
                            <Text style={styles.infoText}>(Please upload your beauty photo now.</Text>
                            <Text style={styles.infoText}>Remember , no one wants to show an interest in your prfile if you dont’t have photos).</Text>
                          </View>

                          <View style={styles.photoUploadBox}>
                            <View style={styles.photoContainer}>
                              <Image source={require('../../assets/images/user_placeholder.png')} style={{width: 90, height: 90}} />
                            </View>
                            <View style={styles.uploadButton}>
                              <RoundButton label="UPLOAD PHOTO" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} />
                            </View>
                          </View>

                          <View>
                            <Text style={styles.infoText}>Upload additional photos (click on photo icon)</Text>
                          </View>

                          <View style={styles.photoImageContainer}>
                            <View style={[styles.photoContainer, styles.width33]}>
                              <Image source={require('../../assets/images/user_placeholder.png')} style={{width: 90, height: 90}} />
                            </View>
                            <View style={[styles.photoContainer, styles.width33, styles.alignCenter]}>
                              <Image source={require('../../assets/images/user_placeholder.png')} style={{width: 90, height: 90}} />
                            </View>
                            <View style={[styles.photoContainer, styles.width33, styles.alignRight]}>
                              <Image source={require('../../assets/images/user_placeholder.png')} style={{width: 90, height: 90}} />
                            </View>
                          </View>

                          <View style={[styles.uploadButton, styles.alignCenterButton]}>
                            <RoundButton label="SKIP" buttonStyle={styles.buttonStyleGray} textStyle={styles.registerText} />
                          </View>
                           
                        </View>
                      </View>
                  </Content>
        let header = <SignupHeader navigation={navigation} onlyLogo />
        return(
          <DrawerMenu navigation={navigation} child={child} header={header} />
        )
    }
}
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },

    headingText: {
    	fontSize: 24
    },
    textCenter: {
    	alignItems: 'center'
    },
    nameText: {
    	color: '#ee1d24',
    	fontSize: 20
    },
    infoText: {
    	color: '#999999',
    	fontSize: 14
    },
    photoUploadBox: {
    	alignItems: 'center',
    	marginTop: 20,
    	marginBottom: 20
    },
    uploadButton: {
    	width: 200,
    	marginTop: 10
    },
    photoContainer: {
    	width: 90,
    	height: 90,
    	borderRadius: 60,
    	overflow: 'hidden'
    },
    photoImageContainer: {
    	flexDirection: 'row',
    	marginTop: 30
    },
    alignCenter: {
    	alignItems: 'center'
    },
    width33: {
    	flex: 1
    },
    alignRight: {
    	alignItems: 'flex-end'
    },
    alignCenterButton: {
    	alignSelf: 'center',
    	marginTop: 30
    },
    buttonStyleGray: {
    	backgroundColor: '#cccccc'
    }
})