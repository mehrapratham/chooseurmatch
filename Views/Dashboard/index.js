import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, SafeAreaView, AsyncStorage } from 'react-native';
import { Container, Content, Drawer, Icon } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize9, fontSize18, fontSize11, fontSize13, fontSize24, fontSize15, fontSize8, fontSize32, menuItems } from '../../redux/actions/constant';
import MainFooter from '../../components/Footer'
import RoundButton from '../../components/Buttons/RoundButton';
import DrawerMenu from '../../components/Common/DrawerMenu';
import { apiCall } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
import Tag from 'react-native-vector-icons/AntDesign'
var { height, width } = Dimensions.get('window');

class Dashboard extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
      loader: false,
      homeContent: {},
      memberLookingList: [],
      newMatch: [],
      userDetail: {}
    }

    async componentDidMount(){
      let user = await AsyncStorage.getItem('user')
      let user_id = await AsyncStorage.getItem('user_id')
      this.setState({ loader: true, userDetail: JSON.parse(user) })
      this.props.dispatch(apiCall({action: 'homepageContent', user_id: user_id})).then(res => {
        console.log(res,"res")
        this.setState({ loader: false })
        if(res.data.status === 'success'){
          this.setState({ homeContent: res.data.data, memberLookingList: res.data.data.memberlooking, newMatch: res.data.data.newMatch })
        }
      }).catch(err => {
        this.setState({ loader: false })
      })
    }

    handelLoader() {
      let { loader } = this.state
      if (loader) {
          return <ShowLoader />
      } else {
          return null
      }
      return
  }


    render(){
        let { navigation } = this.props;
        let { homeContent, newMatch, memberLookingList, userDetail } = this.state;
        console.log(userDetail,"userDetail")
        let child = <Content>
                      <View style={styles.topRedView}></View>
                      <SafeAreaView>
                        <View style={{paddingLeft: 15, flexDirection: 'row', height: 35, alignItems: 'center'}}>
                          <View style={[styles.leftView, {borderRadius: 20, overflow: 'hidden'}]}>
                            <Image source={userDetail.profile_image ? {uri: userDetail.profile_image} : require('../../assets/images/card_img.png')} style={styles.imageStyle}/>
                          </View>
                          <View>
                            <Text style={{marginLeft: 10, color: '#fff', fontWeight: '500'}}>{userDetail.nickname}</Text>
                          </View>
                        </View>
                      </SafeAreaView>
                      <View style={styles.upperCard}>
                        <View style={styles.leftView}>
                          <Image source={require('../../assets/images/discount.png')} style={styles.imageStyle}/>
                        </View>
                        <View style={styles.innerUpperCardView}>
                          <Text style={styles.flatText}>Get Flat 40% OFF</Text>
                          <Text style={styles.downTextCard}>10-RS / PERSON INFORMATION !!!!!</Text>
                          <Text style={styles.redText}>Send Request for share Photo, Contact No.</Text>
                          <Text style={styles.redText}>Email and personal information</Text>
                        </View>
                        {/* <View style={styles.buttonViewDetail}>
                          <RoundButton label="Detail" buttonStyle={styles.buttonDetailOuter} mainView={styles.mainView} textStyle={styles.textStyle}/>
                        </View> */}
                      </View>
                      <View style={[styles.secondView, {alignSelf: 'center'}]}>
                        {/* <View style={[styles.halfView,{borderRightWidth: 0.5, borderColor: '#e6e6e6'}]}>
                          <Text style={styles.acceptText}>0</Text>
                          <Text style={styles.acceptanceText}>ACCEPTANCES</Text>
                        </View> */}
                        <View style={[styles.halfView]}>
                          <Text style={styles.acceptText}>{homeContent.justJoined}</Text>
                          <Text style={styles.acceptanceText}>JUST JOINED</Text>
                        </View>
                      </View>
                      <View style={[styles.containerView,{paddingBottom: 0}]}>
                        <TouchableOpacity style={styles.headingCon} onPress={() => navigation.navigate('NewMatch')}>
                          <Text style={styles.headingText}>NEW MATCH</Text>
                          <Icon name="arrow-forward" style={ styles.itemArrow}  />
                        </TouchableOpacity>

                        <ScrollView horizontal={true} style={styles.scrollCon} showsHorizontalScrollIndicator={false}>
                          {newMatch.length ? newMatch.map((item,key) => {
                            return(
                              <TouchableOpacity style={styles.matchCard} key={key} onPress={() => navigation.navigate('DetailPage', {to_user_id: item.tokensID})} activeOpacity={0.7}>
                                <View>
                                  <Image source={item.profile_image ? {uri: item.profile_image} : require('../../assets/images/card_img.png')} style={{width: 80, height: 80}} />
                                  <Image source={require('../../assets/images/frame.png')} style={styles.frame} />
                                </View>
                                <View style={[styles.halfWidth, styles.havePadding]}>
                                  <Text style={[styles.nameText, {width: 70}]}>{item.nickname}</Text>
                                  <Text style={[styles.yearText, {width: 70}]}>{'"'+item.age} year, {item.height}”</Text>
                                </View>
                              </TouchableOpacity>
                            )
                          }):<View/>}
                            
                        </ScrollView>

                      </View>


                      {/* <View style={styles.containerView}>
                        <View style={styles.headingCon}>
                          <Text style={styles.headingText}>AWAITING YOUR RESPONSE</Text>
                          <Icon name="arrow-forward" style={ styles.itemArrow}  />
                        </View>

                        <ScrollView horizontal={true} style={styles.scrollCon}>
                          <View style={styles.cardContainer}>
                            <View style={styles.halfWidth}>
                              <Image source={require('../../assets/images/card_img.png')} style={{width: '100%', height: 120}} />
                            </View>
                            <View style={[styles.halfWidth, styles.havePadding, styles.contentCenter]}>
                              <Text style={styles.responseText}>Miss Shalini request you to add your photo</Text>
                              <TouchableOpacity style={styles.buttonView}>
                                <Text style={styles.signInText}>Add Photo</Text>
                              </TouchableOpacity>
                            </View>
                          </View>

                          <View style={styles.cardContainer}>
                            <View style={styles.halfWidth}>
                              <Image source={require('../../assets/images/card_img.png')} style={{width: '100%', height: 120}} />
                            </View>
                            <View style={[styles.halfWidth, styles.havePadding, styles.contentCenter]}>
                              <Text style={styles.responseText}>Miss Shalini request you to add your photo</Text>
                              <TouchableOpacity style={styles.buttonView}>
                                <Text style={styles.signInText}>Add Photo</Text>
                              </TouchableOpacity>
                            </View>
                          </View>

                          <View style={styles.cardContainer}>
                            <View style={styles.halfWidth}>
                              <Image source={require('../../assets/images/card_img.png')} style={{width: '100%', height: 120}} />
                            </View>
                            <View style={[styles.halfWidth, styles.havePadding]}>
                              <Text style={styles.responseText}>Miss Shalini request you to add your</Text>
                              <TouchableOpacity style={styles.buttonView}>
                                <Text style={styles.signInText}>Add Photo</Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </ScrollView>
                      </View> */}

                      <View style={[styles.containerView,{flexDirection: 'row'}]}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                          <TouchableOpacity style={styles.shortView} onPress={() => navigation.navigate('LikedUser')}>
                            <View style={styles.cartCon}>
                              <Icon name="heart" style={{fontSize: 46, marginRight: 10, color: whiteColor}}/>
                              <Text style={styles.whiteText}>{homeContent.liked}</Text>
                            </View>
                            <Text style={styles.shortText}>Liked</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.shortView} onPress={() => navigation.navigate('TaggedUser')}>
                            <View style={styles.cartCon}>
                              <Tag name="tag" style={{fontSize: 36, marginRight: 10, color: whiteColor, marginTop: 5}}/>
                              <Text style={styles.whiteText}>{homeContent.tagged}</Text>
                            </View>
                            <Text style={styles.shortText}>Tagged</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={styles.shortView} onPress={() => navigation.navigate('FavouriteUser')}>
                            <View style={styles.cartCon}>
                              <Icon name="star" style={{fontSize: 46, marginRight: 10, color: whiteColor}}/>
                              <Text style={styles.whiteText}>{homeContent.favourite}</Text>
                            </View>
                            <Text style={styles.shortText}>Favourite</Text>
                          </TouchableOpacity>
                          
                        </ScrollView>
                      </View>

                      {/* <View style={styles.successView}>
                        <View style={styles.imageBoul}>
                          <Image source={require('../../assets/images/stars-png-stars-png-clipart-png-image-2992.png')} style={styles.imageStyle}/>
                        </View>
                        <View style={styles.leftViahImage}>
                          <Image source={require('../../assets/images/sm_story3.png')} style={styles.imageStyle}/>
                        </View>
                        <View style={styles.innerViewAbs}>
                          <Text style={styles.successText}>Success Stories</Text>
                          <Text style={styles.loveText}>We love bringing people together and here at Datelleto we've helped thousands of singles find love</Text>
                          <View style={styles.viewWidth}>
                            <RoundButton label="More Stories" buttonStyle={styles.moreButton} mainView={styles.moreViewCon} textStyle={styles.textMainStyles}/>
                          </View>
                        </View>
                      </View> */}

                      <View style={styles.containerView}>
                        <TouchableOpacity style={styles.headingCon} onPress={() => navigation.navigate('MemberLooking')}>
                          <Text style={styles.headingText}>MEMBER LOOKING FOR ME</Text>
                          <Icon name="arrow-forward" style={ styles.itemArrow}  />
                        </TouchableOpacity>
                          {memberLookingList.length ? memberLookingList.map((item,key) => {
                            return(
                              <TouchableOpacity style={styles.cartContainer} key={key} onPress={() => navigation.navigate('DetailPage', {to_user_id: item.tokensID})} activeOpacity={0.7}>
                                <View style={styles.halfImageCon}>
                                  <Image source={item.profile_image ? {uri: item.profile_image} :require('../../assets/images/card_img.png')} style={{width: '100%', height: '100%'}} />
                                </View>
                                <View style={styles.rightConCard}>
                                  <View style={styles.marginBottom20}>
                                    <Text style={styles.nameCardText}>{item.nickname}</Text>
                                    <Text style={styles.pIdText}>PROFILE ID: {item.profile_id}</Text>
                                  </View>
                                  <View>
                                    <Text style={[styles.pIdText,{fontSize: fontSize9}]}>{item.age} year, {item.height}", {item.religion}</Text>
                                    <Text style={[styles.pIdText,{fontSize: fontSize11, fontWeight: '500'}]}>{item.education}</Text>
                                    <Text style={[styles.pIdText,{fontSize: fontSize11, fontWeight: '500'}]}>{item.occupation}</Text>
                                  </View>
                                </View>
                              </TouchableOpacity>
                            )
                          }):<View/>}
                          
                      </View>
                      {this.handelLoader()}
                    </Content>
        return(
          <DrawerMenu child={child} navigation={navigation} />
        )
    }
}
export default connect(state => state)(Dashboard)
const styles = StyleSheet.create({
  
   containerView: {
      padding: 15,
      width: '100%',
      borderBottomWidth: 0.3,
      paddingBottom: 30,
      paddingTop: 30,
      borderColor: '#b2b2b2'
    }, 
    headingText: {
    	color: primaryColor,
      fontSize: fontSize18,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    headingCon:{
    	marginBottom: 30
    },
    itemArrow: {
    	position: 'absolute',
    	right: 0,
    	top: -3
    },
    scrollCon: {
    	flexDirection: 'row'
    },
    cardContainer: {
    	width: width/2 - 22.5,
    	marginRight: 15,
    	flexDirection: 'row',
      borderWidth: 0.3,
      borderColor: '#b2b2b2'
    },
    halfWidth: {
    	flex: 1
    },
    havePadding: {
    	padding: 10,
    },
    buttonStyle: {
    	backgroundColor: primaryColor,
    	height: 20
    },
    buttonView: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        backgroundColor: whiteColor,
        overflow: 'hidden',
        marginTop: 10,
        backgroundColor: primaryColor,
    },
    signInText: {
      color: whiteColor,
      fontSize: fontSize11,
      fontWeight: '500'
    },
    contentCenter: {
      justifyContent: 'center'
    },
    frame: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: 80,
      height: 80
    },
    responseText: {
      color: '#525252',
      fontSize: fontSize9,
      fontWeight: '500',
      fontFamily: fontFamily
    },
    nameText: {
      color: '#525252',
      fontSize: fontSize12,
      fontFamily: fontFamily
    },
    yearText: {
      color: '#525252',
      fontSize: fontSize9,
      fontFamily: fontFamily
    },
    shortView: {
      height: 100,
      backgroundColor: primaryColor,
      borderRadius: 5,
      width: 100,
      marginRight: 10,
      justifyContent: 'center',
      alignItems: 'center'
    },
    cartCon: {
      flexDirection: 'row',
      marginBottom: 0
    },
    whiteText:{
      fontSize: fontSize24,
      color: whiteColor,
      lineHeight: 46,
      fontWeight: '400',
      fontFamily: fontFamily
    },
    shortText: {
      fontSize: fontSize15,
      fontWeight: '400',
      fontFamily: fontFamily,
      color: whiteColor
    },
    halfImageCon: {
      width: 110,
      height: 110
    },
    rightConCard: {
      backgroundColor: whiteColor,
      borderWidth: 0.5,
      width: width - 120,
      padding: 10,
      borderColor: '#b2b2b2'
    },
    cartContainer: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: 20,
      height: 110
    },
    nameCardText: {
      color: primaryColor,
      fontSize: fontSize16,
      fontFamily: fontFamily,
      fontWeight: '500'
    },
    pIdText: {
      color: '#525252',
      fontSize: fontSize8,
      fontFamily: fontFamily
    },
    marginBottom20: {
      marginBottom: 10
    },
    topRedView: {
      height: 200,
      position: 'absolute',
      top: 0,
      zIndex: -1,
      backgroundColor: primaryColor,
      width: '100%'
    },
    upperCard: {
      padding: 15,
      margin: 15,
      backgroundColor: whiteColor,
      borderRadius: 5,
      borderWidth: 0.5,
      borderColor: '#e6e6e6'
    },
    leftView: {
      height: 35,
      width: 35
    },
    imageStyle: {
      height: '100%',
      width: '100%'
    },
    innerUpperCardView: {
      marginTop: 10
    },
    flatText: {
      color: primaryColor,
      fontSize: fontSize32,
      fontFamily: fontFamily,
      fontWeight: '700',
      marginBottom: 10
    },
    downTextCard: {
      color: '#7b7a7a',
      fontSize: fontSize15,
      fontFamily: fontFamily,
      fontWeight: '500',
      borderBottomWidth: 0.5,
      borderColor: '#7b7a7a',
      marginBottom: 5
    },
    redText: {
      color: primaryColor,
      fontSize: fontSize15,
      fontFamily: fontFamily,
      fontWeight: '500',
    },
    secondView: {
      padding: 10,
      backgroundColor: whiteColor,
      width: '100%',
      marginTop: 20,
      flexDirection: 'row',
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderColor: '#e6e6e6'
    },
    halfView: {
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      height: 70
    },
    acceptanceText: {
      color: '#7b7a7a',
      fontSize: fontSize15,
      fontFamily: fontFamily
    },
    acceptText: {
      color: primaryColor,
      fontSize: fontSize15,
      fontFamily: fontFamily,
      marginBottom: 10
    },
    buttonViewDetail: {
      width: 115,
      marginTop: 15
    },
    buttonDetailOuter: {
      height: 20,
      backgroundColor: primaryColor
    },
    mainView: {
      marginBottom: 0
    },
    textStyle: {
      color: whiteColor,
      fontSize: fontSize11
    },
    successView: {
      
    },
    imageBoul: {
      height: 170,
      width: '100%'
    },
    innerViewAbs: {
      position: 'absolute',
      right: 20,
      top: 20,
      width: '50%'
    },
    successText: {
      fontSize: fontSize18,
      fontFamily: fontFamily,
      fontWeight: '600',
      marginBottom: 10,
      color: whiteColor
    },
    loveText: {
      fontSize: fontSize11,
      fontFamily: fontFamily,
      color: whiteColor
    },
    moreButton: {
      height: 20
    },
    viewWidth: {
      width: 115,
      marginTop: 10
    },
    textMainStyles: {
      fontSize: fontSize11
    },
    moreViewCon: {
      marginBottom: 0
    },
    leftViahImage: {
      position: 'absolute',
      left: 0, 
      top: 10,
      width: '40%',
      height: 150
    }
})