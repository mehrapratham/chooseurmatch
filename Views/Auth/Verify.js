import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor, fontSize10 } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import CombinedButton from '../../components/Buttons/CombinedButton';
import SignupHeader from '../../components/Headers/SignupHeader';
var { height, width } = Dimensions.get('window');

export default class Verify extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Verify" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Verify Contact detail :-</Text>
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Enter Pin" type="numeric" />
                                </View>
                                <View style={styles.padding35}>
                                    <CombinedButton leftLabel="VERIFY" rightLabel="REQUEST PIN" onLeftPress={() => navigation.navigate('Partner')}/>
                                </View>
                                <Text style={styles.hiddenTextNo}>You can use privacy option for hidden your number</Text>
                            </View>
                        </View>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30,
        marginBottom: 12
    },
    hiddenTextNo: {
        color: greyColor,
        fontSize: fontSize10,
        textAlign: 'center'
    }
})