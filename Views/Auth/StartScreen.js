import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import CombinedButton from '../../components/Buttons/CombinedButton';
import { whiteColor } from '../../redux/actions/constant';
var { height, width } = Dimensions.get('window');
export default class StartScreen extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        return(
            <View style={styles.container}>
                <ImageBackground source={require('../../assets/images/start_screen.png')} style={{width: width, height: height,padding: 50}}>
                    <View style={styles.buttonView}>
                        <CombinedButton onLeftPress={() => navigation.navigate('SignIn')} onRightPress={() => navigation.navigate('CreateProfileFor')} leftLabel="SIGN IN" rightLabel="REGISTER" />
                    </View>
                </ImageBackground>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%'
    },
    buttonConatiner: {
        width: '100%',
        elevation: 7,
        shadowColor: "#000000",
        shadowOpacity: 0.4,
        shadowRadius: 5,
        shadowOffset: { height: 2, width: 1 },
        borderRadius: 5,
        flexDirection: 'row',
    },
    buttonView: {
        position: 'absolute',
        bottom: 90,
        left: 50
    },

})