import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
import SelectField from '../../components/SelectField/SelectField';
import { apiCall, openToast } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
var { height, width } = Dimensions.get('window');

class ResetPassword extends React.Component{
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
          passwordData: {
            password: '',
            cpassword: ''
          },
          loader: false
        };
      }

      onChange = (key, event) => {
          console.log(key, event)
          let { passwordData } = this.state;
          passwordData[key] = event;
          this.setState({ passwordData })
      }

      onSubmit = () => {
        let { user_id } = this.props.navigation && this.props.navigation.state && this.props.navigation.state.params || ''
        let { passwordData } = this.state;
        if(passwordData.password && passwordData.cpassword){
            this.setState({ loader: true })
            this.props.dispatch(apiCall({action: 'confirmResetPassword', password: passwordData.password, cpassword: passwordData.cpassword, user_id})).then(res => {
                console.log(res, "res")
                this.setState({ loader: false })
                if(res.data.status === 'error'){
                    this.props.dispatch(openToast(res.data.message))
                }else{
                    this.props.dispatch(openToast(res.data.message))
                    this.props.navigation.navigate('SignIn')
                }
            }).catch(err => {
                this.setState({ loader: false })
                console.log(err,"err")
            })
        }else{
            this.props.dispatch(openToast('All Field is required!'))
        }
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    
    render(){
        let { navigation } = this.props;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Reset Password" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Enter New Password:-</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Password" onChange={(event) => this.onChange('password', event)}/>
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Confirm Password" onChange={(event) => this.onChange('cpassword', event)}/>
                                </View>
                                <View style={styles.padding35}>
                                    <RoundButton label="CONFIRM OTP" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={this.onSubmit}/>
                                </View>
                            </View>
                        </View>
                        {this.handelLoader()}
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}
export default connect(state => state)(ResetPassword)
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    }
})