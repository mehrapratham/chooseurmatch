import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
import SelectField from '../../components/SelectField/SelectField';
import { apiCall, openToast } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
var { height, width } = Dimensions.get('window');

class SendOtp extends React.Component{
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            loader: false
        };
      }

      onSubmit = () => {
          let { email } = this.state;
          if(email){
            this.setState({ loader: true })
            this.props.dispatch(apiCall({action: 'sendResetOTP', email})).then(res => {
                this.setState({ loader: false })
                console.log(res,"res")
                if(res.data[0].status == "success"){
                    this.props.dispatch(openToast(res.data[0].message))
                    this.props.navigation.navigate('ConformOtp',{user_id: res.data[0].user_id})
                }else{
                    this.props.dispatch(openToast(res.data[0].message))
                }
            }).catch(err => {
              console.log(err,"err")
              this.setState({ loader: false })
            })
          }else{
              this.props.dispatch(openToast('Email Field is required!'))
          }
      }

      handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    
    render(){
        let { navigation } = this.props;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Reset Password" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Verify Email:-</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Enter Email" onChange={(event) => this.setState({ email: event })}/>
                                </View>
                                <View style={styles.padding35}>
                                    <RoundButton label="SEND OTP" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={this.onSubmit}/>
                                </View>
                                <View style={styles.rowDirection}>
                                    <Text style={styles.manDatoryText}>Enter your mobile and we'll send you the OTP for reset password</Text>
                                </View>
                            </View>
                        </View>
                        {this.handelLoader()}
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}
export default connect(state => state)(SendOtp)
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily,
        textAlign: 'center'
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    }
})