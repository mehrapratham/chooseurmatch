import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native';
import CombinedButton from '../../components/Buttons/CombinedButton';
import { whiteColor, fontSize19, fontSize12, primaryColor } from '../../redux/actions/constant';
import RoundTextField from '../../components/Input/RoundTextField';
import { Container, Content } from 'native-base';
import RoundButton from '../../components/Buttons/RoundButton';
import { signInValidation } from '../../json/json';
import { apiCall, openToast } from '../../redux/actions';
import { connect } from 'react-redux'
import microValidator from 'micro-validator'
import is from 'is_js'
import ShowLoader from '../../components/ShowLoader';
var { height, width } = Dimensions.get('window');

class SignIn extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        loginData: {
            email: '',
            password: '',
            action: 'login'
        },
        errors: {},
        loader: false
    }
    onChange = (event, key) => {
        let { loginData } = this.state;
        loginData[key] = event
        this.setState({ loginData })
    }

    goToNextScreen = () => {
        let { loginData } = this.state;
        let { navigation } = this.props;
        const errors = microValidator.validate(signInValidation, loginData)
        if (!is.empty(errors)) {
            this.setState({ errors })
            return
        }
        this.setState({ errors: {}, loader: true })
        this.props.dispatch(apiCall(loginData)).then(async res => {
            console.log(res,"res")
            this.setState({ loader: false })
            if(res.data.status == 'success'){
                await AsyncStorage.setItem('token',res.data.jwt)
                await AsyncStorage.setItem('user_id',res.data.tokensID)
                await AsyncStorage.setItem('user', JSON.stringify(res.data.users.usermeta))
                this.props.dispatch(openToast('Login Successfully'))
                navigation.navigate('Dashboard')
            }else{
                this.props.dispatch(openToast(res.data.message))
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { errors } = this.state;
        return(
            <Container>
                <Content>
                    <View style={styles.container}>
                        <ImageBackground source={require('../../assets/images/signIn_bg.png')} style={{width: width, height: height,padding: 50}}>
                            <View style={styles.containerMain}>
                                <View style={styles.marginBottom20}>
                                    <RoundTextField placeholder="Email" onChange={(e) => this.onChange(e, 'email')}/>
                                    <Text style={styles.errorMsgText}>{errors.email && errors.email[0]}</Text>
                                </View>
                                <View style={styles.marginBottom20}>
                                    <RoundTextField placeholder="Password" secureTextEntry={true} onChange={(e) => this.onChange(e, 'password')}/>
                                    <Text style={styles.errorMsgText}>{errors.password && errors.password[0]}</Text>
                                </View>
                                <TouchableOpacity style={styles.forgotView} onPress={() => navigation.navigate('SendOtp')}><Text style={styles.forgotText}>Forgot Password</Text></TouchableOpacity>
                                <RoundButton label="SIGN IN" onPress={this.goToNextScreen}/>
                                <Text style={styles.orText}>OR</Text>
                                <RoundButton label="REGISTER NEW USER" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={() => navigation.navigate('CreateProfileFor')} />
                            </View>
                        </ImageBackground>
                    </View>
                </Content>
                {this.handelLoader()}
            </Container>
        )
    }
}
export default connect(state => state)(SignIn)
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%'
    },
    marginBottom20: {
        marginBottom: 20
    },
    forgotView: {
        alignSelf: 'flex-end',
        marginBottom: 20
    },
    forgotText: {
        color: whiteColor,
        fontSize: fontSize12,
        fontWeight: '600'
    },
    orText: {
        color: whiteColor,
        fontSize: fontSize12,
        textAlign: 'center',
        marginBottom: 20
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    containerMain: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        left: 50
    },
    errorMsgText: {
        fontSize: 10,
        color: whiteColor,
        position: 'absolute',
        bottom: -15,
        textAlign: 'center',
        width: '100%'
    }

})