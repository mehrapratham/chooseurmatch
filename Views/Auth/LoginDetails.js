import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, AsyncStorage, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
import { connect } from 'react-redux'
import { apiCall, saveRegisterUser, apiRegisterCall, apiCalls, openToast } from '../../redux/actions';
import SelectField from '../../components/SelectField/SelectField';
import ShowLoader from '../../components/ShowLoader';
import microValidator from 'micro-validator'
import is from 'is_js'
import { loginValidation } from '../../json/json';
var { height, width } = Dimensions.get('window');

class LoginDetails extends React.Component{
    static navigationOptions = {
        header: null
    }
    state = {
        loginData: {
            first_name: '',
            last_name: '',
            email: '',
            user_id: '',
            mobile: '',
            password: '',
            security_answer: ''
        },
        question: {
            label: '',
            value: ''
        },
        questionData: [],
        dummyQuestionData: [],
        loader: false,
        errors: {}
    }

    componentDidMount(){
        this.setState({ loader: true })
        this.props.dispatch(apiCall({ action: 'getSecurityQuestions' })).then(res => {
            this.setState({ loader: false })
            if(res.data){
                let saveData = res.data.map(item => {
                    return {
                        label: item.question, value: item.id
                    }
                })
                let newObj = {label: '', value: ''}
                this.setState({ questionData: saveData, dummyQuestionData: [newObj, ...saveData] })
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    onChangeText = (event, key) => {
        let { loginData } = this.state;
        loginData[key] = event
        this.setState({ loginData })
    }

    onSelectChange = (event, index, check) => {
        let findObj = this.state.dummyQuestionData.find((itm,key) => key === index)
        if(index){
            this.setState({ question: findObj })
        }else{
            this.setState({ question: {label: '',value: ''} }) 
        }
    }

    goToNextScreen = () => {
        
        let { loginData, question } = this.state;
        let {navigation, userData} = this.props;
        loginData.security_question = question.value
        let apiData = {...userData.registerDetail, ...loginData}
        apiData.action = 'registerUser'
        const errors = microValidator.validate(loginValidation, loginData)
        if (!is.empty(errors)) {
            this.setState({ errors })
            return
        }
        this.setState({ loader: true, errors: {} })
        
        this.props.dispatch(apiCall(apiData)).then(async res => {
            this.setState({ loader: false })
            if(res.data.user_id){
                await AsyncStorage.setItem('token', res.data.jwt)
                await AsyncStorage.setItem('user_id', res.data.user_id.toString())
                await AsyncStorage.setItem('user', JSON.stringify(res.data.users.usermeta))
                navigation.navigate('Dashboard')
                this.props.dispatch(openToast('User Register Successfully'))
            }else{
                this.props.dispatch(openToast(res.data.message))
            }
            // navigation.navigate('Verify')
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }

    render(){
        let { navigation } = this.props;
        let { errors } = this.state;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Login Details" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Login detail :-</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="First Name" onChange={(e) => this.onChangeText(e, 'first_name')} keyCode="first_name"/>
                                    <Text style={styles.errorMsgText}>{errors.first_name && errors.first_name[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Last Name" onChange={(e) => this.onChangeText(e, 'last_name')} keyCode="last_name"/>
                                    <Text style={styles.errorMsgText}>{errors.last_name && errors.last_name[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Email Id" onChange={(e) => this.onChangeText(e, 'email')} keyCode="email"/>
                                    <Text style={styles.errorMsgText}>{errors.email && errors.email[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="User Id" onChange={(e) => this.onChangeText(e, 'user_id')} keyCode="user_id"/>
                                    <Text style={styles.errorMsgText}>{errors.user_id && errors.user_id[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Phone No" type="numeric" onChange={(e) => this.onChangeText(e, 'mobile')} keyCode="mobile"/>
                                    <Text style={styles.errorMsgText}>{errors.mobile && errors.mobile[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Set Password" secureTextEntry={true} onChange={(e) => this.onChangeText(e, 'password')} keyCode="password"/>
                                    <Text style={styles.errorMsgText}>{errors.password && errors.password[0]}</Text>
                                </View>
                                <View style={styles.marginBottom45}>
                                    <SelectField label="Security Question" onChange={this.onSelectChange} value={this.state.question.label || 'Select Question'} keyCode="question" item={this.state.questionData}/>
                                    <Text style={styles.errorMsgText}>{errors.security_question && errors.security_question[0]}</Text>
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="Security Ans" onChange={(e) => this.onChangeText(e, 'security_answer')} keyCode="security_answer"/>
                                    <Text style={styles.errorMsgText}>{errors.security_answer && errors.security_answer[0]}</Text>
                                </View>
                                <View style={styles.rowDirection}>
                                    <Text style={styles.manStarText}>*</Text>
                                    <Text style={styles.manDatoryText}>All fields Mandatory</Text>
                                </View>
                                <View style={styles.padding35}>
                                    <RoundButton label="Next" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={this.goToNextScreen}/>
                                </View>
                            </View>
                        </View>
                    </Content>
                    {this.handelLoader()}
                </ImageBackground>
            </Container>
        )
    }
}
export default connect(state => state)(LoginDetails)
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    },
    errorMsgText: {
        fontSize: 10,
        color: "red",
        position: 'absolute',
        bottom: -15
    }
})