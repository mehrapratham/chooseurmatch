import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image } from 'react-native';
import { whiteColor, fontSize19, fontSize12, primaryColor, blackColor, fontSize16, fontFamily } from '../../redux/actions/constant';
import { Container, Content, Button, Icon, Segment } from 'native-base';
import { createProfileList } from '../../json/json';
import SignupHeader from '../../components/Headers/SignupHeader';
import { saveRegisterUser } from '../../redux/actions';
import { connect } from 'react-redux'
var { height, width } = Dimensions.get('window');

class CreateProfileFor extends React.Component{
    static navigationOptions = {
        header: null
    }
    goToNextScreen = (key) => {
        let { navigation } = this.props;
        this.props.dispatch(saveRegisterUser({ profile_for: key.toString() }))
        navigation.navigate('PersonalDetailIst')
    }
    render(){
        let { navigation } = this.props;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Create Profile For" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                                <View style={styles.container}>
                                    {createProfileList.map((item, key) => {
                                        return(
                                            <TouchableOpacity style={key % 2 == 0 ? styles.singleItem : styles.singleRightItem } key={key} onPress={() => this.goToNextScreen(key)}>
                                                <View style={[styles.imageView, (key === 1 || key === 4 || key === 5 || key === 6) && {height: 42, width: 45}]}>
                                                    <Image source={item.image} style={styles.imageStyle}/>
                                                </View>
                                                <Text style={styles.itemText}>{item.label}</Text>
                                            </TouchableOpacity>
                                        )
                                    })}
                                </View>
                        </View>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}
export default connect(state => state)(CreateProfileFor)
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%'
    },
    singleItem: {
        width: '50%',
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderRightWidth: 1,
        borderColor: primaryColor
    },
    singleRightItem: {
        width: '50%',
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: primaryColor
    },
    imageView: {
        width: 35,
        height: 45
    },
    imageStyle: {
        width: '100%',
        height: '100%'
    },
    itemText: {
        marginTop: 12,
        color: blackColor,
        fontSize: fontSize16,
        fontFamily: fontFamily
    }

})