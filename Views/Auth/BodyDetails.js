import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
var { height, width } = Dimensions.get('window');

export default class BodyDetails extends React.Component{
    static navigationOptions = {
        header: null
    }
    render(){
        let { navigation } = this.props;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Body Details" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Height" type="numeric"/>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Body Type" />
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Skin Tone" />
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Do you Smoke ?" />
                                </View>
                                <View style={styles.marginBottom30}>
                                    <TextField label="Drink ?" />
                                </View>
                                <View style={styles.rowDirection}>
                                    <Text style={styles.manStarText}>*</Text>
                                    <Text style={styles.manDatoryText}>All fields Mandatory</Text>
                                </View>
                                <View style={styles.padding35}>
                                    <RoundButton label="Next" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={() => navigation.navigate('Verify')}/>
                                </View>
                            </View>
                        </View>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    }
})