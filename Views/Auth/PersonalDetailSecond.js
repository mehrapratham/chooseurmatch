import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
import SelectField from '../../components/SelectField/SelectField';
import { apiCall, saveRegisterUser } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
import microValidator from 'micro-validator'
import is from 'is_js'
import { personalDetailSecondValidation } from '../../json/json';
var { height, width } = Dimensions.get('window');

class PersonalDetailSecond extends React.Component{
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
          educationData:[],
          dummyEducationData: [],
          education: {
              label: '',
              value: ''
          },
          occupationData: [],
          dummyOccupationData: [],
          occupation: {
            label: '',
            value: ''
          },
          incomeData: [],
          dummyIncomeData: [],
          income: {
              value: '',
              label: ''
          },
          errors: {}
        };
      }
    componentDidMount(){
        this.setState({ loader: true })
        this.props.dispatch(apiCall({ action: 'getEducation' })).then(res => {
            this.setState({ loader: false })
            if(res.data){
                let saveData = res.data.map(item => {
                    return {
                        label: item.education, value: item.id
                    }
                })
                let newObj = {label: '', value: ''}
                this.setState({ educationData: saveData, dummyEducationData: [newObj, ...saveData] })
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
        this.getOccupation()
        this.getIncome()
    }

    getOccupation(){
        this.props.dispatch(apiCall({ action: 'getOccupation' })).then(res => {
            console.log(res,"res")
            this.setState({ loader: false })
            if(res.data){
                let saveData = res.data.map(item => {
                    return {
                        label: item.occupation, value: item.id
                    }
                })
                let newObj = {label: '', value: ''}
                this.setState({ occupationData: saveData, dummyOccupationData: [newObj, ...saveData] })
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }
    getIncome(){
        this.props.dispatch(apiCall({ action: 'getIncome' })).then(res => {
            console.log(res,"res")
            this.setState({ loader: false })
            if(res.data){
                let saveData = res.data.map(item => {
                    return {
                        label: item.income, value: item.id
                    }
                })
                let newObj = {label: '', value: ''}
                this.setState({ incomeData: saveData, dummyIncomeData: [newObj, ...saveData] })
            }
        }).catch(err => {
            this.setState({ loader: false })
        })
    }

    onSelectChange = (event, index, check) => {
        let findObj = this.state.dummyEducationData.find((itm,key) => key === index)
        if(index){
            this.setState({ education: findObj })
        }else{
            this.setState({ education: {label: '',value: ''} }) 
        }
    }

    onSelectChangeOccupation = (event, index, check) => {
        let findObj = this.state.dummyOccupationData.find((itm,key) => key === index)
        if(index){
            this.setState({ occupation: findObj })
        }else{
            this.setState({ occupation: {label: '',value: ''} }) 
        }
    }
    onSelectChangeIncome = (event, index, check) => {
        let findObj = this.state.dummyIncomeData.find((itm,key) => key === index)
        if(index){
            this.setState({ income: findObj })
        }else{
            this.setState({ income: {label: '',value: ''} }) 
        }
    }

    goToNextScreen = () => {
        let { education, occupation, income } = this.state;
        let { navigation, userData } = this.props;
        let data = {
            occupation: occupation.value,
            income: income.value,
            education: education.value
        }
        const errors = microValidator.validate(personalDetailSecondValidation, data)
        if (!is.empty(errors)) {
            this.setState({ errors })
            return
        }
        this.setState({ errors: {} })
        this.props.dispatch(saveRegisterUser({...userData.registerDetail, ...data}))
        navigation.navigate('PersonalDetailThird')
    }

    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { errors } = this.state;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Personal Details" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Education :-</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <SelectField label="Higher Education" onChange={this.onSelectChange} value={this.state.education.label || ''} key="education" item={this.state.educationData}/>
                                    <Text style={styles.errorMsgText}>{errors.education && errors.education[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Work Experience :-</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <SelectField label="Occupation" onChange={this.onSelectChangeOccupation} value={this.state.occupation.label || ''} key="occupation" item={this.state.occupationData}/>
                                    <Text style={styles.errorMsgText}>{errors.occupation && errors.occupation[0]}</Text>
                                </View>
                                <View style={styles.marginBottom45}>
                                <SelectField label="Your Annual Income" onChange={this.onSelectChangeIncome} value={this.state.income.label || ''} key="income" item={this.state.incomeData}/>
                                    <Text style={styles.errorMsgText}>{errors.income && errors.income[0]}</Text>
                                </View>
                                <View style={styles.rowDirection}>
                                    <Text style={styles.manStarText}>*</Text>
                                    <Text style={styles.manDatoryText}>All fields Mandatory</Text>
                                </View>
                                <View style={styles.padding35}>
                                    <RoundButton label="Next" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={this.goToNextScreen}/>
                                </View>
                            </View>
                        </View>
                    </Content>
                    {this.handelLoader()}
                </ImageBackground>
            </Container>
        )
    }
}
export default connect(state => state)(PersonalDetailSecond)
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    },
    errorMsgText: {
        fontSize: 10,
        color: "red",
        position: 'absolute',
        bottom: -15
    },
})