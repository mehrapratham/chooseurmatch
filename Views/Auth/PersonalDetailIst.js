import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, Keyboard, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import { Container, Content } from 'native-base';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, whiteColor, fontSize16 } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import SelectField from '../../components/SelectField/SelectField';
import RoundButton from '../../components/Buttons/RoundButton';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment'
import PickerSelect from '../../components/Common/PickerSelect';
import SignupHeader from '../../components/Headers/SignupHeader';
import DrawerMenu from '../../components/Common/DrawerMenu'
import { connect } from 'react-redux'
import { apiCall, saveRegisterUser } from '../../redux/actions';
import ShowLoader from '../../components/ShowLoader';
import microValidator from 'micro-validator'
import is from 'is_js'
import { personalDetailIstValidation } from '../../json/json';

class PersonalDetailIst extends React.Component{
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
          isDateTimePickerVisible: false,
          dob: '',
          modalVisible: false,
          value: '',
          countryData:[],
          dummyCountryData: [],
          country: {
              label: '',
              value: ''
          },
          stateData: [],
          dummyStateData: [],
          state: {
            label: '',
            value: ''
          },
          cityData: [],
          dummyCityData: [],
          city: {
            label: '',
            value: ''
          },
          gender: '',
          loader: false,
          errors: {}
        };
      }
    componentDidMount(){
        this.setState({ loader: true })
        this.props.dispatch(apiCall({ action: 'getCountry' })).then(res => {
            this.setState({ loader: false })
            if(res.data){
                let saveData = res.data.map(item => {
                    return {
                        label: item.name, value: item.id
                    }
                })
                let newObj = {label: '', value: ''}
                this.setState({ countryData: saveData, dummyCountryData: [newObj, ...saveData] })
            }
            
            
        }).catch(err => {
            this.setState({ loader: false })
        })
    }
     
    showDateTimePicker = () => {
        Keyboard.dismiss();
        this.setState({ isDateTimePickerVisible: true });
    };
    
    hideDateTimePicker = () => {
        Keyboard.dismiss();
        this.setState({ isDateTimePickerVisible: false });
    };
    
    handleDatePicked = date => {
        this.setState({ dob: moment(date).format('DD-MM-YYYY') });
        this.hideDateTimePicker();
    };
    onSelect(index, value){
        this.setState({
            gender: index + 1
        })
    }
    openPicker = (visible) => {
        Keyboard.dismiss();
        this.setState({ modalVisible: visible })
    }
    onSelectChange = (event, index, check) => {
        this.setState({ loader: true })
        let findObj = this.state.dummyCountryData.find((itm,key) => key === index)
        if(index){
            this.setState({ country: findObj })
            this.props.dispatch(apiCall({ action: 'getStateByCountry', country_id: findObj.value })).then(res => {
                this.setState({ loader: false })
                if(res.data){
                    let saveData = res.data.map(item => {
                        return {
                            label: item.name, value: item.id
                        }
                    })
                    let newObj = {label: '', value: ''}
                    this.setState({ stateData: saveData, dummyStateData: [newObj, ...saveData] })
                }
            }).catch(err => {
                this.setState({ loader: false })
            })
        }else{
            this.setState({ loader: false })
            this.setState({ country: {label: '',value: ''} })
        }
    }
    onSelectStateChange = (event, index) => {
        this.setState({ loader: true })
        let findObj = this.state.dummyStateData.find((itm,key) => key === index)
        if(index){
            this.setState({ state: findObj })
            this.props.dispatch(apiCall({ action: 'getCityByState', state_id: findObj.value })).then(res => {
                this.setState({ loader: false })
                if(res.data){
                    let saveData = res.data.map(item => {
                        return {
                            label: item.name, value: item.id
                        }
                    })
                    let newObj = {label: '', value: ''}
                    this.setState({ cityData: saveData, dummyCityData: [newObj, ...saveData] })
                }
            }).catch(err => {
                this.setState({ loader: false })
            })
        }else{
            this.setState({ loader: false })
            this.setState({ state: {label: '',value: ''} })
        }
    }

    onSelectCityChange = (event, index) => {
        let findObj = this.state.dummyCityData.find((itm,key) => key === index)
        if(index){
            this.setState({ city: findObj })
        }else{
            this.setState({ city: {label: '',value: ''} })
        }
    }
    goToNextScreen = () => {
        let { gender, country, state, city, dob } = this.state;
        let { navigation, userData } = this.props;
        let data = {
            gender: gender.toString(),
            country: country.value,
            state: state.value,
            city: city.value,
            dob
        }
        const errors = microValidator.validate(personalDetailIstValidation, data)
        if (!is.empty(errors)) {
            this.setState({ errors })
            return
        }
        this.setState({ errors: {} })
        this.props.dispatch(saveRegisterUser({...userData.registerDetail, ...data}))
        navigation.navigate('PersonalDetailSecond')
    }
    handelLoader() {
        let { loader } = this.state
        if (loader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { errors } = this.state;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Personal Details" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                                <View style={styles.containerView}>
                                    <View style={styles.marginBottom30}>
                                        <RadioGroup
                                            onSelect = {(index, value) => this.onSelect(index, value)}
                                            style={{flexDirection: 'row',marginLeft: -10}}
                                            color="0c0c0c"
                                        >
                                            <RadioButton value={'item1'} color={primaryColor} style={styles.radioView}>
                                                <Text style={styles.radioText}>Male</Text>
                                            </RadioButton>
                                            <RadioButton value={'item1'} color={primaryColor} style={styles.radioView} >
                                                <Text style={styles.radioText}>Female</Text>
                                            </RadioButton>
                                        </RadioGroup>
                                        <Text style={styles.errorMsgText}>{errors.gender && errors.gender[0]}</Text>
                                    </View>
                                    <View style={styles.marginBottom30}>
                                        <TextField label="Date Of Birth" onFocus={this.showDateTimePicker} value={this.state.dob}/>
                                        <Text style={styles.errorMsgText}>{errors.dob && errors.dob[0]}</Text> 
                                    </View>
                                    <View style={styles.marginBottom30}>
                                        <SelectField label="Country" onChange={this.onSelectChange} value={this.state.country.label || ''} key="country" item={this.state.countryData}/>
                                        <Text style={styles.errorMsgText}>{errors.country && errors.country[0]}</Text> 
                                    </View>
                                    <View style={styles.marginBottom30} >
                                        <SelectField label="State" onChange={this.onSelectStateChange} value={this.state.state.label || ''} item={this.state.stateData}/>
                                        <Text style={styles.errorMsgText}>{errors.state && errors.state[0]}</Text> 
                                    </View>
                                    <View style={styles.marginBottom45} >
                                        <SelectField label="City" onChange={this.onSelectCityChange} value={this.state.city.label|| ''} item={this.state.cityData}/>
                                        <Text style={styles.errorMsgText}>{errors.city && errors.city[0]}</Text> 
                                    </View>
                                    <View style={styles.rowDirection}>
                                        <Text style={styles.manStarText}>*</Text>
                                        <Text style={styles.manDatoryText}>All fields Mandatory</Text>
                                    </View>
                                    <View style={styles.padding35}>
                                        <RoundButton label="Next" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={this.goToNextScreen}/>
                                    </View>
                                </View>
                        </View>
                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this.handleDatePicked}
                            onCancel={this.hideDateTimePicker}
                        />
                    </Content>
                    {this.handelLoader()}
                </ImageBackground>
            </Container>
        )
    }
}
export default connect(state => state)(PersonalDetailIst)
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    radioText: {
        fontSize: fontSize14,
        color: blackColor,
        fontFamily: fontFamily
    },
    radioView: {
        width: 110
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    },
    errorMsgText: {
        fontSize: 10,
        color: "red",
        position: 'absolute',
        bottom: -15
    },
})