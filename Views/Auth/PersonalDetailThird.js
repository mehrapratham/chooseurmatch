import React from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, TextInput } from 'react-native';
import { Container, Content } from 'native-base';
import { primaryColor, fontSize14, blackColor, fontFamily, greyColor, fontSize12, fontSize16, whiteColor } from '../../redux/actions/constant';
import TextField from '../../components/Input/TextField';
import RoundButton from '../../components/Buttons/RoundButton';
import SignupHeader from '../../components/Headers/SignupHeader';
import SelectField from '../../components/SelectField/SelectField';
import { apiCall, saveRegisterUser } from '../../redux/actions';
import { connect } from 'react-redux'
import ShowLoader from '../../components/ShowLoader';
import microValidator from 'micro-validator'
import is from 'is_js'
import { personalDetailThirdValidation, maritalData } from '../../json/json';
var { height, width } = Dimensions.get('window');
let newObj = {label: '', value: ''}
let dummymaritalData = [newObj, ...maritalData ]

class PersonalDetailThird extends React.Component{
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
          languageData:[],
          dummyLanguageData: [],
          language: {
              label: '',
              value: ''
          },
          communityData:[],
          dummyCommunityData: [],
          community: {
              label: '',
              value: ''
          },
          marital_status: {
              label: '',
              value: ''
          },
          live_in: '',
          languageLoader: false,
          religionLoader: false,
          errors: {}
        };
    }
    componentDidMount(){
        this.setState({ languageLoader: true, religionLoader: true })
        this.props.dispatch(apiCall({ action: 'getLanguage' })).then(res => {
            this.setState({ languageLoader: false })
            if(res.data){
                let saveData = res.data.map(item => {
                    return {
                        label: item.language, value: item.id
                    }
                })
                let newObj = {label: '', value: ''}
                this.setState({ languageData: saveData, dummyLanguageData: [newObj, ...saveData] })
            }
        }).catch(err => {
            this.setState({ languageLoader: false })
        })

        this.props.dispatch(apiCall({ action: 'getCommunity' })).then(res => {
            this.setState({ religionLoader: false })
            if(res.data){
                let saveData = res.data.map(item => {
                    return {
                        label: item.community, value: item.id
                    }
                })
                let newObj = {label: '', value: ''}
                this.setState({ communityData: saveData, dummyCommunityData: [newObj, ...saveData] })
            }
        }).catch(err => {
            this.setState({ religionLoader: false })
        })
    }
    onSelectChange = (event, index, check) => {
        if(check === 'language'){
            let findObj = this.state.dummyLanguageData.find((itm,key) => key === index)
            if(index){
                this.setState({ language: findObj })
            }else{
                this.setState({ language: {label: '',value: ''} }) 
            }
        }
        if(check === 'community'){
            let findObj = this.state.dummyCommunityData.find((itm,key) => key === index)
            if(index){
                this.setState({ community: findObj })
            }else{
                this.setState({ community: {label: '',value: ''} }) 
            }
        }
        if(check === 'marital_status'){
            let findObj = dummymaritalData.find((itm,key) => key === index)
            if(index){
                this.setState({ marital_status: findObj })
            }else{
                this.setState({ marital_status: {label: '',value: ''} }) 
            }
        }

    }
    onChangeText = (event) => {
        this.setState({ live_in: event })
    }
    goToNextScreen = () => {
        let { marital_status, language, community, live_in } = this.state;
        let { navigation, userData } = this.props;
        let data = {
            marital_status: marital_status.value,
            mother_tongue: language.value,
            religion: community.value,
            live_in
        }
        const errors = microValidator.validate(personalDetailThirdValidation, data)
        if (!is.empty(errors)) {
            this.setState({ errors })
            return
        }
        this.setState({ errors: {} })
        this.props.dispatch(saveRegisterUser({...userData.registerDetail, ...data}))
        navigation.navigate('LoginDetails')
    }
    handelLoader() {
        let { languageLoader, religionLoader } = this.state
        if (languageLoader || religionLoader) {
            return <ShowLoader />
        } else {
            return null
        }
        return
    }
    render(){
        let { navigation } = this.props;
        let { errors } = this.state;
        return(
            <Container>
                <ImageBackground source={require('../../assets/images/bg-2.png')} style={{width: '100%', height: '100%'}}>
                    <SignupHeader label="Personal Details" navigation={navigation}/>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.containerView}>
                                <View style={styles.marginBottom30}>
                                    <Text style={styles.educationText}>Status :-</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <SelectField label="Merital Status" onChange={this.onSelectChange} value={this.state.marital_status.label || ''} keyCode="marital_status" item={maritalData}/>
                                    <Text style={styles.errorMsgText}>{errors.marital_status && errors.marital_status[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <SelectField label="Mother Tongue" onChange={this.onSelectChange} value={this.state.language.label || ''} keyCode="language" item={this.state.languageData}/>
                                    <Text style={styles.errorMsgText}>{errors.mother_tongue && errors.mother_tongue[0]}</Text>
                                </View>
                                <View style={styles.marginBottom30}>
                                    <SelectField label="Your Community" onChange={this.onSelectChange} value={this.state.community.label || ''} keyCode="community" item={this.state.communityData}/>
                                    <Text style={styles.errorMsgText}>{errors.religion && errors.religion[0]}</Text>
                                </View>
                                <View style={styles.marginBottom45}>
                                    <TextField label="You live In" onChange={(event) => this.onChangeText(event)} value={this.state.live_in} keyCode="live_in"/>
                                    <Text style={styles.errorMsgText}>{errors.live_in && errors.live_in[0]}</Text>
                                </View>
                                <View style={styles.rowDirection}>
                                    <Text style={styles.manStarText}>*</Text>
                                    <Text style={styles.manDatoryText}>All fields Mandatory</Text>
                                </View>
                                <View style={styles.padding35}>
                                    <RoundButton label="Next" buttonStyle={styles.buttonStyle} textStyle={styles.registerText} onPress={this.goToNextScreen}/>
                                </View>
                            </View>
                        </View>
                    </Content>
                    {this.handelLoader()}
                </ImageBackground>
            </Container>
        )
    }
}
export default connect(state => state)(PersonalDetailThird)
const styles = StyleSheet.create({
    containerView: {
        padding: 15,
        paddingTop: 30,
        width: '100%'
    },
    container: {
        // flex: 1
    },
    marginBottom30: {
        marginBottom: 30
    },
    marginBottom45: {
        marginBottom: 45
    },
    rowDirection: {
        flexDirection: 'row',
        marginBottom: 45
    },
    manStarText: {
        color: greyColor,
        marginTop: -2,
        marginRight: 3,
        fontSize: 8
    },
    manDatoryText: {
        color: greyColor,
        fontSize: fontSize12,
        fontFamily: fontFamily
    },
    educationText: {
        fontSize: fontSize16
    },
    buttonStyle: {
        backgroundColor: primaryColor
    },
    registerText: {
        color: whiteColor
    },
    padding35: {
        paddingLeft: 35,
        paddingRight: 30
    },
    errorMsgText: {
        fontSize: 10,
        color: "red",
        position: 'absolute',
        bottom: -15
    }
})